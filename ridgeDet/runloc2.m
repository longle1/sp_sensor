%
%     runloc.m
%     Douglas L. Jones
%     Sonistic, LLC
%     April 5, 2012
%
%  runloc.m:  loads call data, synthesizes synthetic array data, and runs localizer
%
%
%  HISTORY:
%    Started from scratch on April 5, 2012
%    Added real data load on April 21, 2012
%
%
%  NOTES:
%
%

clear all
close all

%
%  SET PARAMETERS
%
%fs = 44100;
csound = 343;  % speed of sound in m/s

%freqbpflow = 6100;   % Low BPF cutoff frequency (Hz)
%freqbpfhigh = 6500;  % High BPF cutoff frequency (Hz)

starttime = 35.0;%20	% enter start time into record (in seconds)
endtime =   51.0;%30	% enter end_time_into_record (in seconds)

flag_real_data = 1;  % 0 = make synthetic signal;  1 = load real data
flag_show_data = 1;  % 0 = don't show spectrogram, etc of data;  1 = show it

flag_detect = 1;     % 0 = don't detect;  1 = detect

%
%  Set microphone locations
%


%
%   LOAD REAL DATA
%
if (flag_real_data == 1),
    [x,fs] = wavread('GCWarbler40561');
    x=x';
    xlen = length(x);
end

if (flag_show_data == 1),
%
%  ANALYZE DATA
%
   spec = stft(x(1,:),1,xlen,round(fs/100),1024,2048);
   figure(3)
   %showlevel(spec,80)
   imagesc(flipud(log(spec(1:end/2+1,:))));
%   pause
end

%
%  EXCISE DATA SEGMENT OF INTEREST
%

% startsamp = starttime*fs+1;	% time in samples to start
% endsamp =   endtime*fs;	% time in samples to end
% x = x(:,startsamp:endsamp);

%
%  RUN TF RIDGE DETECTION
%
if ( flag_detect == 1 ),
   refchannel = 1;
   binct = 512;
   binmin = ceil(500.0*binct/fs);   % sets minimum frequency to analyze above the entered value (in Hz)
   binmax = binct/2;
   [stftabs,stftdetect,stftdetectridgeback,ridge_min_time,ridge_max_time,ridge_min_freq,ridge_max_freq,speclog_prob_noise_ridge_now,speclog_prob_noise_ridge_cum_now,specoffset_ridge,specridge_link_back,specbacktrackcumsum,specbacktracktouchedflags] = tfdetect(x(refchannel,:),binct,binmin,binmax,fs);
end

flag_show_ridges = 0;
if (flag_show_ridges == 1),
   ridgect = length(ridge_min_time);
   for ii=1:ridgect,
      figure(7)
      imagesc(stftabs(ridge_min_freq(ii):ridge_max_freq(ii),ridge_min_time(ii):ridge_max_time(ii)))
      figure(8)
      imagesc(stftdetectridgeback(ridge_min_freq(ii):ridge_max_freq(ii),ridge_min_time(ii):ridge_max_time(ii)))
      %pause
   end
end


%  DONE
