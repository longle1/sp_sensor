%
%		tfdetect.m
%		Douglas L. Jones
%		May 18, 2012
%
%  tfdetect.m: This function implements a running time-frequency event detection algorithm with the following features:
%		(1) band-by-band adaptive order-statistic noise floor 
%		(2) forgetting dynamic-program ridge detector (energy or logistic)
%     (3) time-forgetting t-f block event detector (energy or logistic?)
%
%	HISTORY
%	    Started but pruned way down from robustawienertest 
%
%	DEVELOPER NOTES
%     Record summary statistics of each ridge
%
%   NOTES:
%     floor_thresh2 seems to work a bit better than floor_thresh, and is simpler; go with that
%
%     For floor_target = 0.5 (running median), detect_thresh_scale = 3.0 seems to just start to let a few random noise blips through the raw stft magnitude.  4.0 does not
%
%
%
%function [stftabs,stftdetect] = tfdetect(x,binct,binmin,binmax,fs)
function [stftabs,stftdetect,stftdetectridgeback,ridge_min_time,ridge_max_time,ridge_min_freq,ridge_max_freq,speclog_prob_noise_ridge_now,speclog_prob_noise_ridge_cum_now,specoffset_ridge,specridge_link_back,specbacktrackcumsum,specbacktracktouchedflags] = tfdetect(x,binct,binmin,binmax,fs)

%   SETUP

debugflag = 1; % 0 = don't plot out internal results; 1 = display internal results for debugging

binct2 = binct/2;
winlen = binct;	%  length of data window
blklen = binct/2;		%  number of time samples between each t-f block
blklen2 = blklen/2;

epsilon = 0.0000000001;	%  divide-by-zero control

channelct = 1; % For now, just support one data channel, but leave some capabilities in for later expansion

%  Set parameters for noise-floor tracking
floor_a = 0.99*ones(binct,1);
floor_thresh = 0.1*ones(binct,1);
floor_b = 0.02*ones(binct,1);
floor_target = 0.5*ones(binct,1);
floor_fraction = floor_target;%zeros(binct,1);

floor_thresh2 = 0.1*ones(binct,1);
alpha_updown = 0.01;
slow_scale = 0.1;
indicator_countdown = 10;
floor_up = (1+alpha_updown)*ones(binct,1);
floor_up_slow = (1+slow_scale*alpha_updown)*ones(binct,1);
floor_down = (1-alpha_updown)*ones(binct,1);

xfmag = zeros(binct,1);
indicator_thresh = zeros(binct,1);
indicator_last = zeros(binct,1);

detect_thresh_scale = 8.0*ones(binct,1);

%  Set parameters for time-frequency ridge detection
alpha_ridge = exp(-blklen/(0.1*fs))
f_ridge_down_max = ceil(20000*blklen*binct/(fs*fs))  % want X Hz/sec chirp rate; f_ridge_down_max units are freq-samples/blklen-time-samples; 1 freq-sample = fs/binct Hz
f_ridge_up_max = f_ridge_down_max
f_ridge_down = zeros(binct,1);
f_ridge_up = zeros(binct,1);
for ii=binmin:binmax,
   f_ridge_down(ii) = max(binmin,ii-f_ridge_down_max);
   f_ridge_up(ii) = min(binmax,ii+f_ridge_up_max);
end
ridge_offset_penalty_max = 0.05
ridge_offset_penalty = ridge_offset_penalty_max/max(f_ridge_up_max,f_ridge_down_max);
log_prob_noise_min_ridge = -6.0; % limits the influence of a large signal value in the ridge tracking
detect_ridge_thresh = -4.0;
detect_ridge_backtrack_thresh = -4.0;
backtracklen = ceil(0.05*fs/blklen) % enter the multiplier as the approximate track length in sec

%
%   INITIALIZE ARRAYS
%
%  Make the STFT window

%wb = ones(size(hann(winlen)));
wb = hann(winlen);
w = zeros(binct,channelct);
for ii=1:channelct,
  w(:,ii) = [zeros(1,(binct-winlen)/2) wb' zeros(1,(binct-winlen)/2)]'/max(wb);
end

%
%  Zero-pad signal
%
x = [ zeros(binct/2+1,channelct); x.'; zeros(binct/2+1,channelct) ];
xlen = max(size(x));


%
%  Declare arrays
%
blockct = ceil(xlen/blklen);

% Declare current-time frequency vectors
detected = zeros(binct,1);
detectedridge = zeros(binct,1);

log_prob_noise_ridge_now = zeros(binct,1);
log_prob_noise_ridge_cum_now = zeros(binct,1);
log_prob_noise_ridge_cum_last = zeros(binct,1);
offset_ridge = zeros(binct,1);
ridge_link_back = zeros(binct,1);
detected_ridge_label = zeros(binct,1);

% Declare time-frequency arrays
stftabs = zeros(binct,blockct);
stftnormfloor = zeros(binct,blockct);
stftdetect = zeros(binct,blockct);
stftdetectridge = zeros(binct,blockct);
stftdetectridgeback = zeros(binct,blockct);

if (debugflag == 1),
   specindicator = zeros(binct,blockct);
   specfloorfraction = zeros(binct,blockct);
   specfloorthresh = zeros(binct,blockct);
   speclog_prob_noise_ridge_now = zeros(binct,blockct);
   speclog_prob_noise_ridge_cum_now = zeros(binct,blockct);
   specoffset_ridge = zeros(binct,blockct);
   specridge_link_back = zeros(binct,blockct);
   specridge_label = zeros(binct,blockct);
end

%
%  APPLY ALGORITHM
%
epsilon = epsilon*max(max((abs(x))));

ridgect = 0;   % number of unique detected ridges
ridge_label_next = 0;
kkkk = 0;	% initialize block counter
for iiii = 1:blklen:xlen-binct,		% run through the data block by block
   kkkk = kkkk + 1;
   iiii
   xfmag = abs(fft((w.*x(iiii:iiii+binct-1,:))));

   log_prob_noise_ridge_cum_last = log_prob_noise_ridge_cum_now;
   indicator_last = indicator_thresh;
   for iii=binmin:binmax,

      %  BACKGROUND NOISE-FLOOR TRACKING
      indicator_thresh(iii) = 0;
      if (xfmag(iii) > floor_thresh2(iii)), indicator_thresh(iii) = 1; end	% check for output energy above energy threshold
      floor_fraction(iii) = floor_a(iii)*floor_fraction(iii) + (1 - floor_a(iii))*indicator_thresh(iii);	% update IIR running estimate of fraction above threshold
      floor_thresh(iii) = floor_thresh(iii)*(1.0 + floor_b(iii)*(floor_fraction(iii) - floor_target(iii)));	% update energy threshold for target fraction
      if ( xfmag(iii) > floor_thresh2(iii) ),   % implement direct up/down noise floor tracker
         indicator_last(iii) = indicator_last(iii) - 1;
         if ( indicator_last(iii) < 0 ), floor_thresh2(iii) = floor_thresh2(iii)*floor_up(iii);
         else floor_thresh2(iii) = floor_thresh2(iii)*floor_up_slow(iii);
         end
      else
         indicator_last(iii) = indicator_countdown;
         floor_thresh2(iii) = floor_thresh2(iii)*floor_down(iii);
      end

      detected(iii) = 0;
      if ( xfmag(iii) > floor_thresh2(iii)*detect_thresh_scale(iii) ), detected(iii) = 1; end

      %  TIME-VARYING SPECTRAL RIDGE TRACKING
      log_prob_noise_ridge_now(iii) = max(log_prob_noise_min_ridge,-xfmag(iii)/floor_thresh2(iii));
      [log_prob_noise_ridge_cum_now(iii),offset_ridge(iii)] = min((1 - ridge_offset_penalty*abs(iii - [f_ridge_down(iii):f_ridge_up(iii)]')).*log_prob_noise_ridge_cum_last(f_ridge_down(iii):f_ridge_up(iii)));
      log_prob_noise_ridge_cum_now(iii) = alpha_ridge*log_prob_noise_ridge_cum_last(f_ridge_down(iii)+offset_ridge(iii)-1) + (1-alpha_ridge)*log_prob_noise_ridge_now(iii);
      ridge_link_back(iii) = f_ridge_down(iii)-1+offset_ridge(iii);  % pointers to the previous ridge frequency

      % forward track detection
      detectedridge(iii) = 0;
      if ( log_prob_noise_ridge_cum_now(iii) < detect_ridge_thresh ), detectedridge(iii) = 1; end
   end

   stftabs(:,kkkk) = xfmag;
   stftdetect(:,kkkk) = detected;
   stftdetectridge(:,kkkk) = detectedridge;
   specridge_link_back(:,kkkk) = ridge_link_back;
   speclog_prob_noise_ridge_now(:,kkkk) = log_prob_noise_ridge_now;

   %
   % Backward track detection
   %
   % track back backtracklen time blocks
  if (kkkk > backtracklen),
   backtrackcumsum = log_prob_noise_ridge_now;
   backtracktouchedflags = ones(binct,1);
   for i5=kkkk:-1:kkkk-backtracklen+1,
         backtrackcumsumnew = zeros(binct,1);
         backtracktouchedflagsnew = zeros(binct,1);
      for iii=binmin:binmax,
         if ( backtracktouchedflags(iii) == 1),
            backtrackcumsumnew(specridge_link_back(iii,i5)) = min(backtrackcumsumnew(specridge_link_back(iii,i5)),backtrackcumsum(iii)+speclog_prob_noise_ridge_now(specridge_link_back(iii,i5),i5-1));   % Follow pointer back from each frequency to ridge-frequency at previous time and update backward cumsum of detection metric
            backtracktouchedflagsnew(specridge_link_back(iii,i5)) = 1;  % indicate which frequencies were part of a backtracked ridge
         end
      end
      backtrackcumsum = backtrackcumsumnew;
      backtracktouchedflags = backtracktouchedflagsnew;
   end
%if (kkkk == 104)
%  backtrackcumsum(76:85)
%  pause
%end
   backtrackcumsum = (1.0/(backtracklen+1))*backtrackcumsum;

   % find t-f locations detected as part of ridge
   detected_ridge_back = zeros(binct,1);
   detected_ridge_label = zeros(binct,1);
   for iii=binmin:binmax,
      if ( backtrackcumsum(iii) < detect_ridge_backtrack_thresh ),
         ridge_index = stftdetectridgeback(specridge_link_back(iii,kkkk-backtracklen),kkkk-backtracklen-1);
         ridge_label = specridge_label(specridge_link_back(iii,kkkk-backtracklen),kkkk-backtracklen-1);
         if ( ridge_index <= 0 ),
            ridgect = ridgect + 1;
            ridge_index = ridgect;
            ridge_min_time(ridge_index) = kkkk-backtracklen;%(kkkk-backtracklen)*blklen;
            ridge_max_time(ridge_index) = kkkk-backtracklen;%(kkkk-backtracklen)*blklen;
            ridge_min_freq(ridge_index) = iii;
            ridge_max_freq(ridge_index) = iii;
            ridge_label_next = mod(ridge_label_next+30,109);
            ridge_label = ridge_label_next;
         end
         detected_ridge_back(iii) = ridge_index;
         ridge_max_time(ridge_index) = kkkk-backtracklen;%(kkkk-backtracklen)*blklen;
         ridge_min_freq(ridge_index) = min(iii,ridge_min_freq(ridge_index));
         ridge_max_freq(ridge_index) = max(iii,ridge_max_freq(ridge_index));
         detected_ridge_label(iii) = ridge_label;
      end
   end
   stftdetectridgeback(:,kkkk-backtracklen) = detected_ridge_back;
   specridge_label(:,kkkk-backtracklen) = detected_ridge_label;
  end

   
   if (debugflag == 1),
      stftnormfloor(binmin:binmax,kkkk) = xfmag(binmin:binmax)./floor_thresh2(binmin:binmax);

      specfloorfraction(:,kkkk) = floor_fraction;
      specindicator(:,kkkk) = indicator_thresh;
      specfloorthresh(:,kkkk) = floor_thresh;
      specfloorthresh2(:,kkkk) = floor_thresh2;

      speclog_prob_noise_ridge_cum_now(:,kkkk) = log_prob_noise_ridge_cum_now;
      specoffset_ridge(:,kkkk) = offset_ridge;
      if (kkkk > backtracklen),
         specbacktrackcumsum(:,kkkk-backtracklen) = backtrackcumsum;
         specbacktracktouchedflags(:,kkkk-backtracklen) = backtracktouchedflags;
      end
   end
  
end
%
%  SHOW VARIOUS THINGS
%
debugflag = 1;
if (debugflag == 1),
   specscalepower = 0.2;
   maxshowfreqindex = binct/2;
   figure(60)
   imagesc(flipud(stftabs(1:maxshowfreqindex,:).^specscalepower))
   title('Spectrogram of signal')

   figure(61)
   imagesc(flipud(stftnormfloor(1:maxshowfreqindex,:).^specscalepower))
   title('Spectrogram of signal normalized to running noise floor')

   figure(62)
   imagesc(flipud(stftdetect(1:maxshowfreqindex,:)))
   title('Bins detected as containing significant signal')

   figure(63)
   imagesc(flipud(specfloorthresh(1:maxshowfreqindex,:).^specscalepower))
   title('Noise floor estimates')

   figure(64)
   imagesc(flipud(specindicator(1:maxshowfreqindex,:)))
   title('Bins above noise floor threshold')

   figure(65)
   specfloorfraction(binmin,1) = 1.0;
   specfloorfraction(binmin+1,1) = 0.0;
   imagesc(flipud(specfloorfraction(1:maxshowfreqindex,:)))
   title('Running estimate of fraction of bins above noise floor threshold')

   figure(66)
   imagesc(flipud(specfloorthresh2(1:maxshowfreqindex,:).^specscalepower))
   title('Noise floor estimates (type 2)')

   figure(70)
   imagesc(flipud(specridge_label(1:maxshowfreqindex,:)))
   title('Bins detected as part of t-f ridge backtracked')

   figure(71)
   imagesc(flipud(stftdetectridgeback(1:maxshowfreqindex,:)))
   title('Bins detected as part of t-f ridge backtracked')

   figure(72)
   imagesc(flipud(stftdetectridge(1:maxshowfreqindex,:)))
   title('Bins detected as part of t-f ridge')

   figure(73)
   imagesc(flipud(speclog_prob_noise_ridge_now(1:maxshowfreqindex,:)))
   title('Estimated log probability that bin contains just noise')

min(min(speclog_prob_noise_ridge_now))
max(max(speclog_prob_noise_ridge_now(binmin:binmax,:)))

   figure(74)
   imagesc(flipud(speclog_prob_noise_ridge_cum_now(1:maxshowfreqindex,:)))
   title('Estimated log probability that bin connects to no ridge')
min(min(speclog_prob_noise_ridge_cum_now))
max(max(speclog_prob_noise_ridge_cum_now(binmin:binmax,:)))

   figure(75)
   imagesc(flipud(specoffset_ridge(1:maxshowfreqindex,:)))
   title('Offset index of best connection to ridge')

   figure(76)
   imagesc(flipud(specridge_link_back(1:maxshowfreqindex,:)))
   title('Index of best connection to ridge')

   figure(77)
   imagesc(flipud(specbacktrackcumsum(1:maxshowfreqindex,:)))
   title('specbacktrackcumsum')

   figure(78)
   imagesc(flipud(specbacktracktouchedflags(1:maxshowfreqindex,:)))
   title('specbacktracktouchedflags')

end

ridgect
disp('test1')


%  DONE

