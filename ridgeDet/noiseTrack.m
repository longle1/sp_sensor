function floor_track = noiseTrack(sig_stft, varargin)
% floor_track = noiseTrack(sig_stft, varargin)
%
% Doug's algorithm for noise floor tracking
%

if nargin == 1
    alpha_updown = 0.05;
    slow_scale = 0.5;
    indicator_countdown = 2;
    iscorr = false;
else
    alpha_updown = varargin{1}; 
    slow_scale = varargin{2};
    indicator_countdown = varargin{3};
    iscorr = varargin{4};
end
bins = size(sig_stft, 1);
floor_up = (1+alpha_updown)*ones(bins,1);
floor_up_slow = (1+slow_scale*alpha_updown)*ones(bins,1);
floor_down = (1-alpha_updown)*ones(bins,1);

floor_thresh = ones(bins,1);
indicator_last = zeros(bins,1);
floor_track = zeros(size(sig_stft));
for i = 1:size(sig_stft,2)
    for iii = 1:bins
        if ( sig_stft(iii,i) > floor_thresh(iii) ),  
            indicator_last(iii) = indicator_last(iii) - 1;
            if ( indicator_last(iii) < 0 ) % speech is highly probable, slow noise track
                floor_thresh(iii) = floor_thresh(iii)*floor_up_slow(iii);
            else % transient noise is highly probable
                floor_thresh(iii) = floor_thresh(iii)*floor_up(iii);
            end
        else % background noise is highly probable
            indicator_last(iii) = indicator_countdown;
            floor_thresh(iii) = floor_thresh(iii)*floor_down(iii);
        end
    end
    if (iscorr)
        freqCorr = xcorr(floor_thresh, 'coeff');
        freqCorr = [freqCorr(ceil(length(freqCorr)/2):end); freqCorr(1:floor(length(freqCorr)/2))];
        H = toeplitz(freqCorr);
        floor_track(:,i) = H(1:bins,1:bins)*floor_thresh;
    else
        floor_track(:,i) = floor_thresh;
    end
end
