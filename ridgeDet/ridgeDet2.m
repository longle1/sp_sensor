function [ridgetracker, spec, speclog_prob_noise_ridge, specbacktrackcumsum] = ridgeDet2(x, fs, thresh, frameLen)

Nblk = 128;%2^floor(log2(frameLen*fs));
Nfc = 128;
w = hann(Nblk*2)';

% Parameters
noisetracker.alpha_updown = 0.01;
noisetracker.slow_scale = 0.1;
noisetracker.indicator_countdown = 10;
noisetracker.floor_up = (1+noisetracker.alpha_updown)*ones(Nfc,1);
noisetracker.floor_up_slow = (1+noisetracker.slow_scale*noisetracker.alpha_updown)*ones(Nfc,1);
noisetracker.floor_down = (1-noisetracker.alpha_updown)*ones(Nfc,1);
noisetracker.floor_thresh = 0.1*ones(Nfc,1);
noisetracker.indicator_last = zeros(Nfc,1);

ridgetracker.alpha_ridge = exp(-Nblk/(0.1*fs));
ridgetracker.f_ridge_down_max = ceil(20000*Nblk*Nfc*2/(fs*fs)); % want X Hz/sec chirp rate; f_ridge_down_max units are freq-samples/blklen-time-samples; 1 freq-sample = fs/binct Hz
ridgetracker.f_ridge_up_max = ridgetracker.f_ridge_down_max;
ridgetracker.f_ridge_down = zeros(Nfc,1);
ridgetracker.f_ridge_up = zeros(Nfc,1);
for ii=1:Nfc,
   ridgetracker.f_ridge_down(ii) = max(1,ii-ridgetracker.f_ridge_down_max);
   ridgetracker.f_ridge_up(ii) = min(Nfc,ii+ridgetracker.f_ridge_up_max);
end
ridgetracker.ridge_offset_penalty_max = 0.1;
ridgetracker.ridge_offset_penalty = ridgetracker.ridge_offset_penalty_max/max(ridgetracker.f_ridge_up_max,ridgetracker.f_ridge_down_max);
ridgetracker.log_prob_noise_min_ridge = -10.0;
ridgetracker.detect_ridge_backtrack_thresh = thresh;
ridgetracker.backtracklen = ceil(frameLen*4*fs/Nblk); % enter the multiplier as the approximate track length in sec
% States
ridgetracker.log_prob_noise_ridge_cum_now = zeros(Nfc,1);
ridgetracker.specridge_link_back = zeros(Nfc,ridgetracker.backtracklen+1);
ridgetracker.speclog_prob_noise_ridge = zeros(Nfc,ridgetracker.backtracklen+1);
ridgetracker.stftdetectridgeback = zeros(Nfc,2);
ridgetracker.ridge_min_time = zeros(1,0);
ridgetracker.ridge_max_time = zeros(1,0);
ridgetracker.ridge_min_freq = zeros(1,0);
ridgetracker.ridge_max_freq = zeros(1,0);
ridgetracker.ridge_loglik = zeros(1,0);
ridgetracker.ridge_desc = cell(1,0);
ridgetracker.ridgect = 0;
ridgetracker.ridge_cap = 5;

% Main
data = enframe(x,Nblk*2, Nblk*2); data = [data; randn(ridgetracker.backtracklen, size(data, 2))*0.0001]; % pad data for finalization
spec = zeros(Nfc, size(data,1));
spec_noise_floor = zeros(Nfc, size(data,1));
speclog_prob_noise_ridge = zeros(Nfc, size(data,1));
specbacktrackcumsum = zeros(Nfc, size(data,1));
specbacktracktouchedflags = zeros(Nfc, size(data,1));
stftdetectridgeback = zeros(Nfc, size(data,1));
for k = 1:size(data,1)
    s = abs(fft(w.*data(k,:)));
    s = s(1:Nfc);
    spec(:,k) = s;

    offset_ridge = zeros(Nfc,1);
    ridge_link_back = zeros(Nfc,1);
    log_prob_noise_ridge = zeros(Nfc,1);
    log_prob_noise_ridge_cum_last = ridgetracker.log_prob_noise_ridge_cum_now;
    for j=1:Nfc
        % noise tracking
        if(s(j) > noisetracker.floor_thresh(j))
            noisetracker.indicator_last(j) = noisetracker.indicator_last(j)-1;
            if(noisetracker.indicator_last(j) < 0) % signal is highly probable, slow down
                noisetracker.floor_thresh(j) = noisetracker.floor_thresh(j)*noisetracker.floor_up_slow(j);
            else % transient signal is highly probable
                noisetracker.floor_thresh(j) = noisetracker.floor_thresh(j)*noisetracker.floor_up(j);
            end
        else % background noise is highly probable
            noisetracker.indicator_last(j) = noisetracker.indicator_countdown;
            noisetracker.floor_thresh(j) = noisetracker.floor_thresh(j)*noisetracker.floor_down(j);
        end

        % debugging
        spec_noise_floor(:,k) = noisetracker.floor_thresh;

        %  TIME-VARYING SPECTRAL RIDGE TRACKING
        log_prob_noise_ridge(j) = max(ridgetracker.log_prob_noise_min_ridge,-s(j)^2/noisetracker.floor_thresh(j));
        [~, offset_ridge(j)] = ...
            min((1 - ridgetracker.ridge_offset_penalty*abs(j - [ridgetracker.f_ridge_down(j):ridgetracker.f_ridge_up(j)]')).*...
            log_prob_noise_ridge_cum_last(ridgetracker.f_ridge_down(j):ridgetracker.f_ridge_up(j)));

        ridgetracker.log_prob_noise_ridge_cum_now(j) = ridgetracker.alpha_ridge*log_prob_noise_ridge_cum_last(ridgetracker.f_ridge_down(j)+offset_ridge(j)-1) + ...
              (1-ridgetracker.alpha_ridge)*log_prob_noise_ridge(j);
        ridge_link_back(j) = ridgetracker.f_ridge_down(j)+offset_ridge(j)-1;  % pointers to the previous ridge frequency
    end
    ridgetracker.specridge_link_back = [ridgetracker.specridge_link_back(:,2:end) ridge_link_back];
    ridgetracker.speclog_prob_noise_ridge = [ridgetracker.speclog_prob_noise_ridge(:,2:end) log_prob_noise_ridge];

    % debugging
    speclog_prob_noise_ridge(:,k) = log_prob_noise_ridge;

    %
    % Backward track detection
    %
    % track back backtracklen time blocks
    backtrackcumsum = log_prob_noise_ridge;
    backtracktouchedflags = ones(Nfc,1);
    for i5=ridgetracker.backtracklen+1:-1:2,
      backtrackcumsumnew = zeros(Nfc,1);
      backtracktouchedflagsnew = zeros(Nfc,1);
      % Follow pointer back from each frequency to ridge-frequency at previous time and update backward cumsum of detection metric
      for iii=1:Nfc
         if ( backtracktouchedflags(iii) == 1),
            if ridgetracker.specridge_link_back(iii,i5) == 0
                continue;
            end
            backtrackcumsumnew(ridgetracker.specridge_link_back(iii,i5)) = ...
                min(backtrackcumsumnew(ridgetracker.specridge_link_back(iii,i5)),...
                backtrackcumsum(iii)+ridgetracker.speclog_prob_noise_ridge(ridgetracker.specridge_link_back(iii,i5),i5-1));
            backtracktouchedflagsnew(ridgetracker.specridge_link_back(iii,i5)) = 1;  % indicate which frequencies were part of a backtracked ridge
         end
      end
      backtrackcumsum = backtrackcumsumnew;
      backtracktouchedflags = backtracktouchedflagsnew;
    end
    backtrackcumsum = (1.0/(ridgetracker.backtracklen+1))*backtrackcumsum;

    % debugging
    if (k > ridgetracker.backtracklen)
        specbacktrackcumsum(:,k-ridgetracker.backtracklen) = backtrackcumsum;
        specbacktracktouchedflags(:,k-ridgetracker.backtracklen) = backtracktouchedflags;
    end

    %
    % find t-f locations detected as part of ridge
    %
    detected_ridge_back = zeros(Nfc,1);
    for iii=1:Nfc
      if ( backtrackcumsum(iii) <= ridgetracker.detect_ridge_backtrack_thresh ),
         if ridgetracker.specridge_link_back(iii,1) == 0
             continue;
         end
         % pickup from the previous ridge_index
         ridge_index = ridgetracker.stftdetectridgeback(ridgetracker.specridge_link_back(iii,1),2);
         if ( ridge_index == 0 ) 
            ridge_index = -1;

            for l = iii:-1:ridgetracker.f_ridge_down(iii)
                if (detected_ridge_back(l) > 0)
                    ridge_index = detected_ridge_back(l);
                    break;
                end
            end
            if (ridge_index == -1) % still no luck, create a new label
                ridgetracker.ridgect = ridgetracker.ridgect + 1;
                ridge_index = ridgetracker.ridgect;
            end

         end         
         detected_ridge_back(iii) = ridge_index;

      end
    end

    %[detected_ridge_back, ridgetracker] = mergeNodes(detected_ridge_back, ridgetracker);

    curr_min_freq = (Nfc+1)*ones(1, length(ridgetracker.ridge_loglik)); % Reinit every time, one-time init is not enough
    curr_max_freq = zeros(1, length(ridgetracker.ridge_loglik));
    for iii=1:Nfc
        ridge_index = detected_ridge_back(iii);
        if (ridge_index > 0)
            if (ridge_index > length(ridgetracker.ridge_loglik))
                curr_min_freq(ridge_index) = iii;
                curr_max_freq(ridge_index) = iii;

                ridgetracker.ridge_min_time(ridge_index) = k-ridgetracker.backtracklen;
                ridgetracker.ridge_max_time(ridge_index) = k-ridgetracker.backtracklen;
                ridgetracker.ridge_min_freq(ridge_index) = iii;
                ridgetracker.ridge_max_freq(ridge_index) = iii;
                ridgetracker.ridge_loglik(ridge_index) = backtrackcumsum(iii);
                ridgetracker.ridge_desc{ridge_index} = [];
            else
                curr_min_freq(ridge_index) = min(iii, curr_min_freq(ridge_index));
                curr_max_freq(ridge_index) = max(iii, curr_max_freq(ridge_index));

                ridgetracker.ridge_max_time(ridge_index) = k-ridgetracker.backtracklen;
                ridgetracker.ridge_min_freq(ridge_index) = min(iii,ridgetracker.ridge_min_freq(ridge_index));
                ridgetracker.ridge_max_freq(ridge_index) = max(iii,ridgetracker.ridge_max_freq(ridge_index));
                ridgetracker.ridge_loglik(ridge_index) = ridgetracker.ridge_loglik(ridge_index)+backtrackcumsum(iii);
            end
        end
    end
    ridgetracker.stftdetectridgeback = [ridgetracker.stftdetectridgeback(:,2) detected_ridge_back];
    
    ridge_index_set = unique(detected_ridge_back);
    for l = 1:numel(ridge_index_set)
        if (ridge_index_set(l) == 0) % there might be case that ridge_index_set(1) is not 0
            continue;
        end
        ridgetracker.ridge_desc{ridge_index_set(l)} = [ridgetracker.ridge_desc{ridge_index_set(l)} [curr_min_freq(ridge_index_set(l)); curr_max_freq(ridge_index_set(l))]];
    end

    % debugging
    if (k > ridgetracker.backtracklen)
        stftdetectridgeback(:,k-ridgetracker.backtracklen) = detected_ridge_back;
    end
end

% Keep only top ridges
[~, idx] = sort(ridgetracker.ridge_loglik);
ridgetracker.ridge_min_time(idx(ridgetracker.ridge_cap+1:end)) = [];
ridgetracker.ridge_max_time(idx(ridgetracker.ridge_cap+1:end)) = [];
ridgetracker.ridge_min_freq(idx(ridgetracker.ridge_cap+1:end)) = [];
ridgetracker.ridge_max_freq(idx(ridgetracker.ridge_cap+1:end)) = [];
ridgetracker.ridge_loglik(idx(ridgetracker.ridge_cap+1:end)) = [];
ridgetracker.ridge_desc(idx(ridgetracker.ridge_cap+1:end)) = [];

% Highlight time-frequency blocks
ridgetracker.B = cell(1, numel(ridgetracker.ridge_desc)); % ridge boundaries
for k = 1:numel(ridgetracker.ridge_desc)
    lowfreq = ridgetracker.ridge_desc{k}(1,:);
    highfreq = ridgetracker.ridge_desc{k}(2,:);
    ridgetime = [ridgetracker.ridge_min_time(k):ridgetracker.ridge_max_time(k)];
    % Connected points go from start, lowfreq, connect, then backward high freq
    ridgetracker.B{k} = [lowfreq' ridgetime';...
             flipud([highfreq' ridgetime']);...
             lowfreq(1) ridgetracker.ridge_min_time(k)];
end
