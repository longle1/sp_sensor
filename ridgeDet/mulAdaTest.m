% Test multiplicative threshold adaptation
%
% Long Le
% University of Illinois
%

clear all; close all;

N = 200;
O = 0.5; % Original magnitude

x = O*ones(1, N);
y = O*ones(1, N);
for k =1:fix(N/2)-1
    x(k+1) = x(k)*1.05;
    y(k+1) = y(k)*1.025;
end

for k = fix(N/2):N-1
    x(k+1) = x(k)*0.95;
    y(k+1) = y(k)*0.95;
end

figure; hold on;
plot(x, 'r');
plot(y, 'g');
plot(z, 'b');