clear all

[signal,fs] = wavread('noisefloor.wav');
signal = signal(:,1);

bins = 256;

sig_stft = stft(signal,1,length(signal),bins/2,bins,bins); 

alpha_updown = 0.05;%0.01;
slow_scale = 0.5;
indicator_countdown = 2;
floor_up = (1+alpha_updown)*ones(bins,1);
floor_up_slow = (1+slow_scale*alpha_updown)*ones(bins,1);
floor_down = (1-alpha_updown)*ones(bins,1);
wiener_min = 0.1*ones(bins,1);
wiener_scale = 0.1*ones(bins,1);

xfmag2 = zeros(bins,1);
floor_thresh = ones(bins,1);
wienerweights = zeros(bins,1);
indicator_last = zeros(bins,1);

floor_track = zeros(size(sig_stft));

for i = 1:size(sig_stft,2)
    for iii = 1:bins
        if ( sig_stft(iii,i) > floor_thresh(iii) ),  
            indicator_last(iii) = indicator_last(iii) - 1;
            if ( indicator_last(iii) < 0 ),
                floor_thresh(iii) = floor_thresh(iii)*floor_up_slow(iii);
            else
                floor_thresh(iii) = floor_thresh(iii)*floor_up(iii);
            end
        else
            indicator_last(iii) = indicator_countdown;
            floor_thresh(iii) = floor_thresh(iii)*floor_down(iii);
        end
    end
    
    floor_track(:,i) = floor_thresh;
end

figure(1)
imagesc(log(floor_track))

figure(2)
imagesc(log(sig_stft))