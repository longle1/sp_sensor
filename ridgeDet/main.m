% Main script for acoustic search
% Based on Doug's runloc2.m script.
%
% Long Le
% University of Illinois
%
clear all; close all
%addpath('../voicebox/','-end');
rng('default')

%% Low level feature detection/description
%{
folder = {'../icassp2014/TIMIT/TRAIN/DR1/MPSW0/'};%, '../icassp2014/TIMIT/TRAIN/DR1/FSJK1/'};
file = cell(1, numel(folder));
nfile = cell(1, numel(folder));
for k = 1:numel(folder)
	file{k} = dir(fullfile(folder{k}, '*.wav'));
    nfile{k} = length(file{k});
end

fs = 16000;
s = cell(1, sum(cell2mat(nfile)));
lab = cell(1, sum(cell2mat(nfile)));
sIdx = 1;
for k = 1:numel(folder)
    for l = 1:nfile{k}
        tmp = fullfile(folder{k}, file{k}(l).name);
        [sTmp, fsTmp] = readsph(tmp);
        s{sIdx} = resample(sTmp, fs, fsTmp);
        lab{sIdx} = logical(ones(size(s{sIdx})));
        sIdx = sIdx + 1;
    end
end

sPad = cell(size(s));
labPad = cell(size(lab));
for k = 1:numel(sPad)
    % Pad space around speech randomly from 1 to 5 seconds
    tmp = randi([1*fs 5*fs]);
    sPad{k} = padarray(s{k}, tmp, 0, 'both');
    labPad{k} = padarray(lab{k}, tmp, 0, 'both');
end

% Add Gaussian noise
nScale = 0.02;
sigN = zeros(size(sPad));
n = cell(size(sPad));
x = cell(size(sPad));
for k = 1:numel(sPad)
    sigN(k) = nScale*rand(1);
    n{k} = randn(size(sPad{k}))*sigN(k);
    x{k} = n{k} + sPad{k};
end
%}
[x{1}, fs] = wavread('GCWarbler40561.wav');
%[x{1}, fs] = wavread('../icassp2014/noise/noise1.wav');

desctor = cell(1, numel(x));
for k = 1%:numel(s)
    binct = 512;
    binmin = ceil(100.0*binct/fs);   % sets minimum frequency to analyze above the entered value (in Hz)
    binmax = binct/2;
    [stftabs, ridge_min_time,ridge_max_time,ridge_min_freq,ridge_max_freq] = ridgeDetect(x{k},binct,binmin,binmax,fs,-4.0);

    % Remove trivial ridges
    idxT = ridge_max_time > ridge_min_time;
    idxF = ridge_max_freq > ridge_min_freq;
    idx = idxT & idxF;
    ridgect = sum(idx)
    ridge_min_time(~idx) = [];
    ridge_max_time(~idx) = [];
    ridge_min_freq(~idx) = [];
    ridge_max_freq(~idx) = [];
    desctor{k} = [ridge_min_time; ridge_max_time; ridge_min_freq; ridge_max_freq];
    
    % Highlight time-frequency blocks
    fh = figure; hold on;
    imagesc(log(stftabs(1:binmax,:)))
    for l = 1:ridgect
        rectangle('Position', [ridge_min_time(l) ridge_min_freq(l) ...
            ridge_max_time(l)-ridge_min_time(l) ridge_max_freq(l)-ridge_min_freq(l)], ...
            'EdgeColor', 'k')
    end
    axis('tight')
    title(sprintf('Ridge counts: %d', ridgect));
    ytick = [1:(binmax-1)/5:binmax];
    set(gca, 'YTick', ytick);
    set(gca, 'YTickLabel', num2cell(ytick/binmax*fs/2));
    xtick = [1:(blockct-1)/5:blockct];
    set(gca, 'XTick', xtick);
    set(gca, 'XTickLabel', num2cell(xtick*blklen/fs));
end

%% 