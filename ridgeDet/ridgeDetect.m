function [stftabs, ridge_min_time,ridge_max_time,ridge_min_freq,ridge_max_freq] = ridgeDetect(x,varargin)
%function [stftabs, ridge_min_time,ridge_max_time,ridge_min_freq,ridge_max_freq] = tfdetect(x,binct,binmin,binmax,fs,detect_ridge_backtrack_thresh)
%
%		tfdetect.m
%		Douglas L. Jones
%		May 18, 2012
%
%  tfdetect.m: This function implements a running time-frequency event detection algorithm with the following features:
%		(1) band-by-band adaptive order-statistic noise floor 
%		(2) forgetting dynamic-program ridge detector (energy or logistic)
%     (3) time-forgetting t-f block event detector (energy or logistic?)
%
%	HISTORY
%	    Started but pruned way down from robustawienertest 
%
%	DEVELOPER NOTES
%     Record summary statistics of each ridge
%
%   NOTES:
%     floor_thresh2 seems to work a bit better than floor_thresh, and is simpler; go with that
%
%     For floor_target = 0.5 (running median), detect_thresh_scale = 3.0 seems to just start to let a few random noise blips through the raw stft magnitude.  4.0 does not
%
%   EDIT BY: Long Le
%

binct = varargin{1};
binmin = varargin{2};
binmax = varargin{3};
fs = varargin{4};

binct2 = binct/2;
winlen = binct;	%  length of data window
blklen = binct/2;		%  number of time samples between each t-f block
blklen2 = blklen/2;

% Set parameters for noise-floor tracking
floor_a = 0.99*ones(binct,1);
floor_b = 0.02*ones(binct,1);
floor_target = 0.5*ones(binct,1);

floor_thresh2 = 0.1*ones(binct,1);
alpha_updown = 0.01;
slow_scale = 0.1;
indicator_countdown = 10;
floor_up = (1+alpha_updown)*ones(binct,1);
floor_up_slow = (1+slow_scale*alpha_updown)*ones(binct,1);
floor_down = (1-alpha_updown)*ones(binct,1);

xfmag = zeros(binct,1);
indicator_last = zeros(binct,1);

detect_thresh_scale = 8.0*ones(binct,1);

% Set parameters for time-frequency ridge detection
alpha_ridge = exp(-blklen/(0.1*fs));
f_ridge_down_max = ceil(20000*blklen*binct/(fs*fs));  % want X Hz/sec chirp rate; f_ridge_down_max units are freq-samples/blklen-time-samples; 1 freq-sample = fs/binct Hz
f_ridge_up_max = f_ridge_down_max;
f_ridge_down = zeros(binct,1);
f_ridge_up = zeros(binct,1);
for ii=binmin:binmax,
   f_ridge_down(ii) = max(binmin,ii-f_ridge_down_max);
   f_ridge_up(ii) = min(binmax,ii+f_ridge_up_max);
end
ridge_offset_penalty_max = 0.05;
ridge_offset_penalty = ridge_offset_penalty_max/max(f_ridge_up_max,f_ridge_down_max);
log_prob_noise_min_ridge = -6.0; % limits the influence of a large signal value in the ridge tracking
detect_ridge_backtrack_thresh = varargin{5};
backtracklen = ceil(0.05*fs/blklen); % enter the multiplier as the approximate track length in sec

%
%   INITIALIZE ARRAYS
%
%  Make the STFT window

wb = hann(winlen);
w = [zeros((binct-winlen)/2, 1); wb; zeros((binct-winlen)/2, 1)]/max(wb);

%
%  Zero-pad signal
%
%x = [ zeros(binct/2+1,1); x; zeros(binct/2+1,1) ];
xlen = max(size(x));

%
%  Declare arrays
%
blockct = ceil(xlen/blklen);

% Declare current-time frequency vectors
detected = zeros(binct,1);
detectedridge = zeros(binct,1);

log_prob_noise_ridge = zeros(binct,1);
log_prob_noise_ridge_cum_now = zeros(binct,1);
log_prob_noise_ridge_cum_last = zeros(binct,1);
offset_ridge = zeros(binct,1);
ridge_link_back = zeros(binct,1);

% Declare time-frequency arrays
stftabs = zeros(binct,blockct);
stftnormfloor = zeros(binct,blockct);
stftdetect = zeros(binct,blockct);
stftdetectridge = zeros(binct,blockct);
stftdetectridgeback = zeros(binct,blockct);

specindicator = zeros(binct,blockct);
specfloorfraction = zeros(binct,blockct);
specfloorthresh2 = zeros(binct,blockct);
speclog_prob_noise_ridge = zeros(binct,blockct);
speclog_prob_noise_ridge_cum_now = zeros(binct,blockct);
specoffset_ridge = zeros(binct,blockct);
specridge_link_back = zeros(binct,blockct);
specridge_label = zeros(binct,blockct);
specbacktrackcumsum = zeros(binct,blockct);
specbacktracktouchedflags = zeros(binct,blockct);

%%  APPLY ALGORITHM

ridgect = 0;   % number of unique detected ridges
kkkk = 0;	% initialize block counter
for iiii = 1:blklen:xlen-binct,		% run through the data block by block
   kkkk = kkkk + 1;
   iiii;
   xfmag = abs(fft((w.*x(iiii:iiii+binct-1))));

   log_prob_noise_ridge_cum_last = log_prob_noise_ridge_cum_now;
   for iii=binmin:binmax,

      %  BACKGROUND NOISE-FLOOR TRACKING
      if ( xfmag(iii) > floor_thresh2(iii) ),   % implement direct up/down noise floor tracker
         indicator_last(iii) = indicator_last(iii) - 1;
         if ( indicator_last(iii) < 0 ), floor_thresh2(iii) = floor_thresh2(iii)*floor_up_slow(iii);
         else floor_thresh2(iii) = floor_thresh2(iii)*floor_up(iii);
         end
      else
         indicator_last(iii) = indicator_countdown;
         floor_thresh2(iii) = floor_thresh2(iii)*floor_down(iii);
      end

      detected(iii) = 0;
      if ( xfmag(iii) > floor_thresh2(iii)*detect_thresh_scale(iii) ), detected(iii) = 1; end

      %  TIME-VARYING SPECTRAL RIDGE TRACKING
      log_prob_noise_ridge(iii) = max(log_prob_noise_min_ridge,-xfmag(iii)^2/floor_thresh2(iii));
      [~, offset_ridge(iii)] = ...
        min((1 - ridge_offset_penalty*abs(iii - [f_ridge_down(iii):f_ridge_up(iii)]')).*...
        log_prob_noise_ridge_cum_last(f_ridge_down(iii):f_ridge_up(iii)));
    
      log_prob_noise_ridge_cum_now(iii) = alpha_ridge*log_prob_noise_ridge_cum_last(f_ridge_down(iii)+offset_ridge(iii)-1) + ...
          (1-alpha_ridge)*log_prob_noise_ridge(iii);
      ridge_link_back(iii) = f_ridge_down(iii)+offset_ridge(iii)-1;  % pointers to the previous ridge frequency
   end

   stftabs(:,kkkk) = xfmag;
   stftdetect(:,kkkk) = detected;
   specridge_link_back(:,kkkk) = ridge_link_back;
   speclog_prob_noise_ridge(:,kkkk) = log_prob_noise_ridge;

    %
    % Backward track detection
    %
    % track back backtracklen time blocks
    if (kkkk > backtracklen + 1),
        backtrackcumsum = log_prob_noise_ridge;
        backtracktouchedflags = ones(binct,1);
        for i5=kkkk:-1:kkkk-backtracklen+1,
          backtrackcumsumnew = zeros(binct,1);
          backtracktouchedflagsnew = zeros(binct,1);
          % Follow pointer back from each frequency to ridge-frequency at previous time and update backward cumsum of detection metric
          for iii=binmin:binmax,
             if ( backtracktouchedflags(iii) == 1),
                backtrackcumsumnew(specridge_link_back(iii,i5)) = ...
                    min(backtrackcumsumnew(specridge_link_back(iii,i5)),backtrackcumsum(iii)+speclog_prob_noise_ridge(specridge_link_back(iii,i5),i5-1));
                backtracktouchedflagsnew(specridge_link_back(iii,i5)) = 1;  % indicate which frequencies were part of a backtracked ridge
             end
          end
          backtrackcumsum = backtrackcumsumnew;
          backtracktouchedflags = backtracktouchedflagsnew;
        end
        backtrackcumsum = (1.0/(backtracklen+1))*backtrackcumsum;

        % find t-f locations detected as part of ridge
        detected_ridge_back = zeros(binct,1);
        for iii=binmin:binmax,
          if ( backtrackcumsum(iii) < detect_ridge_backtrack_thresh ),
             % pickup from the previous ridge_index
             ridge_index = stftdetectridgeback(specridge_link_back(iii,kkkk-backtracklen),kkkk-backtracklen-1);
             if ( ridge_index == 0 ),
                ridgect = ridgect + 1;
                ridge_index = ridgect;
                ridge_min_time(ridge_index) = kkkk-backtracklen;%(kkkk-backtracklen)*blklen;
                ridge_max_time(ridge_index) = kkkk-backtracklen;%(kkkk-backtracklen)*blklen;
                ridge_min_freq(ridge_index) = iii;
                ridge_max_freq(ridge_index) = iii;
             end
             ridge_max_time(ridge_index) = kkkk-backtracklen;%(kkkk-backtracklen)*blklen;
             ridge_min_freq(ridge_index) = min(iii,ridge_min_freq(ridge_index));% compare between new freq and current min.
             ridge_max_freq(ridge_index) = max(iii,ridge_max_freq(ridge_index));% compare between new freq and current max.
             detected_ridge_back(iii) = ridge_index;
          end
        end
        stftdetectridgeback(:,kkkk-backtracklen) = detected_ridge_back;
    end

    stftnormfloor(binmin:binmax,kkkk) = xfmag(binmin:binmax)./floor_thresh2(binmin:binmax);

    specfloorthresh2(:,kkkk) = floor_thresh2;

    speclog_prob_noise_ridge_cum_now(:,kkkk) = log_prob_noise_ridge_cum_now;
    specoffset_ridge(:,kkkk) = offset_ridge;
    if (kkkk > backtracklen + 1),
        specbacktrackcumsum(:,kkkk-backtracklen) = backtrackcumsum;
        specbacktracktouchedflags(:,kkkk-backtracklen) = backtracktouchedflags;
    end
    
end