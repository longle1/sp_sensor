% Generate labels by manually setting threshold
%
% Long Le
% University of Illinois
%

clear all; close all;

folder = '../icassp2014/noise/';
file = 'noise1.wav';
[x, fs] = wavread(fullfile(folder, file));
xAbs = abs(x);
thresh = 2.5e-3;
ind = xAbs >= 3.0e-3;
al = 0.99;
indfilt = filter(1-al, [1 -al], ind);
thresh2 = 0.5;
lab = indfilt >= thresh2;

figure;
subplot(311); plot([1:numel(xAbs)]/fs, xAbs);
hold on; plot([1:numel(xAbs)]/fs, ones(size(xAbs))*thresh, 'r'); axis('tight')
subplot(312); plot([1:numel(indfilt)]/fs, indfilt); axis('tight')
subplot(313); plot([1:numel(lab)]/fs, lab); axis('tight')

[~,fname,~] = fileparts(file);
labfile = [fname '.mat'];
save(fullfile(folder,labfile), 'lab', 'fs');
