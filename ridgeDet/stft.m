%
%		stft.m
%		Douglas L. Jones
%		University of Illinois
%		October 13, 1993
%
%	stft: This program implements a short-time Fourier transform
%
%	parameters:
%	    inputs:
%		xin:	data sequence to be analyzed
%		ist:	time index of first output slice
%		iend:	time index of last output slice
%		iinc:	step size between output slices
%		blen:	length of short-time data block (Hamming window length)
%		N:	number of frequency samples in output (zero-padded FFT length)
%	    output:
%		smag:	matrix containing |STFT|
%
%function   s2 = stft(xin,ist,iend,iinc,blen,N)
%
function   s2 = stft(x,ist,iend,iinc,blen,N)

%
%   SETUP
%

w = hann(blen)';

size(x);		%  zero pad input data sequence
if (ans(1) == 1),
    x = [zeros(1,blen/2) x zeros(1,blen/2)];
  else
    x = [zeros(1,blen/2) x' zeros(1,blen/2)];
end

s2 = zeros(N,floor((iend-ist)/iinc + 1));

%
%   COMPUTE STFT
%

jjj = 1;
for i=ist:iinc:iend
  s2(:,jjj) = abs(fft(x(i:i+blen-1).*w,N))';
  jjj = jjj + 1;
  end

s2 = s2.*s2;

% Done

