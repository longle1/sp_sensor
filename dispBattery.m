function [tt,batteryLevel] = dispBattery(events)
% function [tt,batteryLevel] = dispBattery(events,DATA,EVENT)
%
% Display battery level of events in temporal order
%
% Long Le <longle1@illinois.edu>
% University of Illinois

%% select events
usTt = zeros(1,0);
usBatteryLevel = zeros(1,0);

for k = 1:numel(events)
    usTt(k) = datenum8601(events{k}.recordDate);
    usBatteryLevel(k) = events{k}.batteryLevel;
end
[tt, idx] = sort(usTt);
batteryLevel = usBatteryLevel(idx);

plot((tt-tt(1))*24, batteryLevel);
xlabel('Time elapsed (hours)'); ylabel('Battery level')
fprintf(1,'Time elapsed: %.2f hours\n', (tt(end)-tt(1))*24);