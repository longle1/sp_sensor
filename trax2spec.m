function spec = trax2spec(R,M,S)
%
% Trax to spec
% 
% Long Le <longle1@illinois.edu>
% University of Illinois
%
spec = zeros(size(S));
for k = 1:size(R,1)
    tmp = ~isnan(R(k,:));
    validR = round(R(k,tmp));
    validT = find(tmp);
    validM = M(k,tmp);
    try
    spec(sub2ind(size(spec),validR, validT)) = validM;
    catch e
        disp('hei')
    end
end