function manLabel()
% Manually label patterns in spectrogram 
% using Derek Hoiem's labeling tool
%
% Long Le
% University of Illinois
%

clear all; %close all
addpath('../voicebox/');
addpath('objectLabelingTool');

datSrc = 2;
if datSrc == 1
    datFolder = '../genCascade/data/GCW/';
    file = dir([datFolder '*.wav']);
    filename = cell(numel(file),1);
    for k = 1:numel(file)
        filename{k} = [datFolder file(k).name];
    end
    outdir = 'label/GCW/';
elseif datSrc == 2
    datFolder = '../acousticsearch/data/whistlecommand/';
    file = dir([datFolder '*.wav']);
    filename = cell(numel(file),1);
    for k = 1:numel(file)
        filename{k} = [datFolder file(k).name];
    end
    outdir = 'label/whistlecommand/';
elseif datSrc == 3
    datFolder = '../matlab-realtime-audioprocessing/TIDIGIT_adults_crop_crop/';
    file = dir([datFolder '*.wav']);
    filename = cell(numel(file),1);
    for k = 1:numel(file)
        filename{k} = [datFolder file(k).name];
    end
    outdir = 'label/TIDIGIT_adults_crop_crop/';
end

outext = '_objects.mat';
if ~exist(outdir, 'dir')
    disp(['creating ' outdir])
    try 
        mkdir(outdir);
    catch
        disp(['could not create ' outdir]);
    end
end

rng('default')

%% Label
for expIdx = 1:numel(filename)
     sprintf('expIdx: %d filename: %s', expIdx, file(expIdx).name)
    
    % read data
    try
        [y, fs] = wavread(filename{expIdx});
    catch MException
        [y, fs] = readsph(filename{expIdx});
    end
    y = y(:,1);
    if (fs > 20000)
        h = fir1(8, 20000/fs); % low pass filter
        y = filter(h, 1, y);
        y = resample(y, 20000, fs);
        fs = 20000;
    end
    x = y + randn(size(y))*std(y)/sqrt(1000);
    
    Nblk = 128;
    Nfc = 128;
    w = hann(Nblk*2)';
    
    % create spectrogram
    data = enframe(x,Nblk*2, Nblk*2);
    spec = zeros(Nfc, size(data,1));
    for k = 1:size(data,1)
        s = abs(fft(w.*data(k,:)));
        s = s(1:Nfc);
        spec(:,k) = s;
    end
    %figure; imagesc([1:size(data,1)], [1:Nfc], spec)
    im = gray2rgb(mat2gray(spec), colormap(jet));
    %im = repmat(mat2gray(spec),[1 1 3]);
    
    % manual labeling
    outname = fullfile(outdir, [strtok(file(expIdx).name, '.') outext]);
    if exist(outname, 'file') % edit
        load(outname);
        objects = objectLabelingTool(im, objects); 
    else % create new
        objects = objectLabelingTool(im);
    end
    save(outname, 'objects');
end
