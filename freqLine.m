function freqLine()
% Frequency line, ridge, blob detector
%
% Long Le
% University of Illinois
% longle1@illinois.edu
%

clear all; close all;
addpath('../voicebox/','-end');

%% Read the data
%{
datFolder = '../genCascade/data/';
[yraw1, fs] = wavread([datFolder 'gcwCalls.wav']);
yraw1 = resample(yraw1, 16000, fs);
[yraw2, fs] = wavread([datFolder 'cardinalCalls.wav']);
yraw2 = resample(yraw2, 16000, fs);
[yraw3, fs] = wavread([datFolder 'interference.wav']);
yraw3 = resample(yraw3, 16000, fs);
[yraw4, fs] = wavread([datFolder 'wind.wav']);
yraw4 = resample(yraw4, 16000, fs);
[yraw5, fs] = wavread([datFolder 'car.wav']);
yraw5 = resample(yraw5, 16000, fs);
yraw6 = chirp(0:1/16000:30,5000, 15, 5000, 'linear')';
yraw7 = chirp(0:1/16000:40,5000, 15, 8000, 'quadratic')';
yraw = [yraw1; yraw2; yraw3.*2; yraw4.*2; yraw5.*0.05; yraw6.*0.05; yraw7*0.05];
%}
%{
[yraw, fs] = wavread('data/GCWarbler40561.wav');
yraw = resample(yraw, 16000, fs);
%}
[yraw, fs] = wavread('data/one.wav');
yraw = resample(yraw, 16000, fs);

y = yraw + 0.0*randn(size(yraw));
fs = 16000;

figure('units','normalized','outerposition',[0 0 1 1]);
subplot(311); plot(y); axis tight

N = 256; % Block length
win = hann(N);
%[Y,F,T] = spectrogram(y, win, N/2, N, fs);
yf = enframe(y, win, N/2)';
Y = fft(yf);
Y = Y(1:N/2+1,:); % take half due to symmetry
T = [1:size(Y,2)]*N/2/fs;
F = [1:size(Y,1)]/N*fs;

ph(1) = subplot(312); imagesc([], F, 10*log10(abs(Y).^2)); axis xy

% Manual labeling
label = zeros(size(T));
label(581:776) = 1;
label(1874:2051) = 1;
label(3013:3218) = 1;
label(4227:4418) = 1;
label(5513:5676) = 1;
label(6634:6821) = 1;
label(8132:8306) = 1;
label(9109:9284) = 1;
label(9841:10020) = 1;
label(10939:11119) = 1;
label(12101:12250) = 1;
label(13128:13299) = 1;
label(14399:14571) = 1;
label = logical(label);

ph(2) = subplot(313); stem(label); axis('tight')
linkaxes(ph, 'x')

%% Try IF detector
% Parameters
nbin = size(Y, 1); % Number of frequency bin
backtracklen = ceil(0.5*fs/N);
hbw = ceil(5000*N*nbin*2/(fs*fs)); % half bandwidth specified in chirp rate
alpha = exp(-N/(0.1*fs)); % step response with the denominator in the exp as time constant
thresh = 0.005;

% running state
spec_state = zeros(nbin, backtracklen+1);
linkBack_state = zeros(nbin, backtracklen+1);
spec_cum = zeros(nbin, 1);
line_cnt = 0;
line_index_state = zeros(nbin, 2);

% Backup
spec_bak = zeros(size(Y));
linkBack_bak = zeros(size(Y));
backtrackcumsum_bak = zeros(size(Y));
line_index_bak = zeros(size(Y));
IF = cell(0);
IFVal = cell(0);

for k = 1:size(Y, 2)
    spec = abs(Y(:,k)).^2;
    
    % Forward tracking
    spec_cum_last = spec_cum;
    offset = zeros(nbin, 1);
    linkBack = zeros(nbin, 1);
    for l = 1:nbin
        upfreq = min(nbin,l+hbw);
        lowfreq = max(1,l-hbw);
        [~, offset(l)] = max( (1 - 0.1/hbw*abs(l - [lowfreq:upfreq]')).*(spec_cum_last(lowfreq:upfreq)) );
        linkBack(l) = lowfreq+offset(l)-1;
        spec_cum(l) = alpha*spec_cum_last(linkBack(l)) + (1-alpha)*spec(l);
    end
    linkBack_state = [linkBack_state(:,2:end) linkBack];
    linkBack_bak(:, k) = linkBack;
    spec_state = [spec_state(:, 2:end) spec];
    spec_bak(:,k) = spec;
    
    if (k > backtracklen)
        % Backtrack every new frame
        backtrackcumsum = spec_state(:, backtracklen+1);
        backtrackflag = ones(nbin, 1); % 1 is to be inspected
        for l = backtracklen+1:-1:2
            backtrackcumsum_tmp = zeros(nbin, 1);
            backtrackflag_tmp = zeros(nbin, 1);
            for m = 1:nbin
                if (backtrackflag(m) == 1)
                    backtrackcumsum_tmp(linkBack_state(m, l)) = ...
                        max(backtrackcumsum_tmp(linkBack_state(m, l)), backtrackcumsum(m) + spec_state(linkBack_state(m, l), l-1));
                    backtrackflag_tmp(linkBack_state(m, l)) = 1;
                end
            end
            backtrackcumsum = backtrackcumsum_tmp;
            backtrackflag = backtrackflag_tmp;
        end
        backtrackcumsum = backtrackcumsum/(backtracklen+1);
        backtrackcumsum_bak(:, k-backtracklen) = backtrackcumsum;
        
        % Output/make decision
        tmpline_index = zeros(nbin, 1);
        for l = 1:nbin
            if (backtrackcumsum(l) > thresh)
                line_index = line_index_state(linkBack_state(l, 1), 2);
                if line_index == 0
                    % new line
                    line_cnt = line_cnt + 1;
                    line_index = line_cnt;
                    
                    IF{line_index} = [l;k-backtracklen];
                    IFVal{line_index} = backtrackcumsum(l);
                    tmpline_index(l) = line_index;
                else
                    % continued line
                    tmpfreq = linkBack_state(l,1);
                    upfreq = min(nbin,tmpfreq+hbw);
                    lowfreq = max(1,tmpfreq-hbw);
                    % nonmaximum suppression
                    if (backtrackcumsum(l) >= max(backtrackcumsum(lowfreq:upfreq)))
                        IF{line_index} = [IF{line_index} [l;k-backtracklen]];
                        IFVal{line_index} = IFVal{line_index}+backtrackcumsum(l);
                        tmpline_index(l) = line_index;
                    end
                end
            end
        end
        line_index_state = [line_index_state(:,2) tmpline_index];
        line_index_bak(:, k-backtracklen) = tmpline_index;
    end
end

figure('units','normalized','outerposition',[0 0 1 1]);
ploth(1) = subplot(311); imagesc(log(spec_bak)); axis xy
hold on; 
for k = 1:line_cnt
    if (size(IF{k}, 2) > 1 && IFVal{k} > 1)
        plot(IF{k}(2,:), IF{k}(1,:), 'w'); text(IF{k}(2,1), IF{k}(1,1), num2str(k))
    end
end
title(['IF: ' num2str(line_cnt)])
ploth(2) = subplot(312); imagesc(backtrackcumsum_bak); axis xy
title('backtrackcumsum\_bak')
ploth(3) = subplot(313); imagesc(line_index_bak); axis xy
title('line\_index\_bak')
linkaxes(ploth, 'xy')
