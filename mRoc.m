function [pd, pf, thres] = mRoc(stat, label, N)
% function [pd, pf, thres] = mRoc(stat, label, N)
% Compute ROC
% 
% Long Le
% University of Illinois
%

if (~islogical(label))
    error('label must be logical')
elseif (~isequal(size(stat), size(label)))
    error('data and label must be of the same size')
elseif numel(label) == 0
    error('empty input')
end

% do NOT use thresSpace, it's not range
%{
thresSpace = unique(stat);
if (nargin <=2)
    thres = thresSpace;
else
    %thres = min(thresSpace):(max(thresSpace)-min(thresSpace))/(N-1):max(thresSpace);
    thres = quantile(thresSpace,[0:1/(N-1):1]);
end
%}
if (nargin <=2)
    N = 1e2;
end
thres = min(stat):range(stat)/(N-1):max(stat);
pd = zeros(1, length(thres));
pf = zeros(1, length(thres));
for k = 1:length(thres)
    pd(k) = mean(stat(label) >= thres(k));
    pf(k) = mean(stat(~label) >= thres(k));
end
% Ensure valid probability
pd(pd == 0) = eps;
pf(pf == 0) = eps;
