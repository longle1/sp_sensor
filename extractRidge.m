function [R, groupId, M, ridgetracker] = extractRidge(spec, fs, fftSize, btLen, initNF)

nBlk = fftSize;%2^floor(log2(frameLen*fs));
nFc = fftSize/2;
 
noisetracker.alpha_updown = 0.01;
noisetracker.slow_scale = 0.1;
noisetracker.indicator_countdown = ceil(btLen*fs/(nBlk/2));
noisetracker.floor_up = (1+noisetracker.alpha_updown)*ones(nFc,1);
noisetracker.floor_up_slow = (1+noisetracker.slow_scale*noisetracker.alpha_updown)*ones(nFc,1);
noisetracker.floor_down = (1-noisetracker.alpha_updown)*ones(nFc,1);
noisetracker.floor_thresh = max(1e-6,initNF);
noisetracker.indicator_last = zeros(nFc,1);

ridgetracker.alpha_ridge = exp(-nBlk/2/(0.1*fs));
ridgetracker.f_ridge_down_max = ceil(20000*nBlk/2*nFc*2/(fs*fs)); % want X Hz/sec chirp rate; f_ridge_down_max units are freq-samples/blklen-time-samples; 1 freq-sample = fs/binct Hz
ridgetracker.f_ridge_up_max = ridgetracker.f_ridge_down_max;
ridgetracker.f_ridge_down = zeros(nFc,1);
ridgetracker.f_ridge_up = zeros(nFc,1);
for ii=1:nFc,
   ridgetracker.f_ridge_down(ii) = max(1,ii-ridgetracker.f_ridge_down_max);
   ridgetracker.f_ridge_up(ii) = min(nFc,ii+ridgetracker.f_ridge_up_max);
end
ridgetracker.ridge_offset_penalty_max = 0.2;
ridgetracker.ridge_offset_penalty = ridgetracker.ridge_offset_penalty_max/max(ridgetracker.f_ridge_up_max,ridgetracker.f_ridge_down_max);
ridgetracker.log_prob_noise_min_ridge = -10.0;
ridgetracker.detect_ridge_backtrack_thresh = -5.0;
ridgetracker.log_prob_noise_ridge_cum_now = zeros(nFc,1);
ridgetracker.backtracklen = ceil(btLen*fs/(nBlk/2)); % enter the multiplier as the approximate track length in sec
ridgetracker.specridge_link_back = zeros(nFc,ridgetracker.backtracklen+1);
ridgetracker.speclog_prob_noise_ridge = zeros(nFc,ridgetracker.backtracklen+1);
ridgetracker.specdetected_ridge_last = zeros(nFc,2);
ridgetracker.ridgect = 0;
ridgetracker.ridgegroupct = 0;
R = zeros(0, size(spec,2));
M = zeros(0, size(spec,2));
groupId = zeros(0, 1); % group index

% Main
spec = padarray(spec,[0,ridgetracker.backtracklen],0,'post'); % pad for finalization
spec_noise_floor = zeros(nFc, size(spec,2));
speclog_prob_noise_ridge = zeros(nFc, size(spec,2));
specbacktrackcumsum = zeros(nFc, size(spec,2));
specbacktracktouchedflags = zeros(nFc, size(spec,2));
for k = 1:size(spec,2)
    s = spec(:,k);

    offset_ridge = zeros(nFc,1);
    ridge_link_back = zeros(nFc,1);
    log_prob_noise_ridge = zeros(nFc,1);
    log_prob_noise_ridge_cum_last = ridgetracker.log_prob_noise_ridge_cum_now;
    for j=1:nFc
        % noise tracking
        if(s(j) > noisetracker.floor_thresh(j))
            noisetracker.indicator_last(j) = noisetracker.indicator_last(j)-1;
            if(noisetracker.indicator_last(j) <= 0) % signal is highly probable, slow down
                noisetracker.floor_thresh(j) = max(1e-6,noisetracker.floor_thresh(j)*noisetracker.floor_up_slow(j));
            else % noise is still highly probable
                noisetracker.floor_thresh(j) = max(1e-6,noisetracker.floor_thresh(j)*noisetracker.floor_up(j));
            end
        else % noise is highly probable
            noisetracker.floor_thresh(j) = max(1e-6,noisetracker.floor_thresh(j)*noisetracker.floor_down(j));
            noisetracker.indicator_last(j) = noisetracker.indicator_countdown;
        end

        %  TIME-VARYING SPECTRAL RIDGE TRACKING
        [~, offset_ridge(j)] = ...
            min((1 - ridgetracker.ridge_offset_penalty*abs(j - [ridgetracker.f_ridge_down(j):ridgetracker.f_ridge_up(j)]')).*...
            log_prob_noise_ridge_cum_last(ridgetracker.f_ridge_down(j):ridgetracker.f_ridge_up(j)));
        
        log_prob_noise_ridge(j) = max(ridgetracker.log_prob_noise_min_ridge,-s(j)^2/noisetracker.floor_thresh(j));
        ridgetracker.log_prob_noise_ridge_cum_now(j) = ridgetracker.alpha_ridge*log_prob_noise_ridge_cum_last(ridgetracker.f_ridge_down(j)+offset_ridge(j)-1) + ...
              (1-ridgetracker.alpha_ridge)*log_prob_noise_ridge(j);
        ridge_link_back(j) = ridgetracker.f_ridge_down(j)+offset_ridge(j)-1;  % pointers to the previous ridge frequency
    end
    ridgetracker.specridge_link_back = [ridgetracker.specridge_link_back(:,2:end) ridge_link_back];
    ridgetracker.speclog_prob_noise_ridge = [ridgetracker.speclog_prob_noise_ridge(:,2:end) log_prob_noise_ridge];

    % debugging
    spec_noise_floor(:,k) = noisetracker.floor_thresh;
    speclog_prob_noise_ridge(:,k) = log_prob_noise_ridge;

    %
    % Backward track detection
    %
    % track back backtracklen time blocks
    backtrackcumsum = log_prob_noise_ridge;
    backtracktouchedflags = ones(nFc,1);
    for i5=ridgetracker.backtracklen+1:-1:2,
      backtrackcumsumnew = zeros(nFc,1);
      backtracktouchedflagsnew = zeros(nFc,1);
      % Follow pointer back from each frequency to ridge-frequency at previous time and update backward cumsum of detection metric
      for iii=1:nFc
         if ( backtracktouchedflags(iii) == 1),
            if ridgetracker.specridge_link_back(iii,i5) == 0
                continue;
            end
            backtrackcumsumnew(ridgetracker.specridge_link_back(iii,i5)) = ...
                min(backtrackcumsumnew(ridgetracker.specridge_link_back(iii,i5)),...
                backtrackcumsum(iii)+ridgetracker.speclog_prob_noise_ridge(ridgetracker.specridge_link_back(iii,i5),i5-1));
            backtracktouchedflagsnew(ridgetracker.specridge_link_back(iii,i5)) = 1;  % indicate which frequencies were part of a backtracked ridge
         end
      end
      backtrackcumsum = backtrackcumsumnew;
      backtracktouchedflags = backtracktouchedflagsnew;
    end
    backtrackcumsum = (1.0/(ridgetracker.backtracklen+1))*backtrackcumsum;

    % debugging
    if (k >= ridgetracker.backtracklen)
        specbacktrackcumsum(:,k-ridgetracker.backtracklen+1) = backtrackcumsum;
        specbacktracktouchedflags(:,k-ridgetracker.backtracklen+1) = backtracktouchedflags;
    end

    %
    % find t-f locations detected as part of ridge
    %
    detected_ridge_back = zeros(nFc,1);
    for iii=1:nFc
      if backtrackcumsum(iii) <= ridgetracker.detect_ridge_backtrack_thresh
         if ridgetracker.specridge_link_back(iii,1) == 0
             continue;
         end
         % pickup from the previous ridge
         ridgeId = ridgetracker.specdetected_ridge_last(ridgetracker.specridge_link_back(iii,1),2);
         % check the start of a new track OR collision at the same time frame
         if ridgeId == 0
             if iii > 1 && detected_ridge_back(iii-1) ~= 0 
                 % create a new ridge with the recent group idx, at the same frame
                 oldGroupId = groupId(detected_ridge_back(iii-1),1);

                 ridgetracker.ridgect = ridgetracker.ridgect + 1;
                 ridgeId = ridgetracker.ridgect;
                 groupId(ridgeId,1) = oldGroupId;
             else
                 % create a new ridge with a new group idx
                 ridgetracker.ridgegroupct = ridgetracker.ridgegroupct + 1;
                 
                 ridgetracker.ridgect = ridgetracker.ridgect + 1;
                 ridgeId = ridgetracker.ridgect;
                 groupId(ridgeId,1) = ridgetracker.ridgegroupct;
             end
         elseif R(ridgeId,k-ridgetracker.backtracklen+1) ~= 0      
             % create a new ridge with the old group idx
             oldGroupId = groupId(ridgeId,1);
            
             ridgetracker.ridgect = ridgetracker.ridgect + 1;
             ridgeId = ridgetracker.ridgect;
             groupId(ridgeId,1) = oldGroupId;
         end
         R(ridgeId,k-ridgetracker.backtracklen+1) = iii;
         M(ridgeId,k-ridgetracker.backtracklen+1) = -backtrackcumsum(iii); % negated
         detected_ridge_back(iii) = ridgeId;
      end
    end
    ridgetracker.specdetected_ridge_last = [ridgetracker.specdetected_ridge_last(:,2) detected_ridge_back];
end
nanIdx = R==0;
R(nanIdx) = nan;
M(nanIdx) = nan;
