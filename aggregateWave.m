function aggregateWave(folderName, padTime, outDir)
% Aggregate all wav file in a folder into a single file, with 0 padding in
% between
% 
% Long Le <longle1@illinois.edu>
%

targetFs = 16e3;

allY = zeros(0,1);
files = dir(folderName);
for k = 3:numel(files)
    [y, fs] = audioread([folderName '\' files(k).name]);
    y = resample(y(:,1), targetFs, fs);
    allY = [allY; y; zeros(padTime*targetFs,1)];
end
pathStr = strsplit(folderName,'\');
audiowrite([outDir '\' pathStr{end} '.wav'], allY, targetFs);