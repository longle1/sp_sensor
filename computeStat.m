function feat = computeStat(F,tt,mode)
% Compute aggregate statistics from the ridges
% 
% Long Le
% University of Illinois
% longle1@illinois.edu
%

switch mode
    case 1
        meanF = zeros(size(F,1),1);
        stdF = zeros(size(F,1),1);
        derF = zeros(size(F,1),1);
        durF = zeros(size(F,1),1);
        for k = 1:size(F,1)
            valIdx = ~isnan(F(k,:));

            meanF(k)  = mean(F(k,valIdx));
            stdF(k) = std(F(k,valIdx));
            derF(k) = mean(diff(F(k,valIdx)));
            durF(k) = range(tt(valIdx));
        end
        feat = [meanF stdF derF durF];
        % prune out nan derivative/ single-bin ridges
        feat(isnan(feat(:,3)),:) = [];
        
        % pick the top 10 ridges only
        [~, sIdx] = sort(feat(:,4),'descend');
        feat = feat(sIdx(1:min(10,size(feat,1))),:);
    case 2
        X = zeros(0,2);
        Xcnt = 0;
        for k = 1:size(F,1)
            for l = 1:size(F,2)
                if ~isnan(F(k,l))
                    Xcnt = Xcnt + 1;
                    X(Xcnt,:) = [F(k,l),tt(l)];
                end
            end
        end
        % absolute time to duration
        X(:,2) = X(:,2) - min(X(:,2));
        %{
        figure;
        subplot(211); hist(X(:,1)); % freq
        xlabel('Frequency (Hz)')
        set(gca,'view',[90 -90])
        subplot(212); hist(X(:,2)); % time
        xlabel('Time (s)')
        %}
        
        feat = zeros(1,10);
        % Freq stats
        feat(1) = median(X(:,1));
        feat(2) = mean(X(:,1));
        feat(3) = std(X(:,1));
        feat(4) = skewness(X(:,1));
        feat(5) = kurtosis(X(:,1));
        % Time stats
        feat(6) = median(X(:,2));
        feat(7) = mean(X(:,2));
        feat(8) = std(X(:,2));
        feat(9) = skewness(X(:,2));
        feat(10) = kurtosis(X(:,2));
        % prune out nan derivative/ single-bin ridges
        feat(isnan(feat(:,1)),:) = [];
        %{
        numMelBank = 10;
        melFilt = melbankm(numMelBank,frameSize,fs);
        %}
end