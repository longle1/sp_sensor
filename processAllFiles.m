function [classLoss, dataSize] = processAllFiles(method)
% Process all files and evaluate for a given methods.
%
% Long Le 
% University of Illinois
% longle1@illinois.edu
%
tic

currDateStr = datestr(now,'yyyy-mm-dd-HH-MM-SS-FFF');

rootDir = '\\UYEN-HP\Public\Dataset\SelectedUrbanSound\data';
%rootDir = 'F:\UrbanSoundAll\SelectedUrbanSound\data';
folders = dir(rootDir);

cnt = 0;
lab = zeros(0,1);
feat = cell(0,1);
bdry = cell(0,1);
for k = 3:numel(folders)
    currFolderName = folders(k).name;
    files = dir([rootDir '\' currFolderName '\*.wav']);
    if sum(strcmp(currFolderName,{'car_horn','gun_shot','speech'}))
        % extract signal semantics/statistics
        parfor l = 1:numel(files)
            disp([currFolderName '\' files(l).name]);
            
            lab(cnt+l,1) = sound2labId(currFolderName);
            [feat{cnt+l,1},bdry{cnt+l,1}] = processOneFile([rootDir '\' currFolderName '\' files(l).name],method,false);
        end
        cnt = cnt + numel(files);
    end
end
%{
for k = 1:numel(F)
    feat{k,1} = computeStat(method(2),F{k},tt{k},M{cnt});
end
%}
save(['\\UYEN-HP\Public\Log\' sprintf('feat_%s_%d.mat',currDateStr,method)],'feat','bdry','lab');

%% Evaluate the discriminative power of the features
idx = 0;
labMat = zeros(0);
for k = 1:numel(feat)
    labMat(idx+1:idx+size(feat{k},1),1) = lab(k)*ones(size(feat{k},1),1);
    idx = idx+size(feat{k},1);
end
%[nr nc]=cellfun(@size, feat);
featMat = cell2mat(feat);

% direct visualization
%{
% 3D
figure; hold on; view(3)
labSet = unique(labMat);
for k = 1:numel(labSet)
    selIdx = labMat == labSet(k);
    plot3(featMat(selIdx,1),featMat(selIdx,2),featMat(selIdx,3),['x' idx2col(k)])
end
% 1D
for k = 1:size(featMat,2)
    figure;
    subplot(211); plot(featMat(:,k));
    subplot(212); plot(labMat);
end
%}

% Train SVMs to detect a target class
rng('default')
labSet = unique(labMat);
svmModel = cell(numel(labSet),1);
CVSVMModel = cell(numel(labSet),1);
classLoss = zeros(numel(labSet),1);
for k = 1:numel(labSet)
    % treat nans as missing values and skip
    binLabMat = labMat == labSet(k);
    % simply put the data and the model together into a structure
    svmModel{k} = fitcsvm(featMat,binLabMat,'KernelFunction','rbf','KernelScale','auto','Standardize',true);
    % Model validation
    CVSVMModel{k} = crossval(svmModel{k},'Kfold',10);
    % simply return the loss
    classLoss(k) = kfoldLoss(CVSVMModel{k});
end
%{
% verify kfoldLoss output
targetIdx = 2;
kfoldLoss(CVSVMModel{targetIdx},'mode','individual')
figure;
nFolds = CVSVMModel{2}.KFold;
for k = 1:nFolds
    testInd = test(CVSVMModel{targetIdx}.Partition,k);
    aModel = CVSVMModel{targetIdx}.Trained{k};

    binLabMat = CVSVMModel{targetIdx}.Y(testInd);
    c = predict(aModel,CVSVMModel{targetIdx}.X(testInd,:));

    subplot(2,nFolds,k); stem(c); subplot(2,nFolds,nFolds+k); stem(binLabMat)
    title(sprintf('err = %.5f',mean(c~=binLabMat)));
end
%}

%{
% In-fold loss trained on in-fold
figure;
for k = 1:4
    binLabMat = labSet(k) == labMat;    
    svmModel{k} = fitPosterior(svmModel{k}, featMat, binLabMat);
    c = predict(svmModel{k},featMat);

    subplot(2,4,k); stem(c); subplot(2,4,4+k); stem(binLabMat)
    title(sprintf('err = %.5f',mean(c~=binLabMat)));
end
%}

%{
numpar = 5;
svmModel = cell(numel(labSet),numpar);
recall = zeros(numel(labSet),numpar);
falarm = zeros(numel(labSet),numpar);
prec = zeros(numel(labSet),numpar);
rho = zeros(numel(labSet),numpar);
for k = 1:numel(labSet)
    targetInd = double(labMat == labSet(k)); targetInd(targetInd==0) = -1;
    [trainFeat, trainLab, testFeat, testLab] = xvalid(featMat, targetInd, numpar);
    
    for l = 1:numpar
        svmModel{k,l} = fitcsvm(trainFeat{l},trainLab{l},'KernelFunction','rbf','KernelScale','auto','Standardize',true);
        svmModel{k,l} = fitPosterior(svmModel{k,l},trainFeat{l},trainLab{l});
        [testOut, ~] = predict(svmModel{k,l}, testFeat{l});
        
        recall(k,l) = mean(testOut(testLab{l}==1)==1);
        falarm(k,l) = mean(testOut(testLab{l}==-1)==1);
        prec(k,l) = mean(testLab{l}(testOut==1)==1);
        rho(k,l) = mean(testLab{l}(testOut==-1)==1);
    end
end
%}

%% Evaluate the amount of raw data stored
datumSize = zeros(numel(bdry),1);
for k = 1:numel(bdry)
    % partition into disjoint sets
    bdrySet = partSet(bdry{k});
    % aggregate all
    datumSize(k) = sum(diff(bdrySet,[],2));
end
dataSize = sum(datumSize);

save(['\\UYEN-HP\Public\Log\' sprintf('classLoss_%s_%d.mat',currDateStr,method)],'classLoss','svmModel','dataSize');

toc