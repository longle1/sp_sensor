function [X,W,L,C,D] = ridgeCluster(FI, M, K)
% Clustering of ridges/function
%
% Long Le
% University of Illinois
% longle1@illinois.edu
%

X = zeros(0,2);
W = zeros(0,1);
Xcnt = 0;
for k = 1:size(FI,1)
    for l = 1:size(FI,2)
        if ~isnan(FI(k,l))
            Xcnt = Xcnt + 1;
            X(Xcnt,:) = [FI(k,l),l];
            W(Xcnt,1) = M(k,l);
        end
    end
end
options.weight = W;
[L,C,D]= fkmeans(X, K, options);% label, centroid, distortion