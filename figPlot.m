function figPlot()
% Figure plot for the first major revision
%
% Long Le <longle1@illinois.edu>
%
clear all; close all;

addpath(genpath('../IlliadAccess/matlab'))

%% Precision-recall plot
GCW = [0.688311688	0.981481481;
0.666666667	0.962962963;
0.538461538	0.933333333;
0.12962963	0.777777778;
0.086956522	0.714285714;
0.02238806	0.428571429;
0	0
];

whistle = [0.727272727	1;
0.28125	1;
0.041916168	1;
0 0];

key1 = [0.571428571	1;
0.553571429	1;
0.5625	1;
0.364341085	0.959183673;
0.183006536	0.965517241;
0.035175879	1;
0.009049774	1;
];

key2 = [0.666666667	0.947368421;
0.672897196	0.935064935;
0.688679245	0.924050633;
0.372093023	0.905660377;
0.209150327	0.820512821;
0.020100503	0.571428571;
0	0;
];

key3 = [0.612612613	0.985507246;
0.581818182	0.96969697;
0.618181818	0.971428571;
0.362903226	0.97826087;
0.164473684	0.961538462;
0.031578947	0.75;
0.00456621	0.5;
];

key4 = [0.535714286	1;
0.530973451	1;
0.558558559	0.984126984;
0.264285714	0.973684211;
0.109756098	0.947368421;
0.014778325	1;
];

key5 = [0.317757009	0.871794872;
0.293577982	0.914285714;
0.277777778	0.882352941;
0.088235294	0.857142857;
0.042944785	0.7;
0.004926108	0.2;
0	0;
];

key6 = [0.485148515	0.816666667;
0.475247525	0.813559322;
0.425742574	0.796296296;
0.371428571	0.75;
0.219298246	0.581395349;
0.104895105	0.46875;
0.039325843	0.35;
];

key7 = [0.365384615	0.826086957;
0.359223301	0.804347826;
0.349056604	0.860465116;
0.196581197	0.696969697;
0.049295775	0.4375;
0.026595745	0.833333333;
];

key8 = [0.43	0.781818182;
0.414141414	0.759259259;
0.396039604	0.784313725;
0.289473684	0.785714286;
0.158730159	0.606060606;
0.024242424	0.235294118;
0	0;
];

key9 = [0.462962963	0.925925926;
0.458715596	0.943396226;
0.440366972	0.941176471;
0.238095238	0.882352941;
0.083333333	0.866666667;
0.025510204	0.833333333;
];

key = mean([key1;key2;key3;key4;key5;key6;key7;key8;key9]);

datkey1 = [64	48	112	0;
62	50	112	0;
63	49	112	0;
47	82	93	2;
28	125	70	1;
7	192	25	0;
2	219	3	0;
];

datkey2 = [72	36	112	4;
72	35	112	5;
73	33	112	6;
48	81	90	5;
32	121	64	7;
4	195	22	3;
0	219	3	2;
];

datkey3 = [68	43	112	1;
64	46	112	2;
68	42	112	2;
45	79	99	1;
25	127	71	1;
6	184	32	2;
1	218	4	1;
];

datkey4 = [60	52	112	0;
60	53	112	0;
62	49	112	1;
37	103	83	1;
18	146	59	1;
3	200	21	0;
0	223	1	0;
];

datkey5 = [34	73	112	5;
32	77	112	3;
30	78	112	4;
12	124	85	2;
7	156	58	3;
1	202	17	4;
0	220	3	1;
];

datkey6 = [49	52	112	11;
48	53	112	11;
43	58	112	11;
39	66	106	13;
25	89	92	18;
15	128	64	17;
7	171	33	13;
];

datkey7 = [38	66	112	8;
37	66	112	9;
37	69	112	6;
23	94	97	10;
7	135	73	9;
5	183	35	1;
0	220	4	0;
];

datkey8 = [43	57	112	12;
41	58	112	13;
40	61	112	11;
33	81	101	9;
20	106	85	13;
4	161	46	13;
0	207	12	5;
];

datkey9 = [50	58	112	4;
50	59	112	3;
48	61	112	3;
30	96	94	4;
13	143	66	2;
5	191	27	1;
0	220	4	0;
];

datkey = datkey1+datkey2+datkey3+datkey4+datkey5+datkey6+datkey7+datkey8+datkey9;
perfkey = zeros(size(datkey,1),2);
% prec
for k = 1:size(perfkey)
    perfkey(k,1) = datkey(k,1)/(datkey(k,1)+datkey(k,2));
end
% recall
for k = 1:size(perfkey)
    perfkey(k,2) = datkey(k,1)/(datkey(k,1)+datkey(k,4));
end

dbNum = [3 2 1 0 -0.3 -0.7 -1]';
dbLabel = zeros(size(dbNum, 1), 7);
for k = 1:size(dbNum)
    dbLabel(k,:) = sprintf('%+0.1f dB', dbNum(k));
end
dbLabel = char(dbLabel);
symb = ['o' 'x' '>' '<' 's' 'd' 'v'];

%%
figure; hold on; 

plot(GCW(:,2), GCW(:,1), 'dr-.', 'linewidth', 3); 
%for k = 1:size(GCW,1)
%    plot(GCW(k,2),GCW(k,1),symb(k), 'color', 'r', 'linewidth', 5)
%end
%text(GCW(:,2), GCW(:,1), dbLabel(1:size(GCW,1),:), 'fontsize', 10, 'color', 'r', 'HorizontalAlignment', 'left')

plot(whistle(:,2), whistle(:,1), 'dg--', 'linewidth', 3); 
%for k = 1:size(whistle,1)
%    plot(whistle(k,2),whistle(k,1),symb(k), 'color', 'g', 'linewidth', 5)
%end
%text(whistle(:,2), whistle(:,1), dbLabel(1:size(whistle,1),:), 'fontsize', 10, 'color', 'g', 'HorizontalAlignment', 'right');

plot(perfkey(:,2), perfkey(:,1), 'db-', 'linewidth', 3); 
%for k = 1:size(perfkey,1)
%    plot(perfkey(k,2),perfkey(k,1),symb(k), 'color', 'b', 'linewidth', 5)
%end
%text(perfkey(:,2), perfkey(:,1), dbLabel(1:size(perfkey,1),:), 'fontsize', 10, 'color', 'b', 'VerticalAlignment', 'top');

%axis([0,1.1,0,1.1]);
set(gca, 'fontsize', 15)
xlabel('Recall', 'fontsize', 15); ylabel('Precision', 'fontsize', 15)
legend('GCW', 'Whistle', 'TIDIGITS', 'location', 'northwest')

%%
DB = 'publicDb';
USER = 'publicUser';
PWD = 'publicPwd';

q.t1 = datenum(2014,10,05,19,55,00); q.limit = 24;
%q.t1 = datenum(2014,10,06,01,05,00); q.limit = 24;
%q.t1 = datenum(2014,10,06,01,23,00); q.limit = 24;
events = IllView(DB, USER, PWD, q);

k = 20;
%k = 11;
%k = 11;
[data, y, header] = IllDownData(DB, USER, PWD, events{k}.filename);

figure;
Y = spectrogram(y, hanning(256), 0);
hold on; ylim([1, 128]);
imagesc(10*log10(abs(Y))); axis xy;

fdnames = fieldnames(events{k}.TFRidge);
for l = 1:numel(fdnames)
    time = events{k}.TFRidge.(fdnames{l}).time;
    freq = events{k}.TFRidge.(fdnames{l}).freq;
    plot(time, freq, 'color', 'k', 'linewidth', 3); axis tight
end
set(gca, 'fontsize', 15)
set(gca, 'XTickLabel', [1:(size(Y, 2)-1)/5:size(Y, 2)]*events{k}.Nblk/events{k}.fs ,'YTickLabel', [1:(size(Y,1)-1)/5:size(Y,1)]/events{k}.Nfreq*events{k}.fs/2)
xlabel('Time (s)', 'fontsize', 15); ylabel('Frequency (Hz)', 'fontsize', 15)

%% Figure plot for the second major revision
datSrc = 1;
if datSrc == 1
    load('GCW.mat');
    figTitle = 'GCW';
elseif datSrc == 2
    load('whistlecommand.mat');
    figTitle = 'whistlecommand';
elseif datSrc == 3
    load('TIDIGIT_adults_crop_crop.mat');
    figTitle = 'TIDIGIT';
end

mColor = 'rgb';
figure; hold on;
for k = 1:length(ROCs)
    plot(ROCs{k}(2,:), ROCs{k}(1,:), [mColor(k) 'd-'])
end
legend('Ridge Tracking', 'Magnitude Tracking', 'Otsu', 'location', 'southeast')
title(figTitle)
