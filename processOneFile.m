function [feat,bdry] = processOneFile(filename, method, isPlot)
% Process one file.
%
% Long Le 
% University of Illinois
% longle1@illinois.edu
%

%{
[pathstr, fname, ~] = fileparts(filename);
tokens = strsplit(pathstr, '\');
matFilename = ['\\' tokens{2} '\' tokens{3} '\Log\' sprintf('R_%s_%s_%d.mat',tokens{end},fname,method)];
%}

while 1
    try
        %[y,fs] = audioread('./data/GCWarbler40561.wav');
        %[y,fs] = audioread('./data/rings/ring1.wav');
        [y,fs] = audioread(filename);
        break;
    catch
        [~, hostname] = system('hostname');
        fprintf(1,'unable to find %s on host %s, retry...\n', filename, hostname);
        pause(1);
    end
end
y = resample(y(:,1), 16e3, fs); % process only mono-channel
fs = 16e3;
%y = y(1:min(numel(y),fs*15)); % limited to 15 s of audio

fftSize = 256;
[S,tt,ff] = mSpectrogram(y,fs,fftSize);
switch method
    case 1
        % Ridge tracker
        %{
        if ~exist(matFilename,'file')
            [R,gId,M] = extractRidge(abs(S), fs, -1, 0.01);
            save(matFilename, 'R','gId','M');
        else
            load(matFilename, 'R','gId','M');
        end
        %}
        [R,gId,M] = extractRidge(abs(S), fs, fftSize, 0.04, max(1e-6,quantile(abs(S(1:fftSize/2,:)),0.5,2)));
        [F,T] = ridgeIdx2TFRidge(R,fs,fftSize,tt);
        
        [feat,selGId,bdry] = ridgeGroupStat(F,T,gId,M,fs,fftSize);
        
        if isPlot
            figure; hold on
            imagesc(tt, ff, log(abs(S)));
            colormap(1-gray)
            if size(F,1) > 0 % check if there's at least a ridge
                %gIdxSet = unique(gIdx);
                for k = 1:size(F,1)
                    plot(T(k,:)',F(k,:)',idx2col(k));
                end
                %{
                for k = 1:size(selGId,1)
                    if (selGId(k) > 0)
                        plot(T(selGId(k),:)',F(selGId(k),:)',idx2col(k));
                    end
                end
                %}
            end
            axis xy; axis tight
            title(sprintf('Method: %d\nFilename: %s', method, filename),'interpreter','none')
        end
    case 2
        % Dan Ellis' sinusoidal modelling
        %{
        if ~exist(matFilename,'file')
            R = extractrax(abs(S));
            save(matFilename, 'R');
        else
            load(matFilename, 'R');
        end
        %}
        [R,M] = extractrax(abs(S));
        [F,T] = ridgeIdx2TFRidge(R,fs,fftSize,tt);
        
        [feat,selId,bdry] = ridgeStat(F,T,M,fs,fftSize);
        
        if isPlot
            figure; hold on
            imagesc(tt, ff, log(abs(S)));
            colormap(1-gray)
            if size(F,1) > 0 % check if there's at least a ridge
                for k = 1:numel(selId)
                    plot(T(selId(k),:)',F(selId(k),:)',idx2col(k));
                end
            end
            axis xy; axis tight
            title(sprintf('Method: %d\nFilename: %s', method, filename),'interpreter','none')
        end
    case 3
        cepst = melcepst(y,fs,'Mtay',16)';
        
        feat = cepstStat(cepst);
        bdry = [tt(1),tt(end)];
end
%[X,W,L,C,D] = ridgeCluster(FI, M, 5);