function testDetectChirp()
% 
% Test detectOneFile.m with chirp signals
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%

rng('default')
addpath('./sinemodel');

fs = 16e3;
blockSize = 256;

t = [1:fs]/fs; % seconds
y{1} = padarray(chirp(t,5000,1,7000),[0 fs*5],0,'both');
%y{2} = padarray(chirp(t,3000,1,0),[0 fs*1],0,'both');
%y{2} = padarray(chirp(t,500,1,2000,'q',[],'convex'),[0 fs*1],0,'both');
%y{3} = padarray(chirp(t,2000,1,500,'q',[],'concave'),[0 fs*1],0,'both');
y{2} = padarray(chirp(t,500,1,1500,'q',[],'concave'),[0 fs*5],0,'both');
y{3} = padarray(chirp(t,3000,1,2000,'q',[],'convex'),[0 fs*5],0,'both');
%y{5} = padarray(chirp(t,200,1,8000,'logarithmic'),[0 fs*1],0,'both');
y{4} = padarray(chirp(t,4000,1,4000),[0 fs*5],0,'both');

Y = (y{1}+y{2}+y{3}+y{4})/4;
N = 0.001*randn(size(Y));

figure; suptitle('clean signal')
%for k = 1:numel(y)
%subplot(numel(y)/2,2,k);
[S,tt,ff] = mSpectrogram(Y,fs,blockSize);
imagesc(tt,ff,S); axis xy;
xlabel('Time (s)'); ylabel('Frequency (Hz)');
%end
gt = max(S) > 0;
pi1 = mean(gt);

SNR = [5e-3 0.01 0.05 0.1];
for l = 1:numel(SNR)
    %for k = 1:numel(y)
    %subplot(numel(y),2,2*(k-1)+1);
    
    %*** various plots
    figure(11);
    subplot(numel(SNR)/2,2,l); plot(SNR(l)*Y+N);
    title(sprintf('noisy signal at %.4f dB',10*log10(SNR(l))))
    
    [S,tt,ff] = mSpectrogram(SNR(l)*Y+N,fs,blockSize);
    figure(12); 
    subplot(numel(SNR)/2,2,l); imagesc(tt,ff,S); axis xy;
    title(sprintf('noisy signal at %.4f dB',10*log10(SNR(l))))

    %subplot(numel(y),2,2*(k-1)+2);
    tic
    [~,specDet] = detectOneFile(S,[],false);
    toc
    figure(13); 
    subplot(numel(SNR)/2,2,l); imagesc(tt,ff(1:end-1),specDet); axis xy;
    title(sprintf('specDet at %.4f dB',10*log10(SNR(l))))
    %end
    
    tic
    [R,M] = extractrax(S);
    toc
    specDet2 = trax2spec(R,M,S);
    figure(14); 
    subplot(numel(SNR)/2,2,l); imagesc(tt,ff,specDet2); axis xy;
    title(sprintf('specDet2 at %.4f dB',10*log10(SNR(l))))
    %figure;
    %imagesc(tt,ff,S); axis xy; hold on; plot(tt,R*fs/256,'r')
    % suptitle(sprintf('S & R at %.4f',SNR))
    
    %*** evaluate against ground truth
    testStat = max(specDet);
    testStat2 = max(specDet2);
    [pd, pf, thres] = mRoc(testStat(1:numel(gt)), gt,1e2);
    [pd2, pf2, thres2] = mRoc(testStat2, gt,1e2);
    
    figure(15); 
    subplot(numel(SNR)/2,2,l); 
    plot(pi1*(1-pd)+(1-pi1)*pf); hold on; plot(pi1*(1-pd2)+(1-pi1)*pf2,':');
    legend('DP','MQ')
    %xlabel('Threshold index'); ylabel('Bayes risk'); title(sprintf('SNR = %.4f dB, \\pi_1 = %.2f',10*log10(SNR(l)), pi1))
    xlabel('Threshold index'); ylabel('Bayes risk'); title(sprintf('SNR = %.4f dB',10*log10(SNR(l))))
end