function [train_feat, train_label, test_feat, test_label] = xvalid(feat_desc, bin_label, numpar)
% Prepare data for cross validation
% 
% Long Le
% University of Illinois

% Partition the data into target and nontarget
target_feat = feat_desc(bin_label == 1,:);
nontarget_feat = feat_desc(bin_label ~= 1,:);

% Partition the data into train and test
target_par_idx = crossvalind('Kfold', size(target_feat,1), numpar);
nontarget_par_idx = crossvalind('Kfold', size(nontarget_feat,1), numpar);
train_feat = cell(1, numpar);
train_label = cell(1, numpar);
test_feat = cell(1, numpar);
test_label = cell(1, numpar);
for k = 1:numpar
    target_train_feat = target_feat(target_par_idx ~= k,:);
    target_train_label = ones(size(target_train_feat, 1),1);
    nontarget_train_feat = nontarget_feat(nontarget_par_idx ~= k,:);
    nontarget_train_label = -ones(size(nontarget_train_feat, 1),1);
    train_feat{k} = [nontarget_train_feat; target_train_feat];
    train_label{k} = [nontarget_train_label; target_train_label];
    % Randomize
    %pidx = randperm(size(train_label{k},1));
    %train_feat{k} = train_feat{k}(pidx,:);
    %train_label{k} = train_label{k}(pidx);

    target_test_feat = target_feat(target_par_idx == k,:);
    target_test_label = ones(size(target_test_feat, 1),1);
    nontarget_test_feat = nontarget_feat(nontarget_par_idx == k,:);
    nontarget_test_label = -ones(size(nontarget_test_feat, 1),1);
    test_feat{k} = [nontarget_test_feat; target_test_feat];
    test_label{k} = [nontarget_test_label; target_test_label];
    % Randomize
    %pidx = randperm(size(test_label{k},1));
    %test_feat{k} = test_feat{k}(pidx,:);
    %test_label{k} = test_label{k}(pidx);
end