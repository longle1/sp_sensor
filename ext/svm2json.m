function [json, json_svm_model] = svm2json(svm_model)
%
% Convert SVM model in MatLab to JSON
% Long Le
% longle1@illinois.edu
% University of Illinois
%

%c_svm_model = compact(svm_model);
c_svm_model = svm_model;

% json svm_model
json_svm_model.alpha = c_svm_model.Alpha;
json_svm_model.bias = c_svm_model.Bias;
json_svm_model.kernelFcn = c_svm_model.KernelParameters.Function;
json_svm_model.kernelScale = c_svm_model.KernelParameters.Scale;
json_svm_model.mu = c_svm_model.Mu;
json_svm_model.sigma = c_svm_model.Sigma;
json_svm_model.sv = c_svm_model.SupportVectors;
json_svm_model.svl = c_svm_model.SupportVectorLabels;
json_svm_model.C = c_svm_model.BoxConstraints(c_svm_model.IsSupportVector);
% score transform parameters
remain = c_svm_model.ScoreTransform;
[token, remain] = strtok(remain, ',');
if strcmp(token, '@(S)sigmoid(S')
    json_svm_model.xformFcn = 'sigmoid';
    [token, remain] = strtok(remain, ',');
    sxfp(1) = str2double(token);
    [token, ~] = strtok(remain, ')');
    sxfp(2) = str2double(token);
    json_svm_model.sxfp = sxfp;
elseif strcmp(token, '@(S)step(S')
    json_svm_model.xformFcn = 'step';
    [token, remain] = strtok(remain, ',');
    sxfp(1) = str2double(token);
    [token, remain] = strtok(remain, ',');
    sxfp(2) = str2double(token);
    [token, ~] = strtok(remain, ')');
    sxfp(3) = str2double(token);
    json_svm_model.sxfp = sxfp;
else
    json_svm_model.xformFcn = 'none';
    json_svm_model.sxfp = [];
end
% Assuming jsonlab dependence
json = savejson('', json_svm_model);