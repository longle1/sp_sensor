function y = wmean(x, w)
% weighted mean
% x - D x N
% w - 1 x N
% y - D x 1

if isempty(x)
    y = [];
end

w = w/norm(w, 1);
y = x*w';