% Probabilistic SVM on TF features
%
% Long Le
% University of Illinois
%

clear all; %close all;

%% Learn matlab svm api
datSrc = [1 3]; % Data source for each class

if datSrc(1) == 1
    load('../node-paper/GCW.mat');
elseif datSrc(1) == 2
    load('../node-paper/whistlecommand.mat');
elseif datSrc(1) == 3
    load('../node-paper/TIDIGIT_adults_crop_crop.mat');
end
ridge_min_time = cell2mat(ridge_min_time);
ridge_max_time = cell2mat(ridge_max_time);
ridge_min_freq = cell2mat(ridge_min_freq);
ridge_max_freq = cell2mat(ridge_max_freq);
ridge_max_inst_bw = cell2mat(ridge_max_inst_bw);
ridge_loglik = cell2mat(ridge_loglik);
pred1 = [ridge_loglik]';
data1 = [ridge_max_time-ridge_min_time; ridge_min_freq; ridge_max_freq; ridge_max_inst_bw]';
label1 = 1*ones(size(data1, 1), 1);

if datSrc(2) == 1
    load('../node-paper/GCW.mat');
elseif datSrc(2) == 2
    load('../node-paper/whistlecommand.mat');
elseif datSrc(2) == 3
    load('../node-paper/TIDIGIT_adults_crop_crop.mat');
end
ridge_min_time = cell2mat(ridge_min_time);
ridge_max_time = cell2mat(ridge_max_time);
ridge_min_freq = cell2mat(ridge_min_freq);
ridge_max_freq = cell2mat(ridge_max_freq);
ridge_loglik = cell2mat(ridge_loglik);
ridge_max_inst_bw = cell2mat(ridge_max_inst_bw);
pred2 = [ridge_loglik]'; 
data2 = [ridge_max_time-ridge_min_time; ridge_min_freq; ridge_max_freq; ridge_max_inst_bw]';
label2 = -1*ones(size(data2, 1), 1);

pred = [pred1;pred2];
data = [data1;data2];
label = [label1;label2];

% Remove noisy data
idx = pred > -200;
data(idx,:) = [];
label(idx) = [];

%Train the SVM Classifier
svm_model = svmtrain(data,label,'kernel_function','rbf');
c = svmclassify(svm_model, data);

% Results
sprintf('Correct: %f', mean(c==label))
if (size(data,2) == 3)
    svm_3d_matlab_vis(svm_model,data,num2cell(num2str(label)))
    xlabel('duration'); ylabel('min freq'); zlabel('max freq')
end

%% Test matlab implementation of svm
target_class = 7;
numpar = 2;
rng('default');
load('feat', 'feat');
    
% Partition the data into target and nontarget
target_idx = find(label == target_class);
target_feat = feat(:,target_idx);
target_metaFeat = metaFeat(:,target_idx);
nontarget_idx = find(label ~= target_class);
nontarget_feat = feat(:,nontarget_idx);
nontarget_metaFeat = metaFeat(:,nontarget_idx);

target_par_idx = crossvalind('Kfold', size(target_feat,2), numpar);
nontarget_par_idx = crossvalind('Kfold', size(nontarget_feat,2), numpar);

k = 1;j = 0;l=1;
target_train_feat = target_feat(:,target_par_idx ~= k);
target_train_metaFeat = target_metaFeat(:,target_par_idx ~= k);
target_train_label = ones(1,size(target_train_feat, 2));
nontarget_train_feat = nontarget_feat(:,nontarget_par_idx ~= k);
nontarget_train_metaFeat = nontarget_metaFeat(:,nontarget_par_idx ~= k);
nontarget_train_label = -ones(1,size(nontarget_train_feat, 2));
train_feat = [nontarget_train_feat target_train_feat];
train_metaFeat = [nontarget_train_metaFeat target_train_metaFeat];
train_label = [nontarget_train_label target_train_label];

target_test_feat = target_feat(:,target_par_idx == k);
target_test_metaFeat = target_metaFeat(:,target_par_idx == k);
target_test_label = ones(1,size(target_test_feat, 2));
nontarget_test_feat = nontarget_feat(:,nontarget_par_idx == k);
nontarget_test_metaFeat = nontarget_metaFeat(:,nontarget_par_idx == k);
nontarget_test_label = -ones(1,size(nontarget_test_feat, 2));
test_feat = [nontarget_test_feat target_test_feat];
test_metaFeat = [nontarget_test_metaFeat target_test_metaFeat];
test_label = [nontarget_test_label target_test_label];

% using matlab predict interface
svm_model{l,j*numpar+k} = fitcsvm(cell2mat(train_feat(l,:))',train_label','KernelFunction','rbf', 'KernelScale', 'auto', 'Standardize', true);
[~, s] = predict(svm_model{l,j*numpar+k}, cell2mat(test_feat(l,:))');
svm_model{l,j*numpar+k} = fitPosterior(svm_model{l,j*numpar+k}, cell2mat(train_feat(l,:))', train_label');
[c, p] = predict(svm_model{l,j*numpar+k}, cell2mat(test_feat(l,:))');
% manually implement classification
c_svm_model = compact(svm_model{l,j*numpar+k});
test_input = cell2mat(test_feat(l,:))';
std_test_input = (test_input-repmat(c_svm_model.Mu,size(test_input,1),1))./repmat(c_svm_model.Sigma,size(test_input,1),1);
nlx = eval(c_svm_model.ScoreTransform);
manual_s = zeros(size(c));
manual_c = zeros(size(c));
manual_p = zeros(size(p,1), 1);
for n = 1:10%size(test_input, 1)
    disp(n)
    wx = zeros(size(c_svm_model.SupportVectors, 1), 1);
    for nn = 1:size(c_svm_model.SupportVectors, 1)
        wx(nn) = c_svm_model.Alpha(nn)*c_svm_model.SupportVectorLabels(nn)*exp(-norm(c_svm_model.SupportVectors(nn,:) - std_test_input(n,:), 2)^2/c_svm_model.KernelParameters.Scale^2);
    end
    score = sum(wx)+c_svm_model.Bias;
    manual_s(n) = score;
    manual_c(n) = sign(score);
    manual_p(n) = nlx(score);
end
