function M = xentropymx(A, B)
% Cross entropy matrix

M = zeros(size(A,2), size(B, 2));
for k = 1:size(A, 2)
    for l = 1:size(B,2)
        M(k,l) = A(:,k)'*log(B(:,l));
    end
end