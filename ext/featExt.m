function [feat, metaFeat] = featExt(data, srate, frameLen, featSrc)
% data - Nx1
% srate - sampling rate
% frameLen - frame length

% add tiny noise to ensure no cepst frame is 0
data = data + randn(numel(data),1)*0.0001;
data = resample(data, 44100, srate);
srate = 44100;

if (featSrc == 1)
    [ridgetracker, spec, speclog_prob, specbacktrackcumsum] = ridgeDet2(data, srate, -5.0, frameLen);
    
    mTF = zeros(5,2);
    sTF = zeros(5,2);
    for l = 1:numel(ridgetracker.B)
        t = ridgetracker.B{l}(:,2); t = t-min(t); % convert to duration
        f = ridgetracker.B{l}(:,1);
        mTF(l,:) = [mean(f) mean(t)];
        sTF(l,:) = [std(f) std(t)];
    end
    
    % D x 1
    feat = [mTF;sTF]; feat = reshape(feat, numel(feat),1);
    metaFeat = ridgetracker;
elseif (featSrc == 2)
    [ridgetracker, spec, speclog_prob, specbacktrackcumsum] = ridgeDet2(data, srate, -5.0, frameLen);
    
    speclog_prob = colvecnorm(exp(speclog_prob), 1); % Normalize prob
    % zero-pad/truncate post and add a sentinel packet
    if (size(speclog_prob, 2) < 512)
        speclog_prob = [size(speclog_prob, 2)*ones(size(speclog_prob, 1), 1) padarray(speclog_prob, [0, 511-size(speclog_prob, 2)], 'post')];
    else
        speclog_prob = [511*ones(size(speclog_prob, 1), 1) speclog_prob(:,1:511)];
    end
    % reshape
    speclog_prob = reshape(speclog_prob, numel(speclog_prob), 1);
    
    % D(128).(1+N(511)) x 1
    feat = speclog_prob;
    metaFeat = ridgetracker;
elseif (featSrc == 3)
    all_cepst = melcepst(data, srate, 'NdD', 25, 40, 2^floor(log2(frameLen*srate)), 2^floor(log2(frameLen*srate))/2)';
    cepst = all_cepst(1:25,:); 
    delta_cepst = all_cepst(26:50,:);
    delta_delta_cepst = all_cepst(51:75,:);

    min_feat= min(cepst, [], 2);
    max_feat= max(cepst, [], 2);
    median_feat= median(cepst, 2);
    mean_feat= mean(cepst, 2);
    var_feat= var(cepst, 0, 2); % default normalization
    skew_feat= skewness(cepst, 1, 2);
    kurtosis_feat= kurtosis(cepst, 1, 2);
    mean_delta_feat = mean(delta_cepst, 2);
    mean_delta_delta_feat = mean(delta_delta_cepst, 2);
    agg_cepst = [min_feat;max_feat;median_feat;mean_feat;var_feat;skew_feat;kurtosis_feat;mean_delta_feat;mean_delta_delta_feat];
    
    % D(225)x 1 
    feat = agg_cepst;
    metaFeat = all_cepst;
elseif (featSrc == 4)
    all_cepst = abs(melcepst(data, srate, 'NdD', 25, 40, 2^floor(log2(frameLen*srate)), 2^floor(log2(frameLen*srate))/2))';
    cepst = all_cepst(1:25,:);
    % zero-pad/truncate post and add a sentinel packet
    if (size(cepst, 2) < 512)
        cepst = [size(cepst, 2)*ones(size(cepst, 1), 1) padarray(cepst, [0, 511-size(cepst, 2)], 'post')];
    else
        cepst = [511*ones(size(cepst, 1), 1) cepst(:,1:511)];
    end
    % reshape
    cepst = reshape(cepst, numel(cepst), 1);
    
    % D(25).(1+N(511)) x 1
    feat = cepst;
    metaFeat = all_cepst;
end