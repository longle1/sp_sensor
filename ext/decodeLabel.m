function labString = decodeLabel(labelIdx)

if labelIdx == 1
    labString = 'air_conditioner';
elseif labelIdx == 2
    labString = 'car_horn';
elseif labelIdx == 3
    labString = 'children_playing';
elseif labelIdx == 4
    labString = 'dog_bark';
elseif labelIdx == 5
    labString = 'drilling';
elseif labelIdx == 6
    labString = 'engine_idling';
elseif labelIdx == 7
    labString = 'gun_shot';
elseif labelIdx == 8
    labString = 'jackhammer';
elseif labelIdx == 9
    labString = 'siren';
elseif labelIdx == 10
    labString = 'street_music';
elseif labelIdx == 11
    labString = 'knock';
elseif labelIdx == 12
    labString = 'door_slam';
elseif labelIdx == 13
    labString = 'steps';
elseif labelIdx == 14
    labString = 'chair_moving';
elseif labelIdx == 15
    labString = 'cup_jingle';
elseif labelIdx == 16
    labString = 'paper_wrapping';
elseif labelIdx == 17
    labString = 'key_jingle';
elseif labelIdx == 18
    labString = 'keyboard_type';
elseif labelIdx == 19
    labString = 'phone_ring';
elseif labelIdx == 20
    labString = 'applause';
elseif labelIdx == 21
    labString = 'cough';
elseif labelIdx == 22
    labString = 'laugh';
elseif labelIdx == 23
    labString = 'speech';
elseif labelIdx == 24
    labString = 'whistle';
elseif labelIdx == 25
    labString = 'finger_snap';
else
    labString = 'undefined';
end