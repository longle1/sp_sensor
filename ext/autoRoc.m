function [pd, pf, thres] = autoRoc(stat, label, N)
% Compute ROC curves (N points) automatically from the statistics and
% labels
% 
% Long Le
% University of Illinois
%

if (~islogical(label))
    labelVal = unique(label);
    if (numel(labelVal) == 2)
        label(label == labelVal(1)) = 0;
        label(label == labelVal(2)) = 1;
        label = logical(label);
    else
        error('label must be logical')
    end
elseif (~isequal(size(stat), size(label)))
    error('data and label must be of the same size')
elseif numel(label) == 0
    error('empty input')
end

thresSpace = unique(stat);
if (nargin <=2)
    thres = thresSpace;
else
    %thres = min(thresSpace):(max(thresSpace)-min(thresSpace))/(N-1):max(thresSpace);
    thres = quantile(thresSpace,[0:1/(N-1):1]);
end
pd = zeros(1, length(thres));
pf = zeros(1, length(thres));
for k = 1:length(thres)
    pd(k) = mean(stat(label) >= thres(k));
    pf(k) = mean(stat(~label) >= thres(k));
end
% Ensure valid probability
pd(pd == 0) = eps;
pf(pf == 0) = eps;
