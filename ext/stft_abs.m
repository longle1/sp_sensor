function spec = stft_abs(x)

Nblk = 128;
Nfc = 128;
w = hann(Nblk*2)';

data = enframe(x,Nblk*2, Nblk*2);
spec = zeros(Nfc, size(data,1));

for k = 1:size(data,1)
    s = abs(fft(w.*data(k,:)));
    s = s(1:Nfc);
    spec(:,k) = s;
end
