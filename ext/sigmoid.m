function y = sigmoid(x,a,b)
y = 1/(1+exp(a*x+b));