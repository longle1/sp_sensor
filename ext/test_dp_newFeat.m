% Application to detect whistle/GCW.
%
% Long Le
% University of Illinois
%

clear all; % close all
addpath(genpath('../acousticsearch/common'))

PLOT = false;

datFolder{1} = '../sm-paper/genCascade/data/GCW/';
datFolder{2} = '../acousticsearch/data/whistlecommand/';
datFolder{3} = '../matlab-realtime-audioprocessing/TIDIGIT_adults_crop_crop/';
datIdx = 0;
for l = 1:numel(datFolder)
    file = dir([datFolder{l} '*.wav']);
    filename = cell(numel(file),1);
    for k = 1:numel(file)
        filename{k} = [datFolder{l} file(k).name];
        try
            [y, fs] = wavread(filename{k});
        catch MException
            [y, fs] = readsph(filename{k});
        end
        y = y(:,1);
        if (fs > 20000)
            h = fir1(8, 20000/fs); % low pass filter
            y = filter(h, 1, y);
            y = resample(y, 20000, fs);
            fs = 20000;
        end
        datIdx = datIdx + 1;
        data{datIdx} = y;
        label(datIdx) = l;
    end
end

%% Partition the data into train and test
target_class = 2;
feat_thresh = [-2.0 -4.0 -2.0];
target_idx = find(label == target_class); % index of the target class
nontarget_idx = find(label ~= target_class);

numpar = 2; % number of train partitions
ind = false(1,length(target_idx));
ind(1:fix(length(target_idx)/numpar)) = true;
for paridx = 1:numpar
    train_target_idx{paridx} = target_idx(ind); 
    train_nontarget_idx{paridx} = nontarget_idx(ind);
    test_idx{paridx} = setdiff([1:length(label)], [train_target_idx{paridx} train_nontarget_idx{paridx}]);
    ind = circshift(ind, [0 fix(length(target_idx)/numpar)]);
end

%% Classification
accuracy = zeros(numpar, 1);
for paridx = 1:numpar
    %%% Training
    train_target_feat = cell(1, numel(train_target_idx{paridx}));
    for k = 1:numel(train_target_idx{paridx})
        cur_dat = data{train_target_idx{paridx}(k)};
        [ridgetracker, spec, speclog_prob_noise_ridge, specbacktrackcumsum] = ridgeDet2(cur_dat, fs, feat_thresh(target_class));
        train_target_feat{k} = colvecnorm(exp(speclog_prob_noise_ridge), 1);
    end
    
    train_nontarget_feat = cell(1, numel(train_nontarget_idx{paridx}));
    for k = 1:numel(train_nontarget_idx{paridx})
        cur_dat = data{train_nontarget_idx{paridx}(k)};
        [ridgetracker, spec, speclog_prob_noise_ridge, specbacktrackcumsum] = ridgeDet2(cur_dat, fs, feat_thresh(target_class));
        train_nontarget_feat{k} = colvecnorm(exp(speclog_prob_noise_ridge), 1);
    end
    
    train_feat = [train_target_feat train_nontarget_feat];
    
    score = zeros(numel(train_feat), numel(train_target_feat));
    for k = 1:numel(train_feat)
        for l = 1:numel(train_target_feat)
            CM = kldivmx(train_feat{k}, train_target_feat{l});
            [p,q,C] = dpfast(CM);
            score(k, l) = C(end, end);
        end
    end
    agg_score = median(score, 2);
    % Find maximum margin threshold
    binlabel = label([train_target_idx{paridx} train_nontarget_idx{paridx}]);
    binlabel(binlabel ~= target_class) = -1;
    binlabel(binlabel == target_class) = 1; % ordering matters, as 1 is also a target class
    svm_model = svmtrain(agg_score, binlabel);
    thresh = svm_model.Bias*svm_model.ScaleData.scaleFactor-svm_model.ScaleData.shift;
    
    %%% Testing
    score = zeros(numel(test_idx{paridx}), numel(train_target_feat));
    for k = 1:numel(test_idx{paridx})
        cur_dat = data{test_idx{paridx}(k)};
        [ridgetracker, spec, speclog_prob_noise_ridge, specbacktrackcumsum] = ridgeDet2(cur_dat, fs, feat_thresh(target_class));
        feat = colvecnorm(exp(speclog_prob_noise_ridge), 1);
        for l = 1:numel(train_target_feat)
            %SM = simmx(feat, train_feat{l});
            %[p,q,C] = dpfast(1-SM);
            CM = kldivmx(feat, train_target_feat{l});
            [p,q,C] = dpfast(CM);
            score(k, l) = C(end, end);
            
            if (PLOT)
                figure; hold on; 
                imagesc(C)
                plot(q,p,'r');
                axis tight
            end
        end
    end
    agg_score = median(score, 2); % aggregate score
    pred = agg_score < thresh;
    gt = (label(test_idx{paridx}) == target_class); % ground truth
    accuracy(paridx) = mean(pred(:) == gt(:));
end