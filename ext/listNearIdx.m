function s_idx = listNearIdx(pi1,piSpace)

n = abs(pi1-piSpace(:));
[s_val, s_idx] = sort(n, 'ascend');