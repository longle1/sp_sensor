function [classData, classDataCnt] = readDataset(datFolderString, exclude_datFolderString, file_limit, time_limit)

datFolder = dir(datFolderString);
ind = [datFolder(:).isdir];
datFolder = datFolder(ind);
datFolder(ismember({datFolder.name},[{'.','..'}, exclude_datFolderString])) = [];

classDataCnt = 0;
for l = 1:numel(datFolder)
    prefix = fullfile(datFolderString, datFolder(l).name);
    file = [dir(fullfile(prefix, '*.wav')); dir(fullfile(prefix, '*.mp3')); dir(fullfile(prefix, '*.flac'))];
    filename = cell(numel(file),1);
    for k = 1:min(file_limit, numel(file))
        filename{k} = fullfile(prefix, file(k).name);
        
        fid = fopen([strtok(filename{k},'.') '.csv']);
        m = textscan(fid, '%f%f%d%s', 'delimiter', ',');
        fclose(fid);
        
        % Limit data to process
        info = audioinfo(filename{k}); fs = info.SampleRate;
        for j = 1:numel(m{1})
            classDataCnt = classDataCnt + 1;
            try
                y = audioread(filename{k}, [1+fix(m{1}(j)*fs), min([1+fix(m{1}(j)*fs)+fs*time_limit, fix(m{2}(j)*fs), info.TotalSamples])]);
            catch ex
                info
                [m{1} m{2}]
                rethrow(ex);
            end
            classData(classDataCnt).data = y(:,1);
            classData(classDataCnt).srate = fs;
            classData(classDataCnt).label = encodeLabel(m{4}(j));
            classData(classDataCnt).filename = filename{k};
        end
    end
end