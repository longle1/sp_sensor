\section{Derivation of Equation \eqref{eqn:opt}}\label{subsec:app2}
The following steps follow from standard arguments that can be found in \cite{levy2008principles}.

First, the objective in \eqref{eqn:min} is expanded
$$
\begin{aligned}
&\min_f\mathbb{E}[(f(Y^N) - q(X^N))^2] \\
= & \int \min_f  \sum_{x^N} (f(y^N) - q(x^N))^2 p(x^N|y^N) p(y^N) \, \mathrm{d}y^N
\end{aligned}
$$
The first-order optimality condition yields
$$
\begin{aligned}
&\sum_{x^N} (f^\ast(y^N) - q(x^N)) p(x^N|y^N) = 0\\
\Leftrightarrow& f^\ast(y^N) = \sum_{x^N} \sum_{i=1}^N\phi(x_i)\prod_{j=1}^N p(x_j|y_j)\\
\Leftrightarrow& f^\ast(y^N) = \sum_{i=1}^N \sum_{x_i} \phi(x_i) p(x_i|y_i)
\end{aligned}
$$
where the second-to-last line follows from the independence assumption and the definition of $q(x^N)$. The last line
follows from marginalization.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Proof of Theorem \ref{thm:sol2}}\label{subsec:app1}
The strategy of the proof is to decouple the censoring and the estimation (with censored data) steps.
Expanding the objective in \eqref{eqn:min} yields
$$
\begin{aligned}
&\min_{f\in \mathcal{F}_n}\mathbb{E}[(f(Y^N) - q(X^N))^2] \\
= &\min_{g,h} \int \sum_{x^N} (g(h(y^N)) - q(x^N))^2 p(x^N,y^N)  \, \mathrm{d}y^N\\
= &\min_{\mathcal{I}} \min_g \int \sum_{x^N} (g(y^n) - q(x^N))^2 \prod_{i\in\mathcal{I}}
p(x_i,y_i) \prod_{i\in\bar{\mathcal{I}}} p(x_i)  \, \mathrm{d}y^N\\
= &\min_{\mathcal{I}} \int \min_g \sum_{x^N} (g(y^n) - q(x^N))^2 \prod_{i\in\mathcal{I}}
p(x_i|y_i) \prod_{i\in\bar{\mathcal{I}}} p(x_i)  \\ 
&p(y^N) \, \mathrm{d}y^N\\
\end{aligned}
$$
where the optimization over $h$ is replaced by the optimization over $\mathcal{I}$. 
The sets $\mathcal{I}$ and $\bar{\mathcal{I}}$ denote the collection of indices of the observations that are chosen and not chosen by $h$, respectively.
\begin{equation}\label{eqn:I}
\begin{aligned}
\mathcal{I}(y^N) &\in \binom{\{1,\dots, N\}}{n}\\
\bar{\mathcal{I}}(y^N) &= \{1,\dots, N\}\setminus \mathcal{I}
\end{aligned}
\end{equation}
By definition, the cardinalities of $\mathcal{I}$  and $\bar{\mathcal{I}}$ are $n$ and $N-n$, respectively.
With a slight abuse of notation, $y^n$ denotes the observations indexed by $\mathcal{I}$, i.e. $y^n \triangleq \{y_i, i\in \mathcal{I}\}$.
Note that the $\{y_i, i\in \bar{\mathcal{I}}\}$ are marginalized out and the  optimization over $g$ is independent of 
the optimization over $\mathcal{I}$.

The first-order condition for the minimization over $g$ yields
$$
\begin{aligned}
&\sum_{x^N} (g^\ast(y^n) - q(x^N)) \prod_{i\in\mathcal{I}} p(x_i|y_i) 
\prod_{i\in\bar{\mathcal{I}}} p(x_i)  = 0\\
\Leftrightarrow& g^\ast(y^n) = \sum_{x^N} \sum_{i=1}^N\phi(x_i)\prod_{j\in\mathcal{I}} p(x_j|y_j) 
\prod_{j\in\bar{\mathcal{I}}} p(x_j)\\
\Leftrightarrow& g^\ast(y^n) = \sum_{i\in\mathcal{I}} \sum_{x_i} \phi(x_i) p(x_i|y_i)  + 
\sum_{i\in\bar{\mathcal{I}}} \sum_{x_i} \phi(x_i) p(x_i) 
\end{aligned}
$$
Then it remains to find the best set $\mathcal{I}$. 

From \eqref{eqn:I}, there are $\binom{N}{n}$ possible $\mathcal{I}$, which makes finding the best one very difficult.
In what follows it is shown that the best set $\mathcal{I}$ can be constructed 
sequentially without enumerating all combinations.
Let $f_n \triangleq g^\ast\circ h$ for some $h$, i.e. 
$$
f_n(y^N) = \sum_{i\in\mathcal{I}} \sum_{x_i} \phi(x_i) p(x_i|y_i)  + \sum_{i\in\bar{\mathcal{I}}} \sum_{x_i} \phi(x_i) p(x_i) 
$$
Then the following holds.
\begin{equation}\label{eqn:triangleq}
\begin{aligned}
\mathbb{E}[(f_n - q)^2] 
&= \mathbb{E}[(f_n - f^\ast + f^\ast - q)^2]\\
&= \mathbb{E}[(f_n - f^\ast)^2] + \mathbb{E}[(f^\ast - q)^2]
\end{aligned}
\end{equation}
where the second line follows from the orthogonality property of MMSE \cite{levy2008principles}. 
%Specifically,
%$$
%\begin{aligned}
%&\ \mathbb{E}[(f_n - f^\ast)(f^\ast - q)] \\
%&=\mathbb{E}[(f_n(Y^N) - f^\ast(Y^N))(f^\ast(Y^N) - q(X^N))]\\
%&= \mathbb{E}[(f_n(Y^N) - f^\ast(Y^N))\underbrace{(f^\ast(Y^N) - \mathbb{E}_{X^N|Y^N}q(X^N))}_{0}]\\
%&= 0 
%\end{aligned}
%$$
Using \eqref{eqn:triangleq}, \eqref{eqn:min} is equivalent to
$$
\begin{aligned}
&\min_{f_n\in\mathcal{F}_n} \mathbb{E}[(f_n(Y^N) - f^\ast(Y^N))^2]\\
\Leftrightarrow &\min_{\bar{\mathcal{I}}} \mathbb{E}[(\sum_{i\in\bar{\mathcal{I}}(Y^N)} \Delta(Y_i))^2],\quad (\text{Using definition }\eqref{eqn:delta})\\
\Leftrightarrow &\min_{\bar{\mathcal{I}}} \mathbb{E}[\sum_{i\in\bar{\mathcal{I}}(Y^N)} \Delta^2(Y_i)], \quad (\text{Since }\mathbb{E}[\Delta(Y_i)] = 0, \forall i)\\
\Leftrightarrow & \int \Big[\min_{\bar{\mathcal{I}}} \sum_{i\in\bar{\mathcal{I}}(y^N)} \Delta^2(y_i)\Big] p(y^N) dy^N
\end{aligned}
$$
Consequently, the optimal $\bar{\mathcal{I}}^\ast$, or equivalently $\mathcal{I}^\ast$, can be constructed progressively as follows
$$
\begin{aligned}
\text{For }n = N-1: \bar{\mathcal{I}}_1^\ast &= \arg\min_{\{i\} \in\binom{\{1,\dots, N\}}{1}} \Delta^2(y_i)\\
\text{For }n = N-2: \bar{\mathcal{I}}_2^\ast &= \arg\min_{\{i,j\} \in\binom{\{1,\dots, N\}}{2}} \Delta^2(y_i) + \Delta^2(y_j)\\
&= \arg\min_{\substack{\{i\} \in \binom{\{1,\dots, N\}}{1}\\\{j\} \in \binom{\{1,\dots, N\}\setminus \{i\}}{1}}} \Delta^2(y_i) + \Delta^2(y_j)\\
&=\{ \bar{\mathcal{I}}^\ast_1, \arg\min_{\{j\} \in \binom{\{1,\dots, N\}\setminus \bar{\mathcal{I}}^\ast_1}{1}} \Delta^2(y_j) \}\\
&\dots
\end{aligned}
$$
The above sequential process is equivalent to the sorting procedure described in Theorem \ref{thm:sol2}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Proof of Theorem \ref{thm:0-1Loss}}\label{subsec:app5}
The proof here follows the strategy in Appendix \ref{subsec:app1}.
For $n = N$, the objective in \eqref{eqn:01lossobj} is equivalent to
$$
\begin{aligned}
&\max_{f} \mathrm{P}(f(Y^N) = \sum_{i=1}^N\phi(X_i))\\
\Leftrightarrow &\max_{\substack{f_i\\i=1,\dots, N}} \sum_{i=1}^N\mathrm{P}(f_i(Y^N) = \phi(X_i))
\end{aligned}
$$
which yields
$$
f^\ast(y^N) = \sum_{i=1}^N \arg\max_{\phi_i} p(\phi_i|y_i)
$$
where $\phi_i$ is defined in \eqref{eqn:phi_i}. For $n < N$, using the definitions in \eqref{eqn:phi_i_hat}, it can be shown that
$$
f_n(y^n) = \sum_{i\in \mathcal{I}} \hat{\phi}_i(y_i) + \sum_{i\in \bar{\mathcal{I}}} \check{\phi_i}
$$
It remains to find the best set $\mathcal{I}^\ast$. Toward this end, consider the following error functions
$$
\begin{aligned}
\mathrm{P}(f^\ast = q)  &= \mathbb{E}[\sum_{i=1}^Np(\hat{\phi}_i|Y_i)]\\
\mathrm{P}(f_n = q)  &= \mathbb{E}[\sum_{i\in\mathcal{I}}p(\hat{\phi}_i|Y_i) + \sum_{i\in\bar{\mathcal{I}}}p(\check{\phi}_i|Y_i)]
\end{aligned}
$$
Thus
$$
\begin{aligned}
\mathrm{P}(f^\ast = q) - \mathrm{P}(f_n = q) &= \mathbb{E}[\sum_{i\in\bar{\mathcal{I}}} p(\hat{\phi}_i|Y_i)-p(\check{\phi}_i|Y_i)]\\
&=\mathbb{E}[\sum_{i\in\bar{\mathcal{I}}} \Delta_p (Y_i)]
\end{aligned}
$$
where $\Delta_p$ is given in \eqref{eqn:delta_p}
and the optimal $\mathcal{I}^\ast$ can be found by removing the observations with the smallest $\Delta_p$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Proof of Theorem \ref{thm:vsRand}}\label{subsec:app3}
By definition,
$$
R_n(\delta_2) \triangleq \mathbb{E}[\frac{1}{N} \sum_{i=1}^n \mathbf{1}(\delta_2(Y_i) \neq X_i)]+
\mathbb{E}[\frac{1}{N} \sum_{i=n+1}^N \mathbf{1}(-1 \neq X_i)]
$$
and
$$%\begin{equation}\label{eqn:Bayes}
\begin{aligned}
R_r&(\delta_1, \delta_2) \triangleq \\
&\mathbb{E}[\frac{1}{N} \sum_{i=1}^n \mathbf{1}(\delta_2(Y_i) \neq X_i)| \delta_1(Y_1)=1, \dots,  \delta_1(Y_n)=1]\\
+&\mathbb{E}[\frac{1}{N} \sum_{i=n+1}^N \mathbf{1}(-1 \neq X_i)| \delta_1(Y_{n+1}) =-1, \dots,  \delta_1(Y_N)=-1]
\end{aligned}
$$%\end{equation}
where it is assumed without loss of generality that the first $n$ $Y_i$ are uncensored.
Then, using the independent and identical assumption of the joint distribution of $X$ and $Y$ and conditioning on $X$, it is straightforward to show that
\begin{equation}\label{eqn:BayesNaive}
R_n(\delta_2) = \frac{n}{N}\Big[ (1-r_2)\pi_1 + f_2(1-\pi_1)\Big]  + \frac{N-n}{N} \rho_1
\end{equation}
\begin{equation}\label{eqn:BayesRank}
R_r(\delta_1, \delta_2) = \frac{n}{N}\Big[ (1-r_2)\pi_2 + f_2(1-\pi_2)\Big]  + \frac{N-n}{N} \rho_2
\end{equation}
using the definitions in Theorem \ref{thm:vsRand}.
Subtracting \eqref{eqn:BayesRank} from \eqref{eqn:BayesNaive} yields \eqref{eqn:BayesDiff}.
In addition, Equation \eqref{eqn:con} follows directly from the law of total probability.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Proof of Theorem \ref{thm:lowerBound}}\label{subsec:app4}
The Bayes risk of the cascade system in Figure \ref{fig:cascade} is given by
\begin{multline*}
R \triangleq \mathrm{P}(\delta_K(Y) \neq X|\delta^{K-1}(Y)=1)\mathrm{P}(\delta^{K-1}(Y) = 1)\\
+\mathrm{P}(-1 \neq X|\delta_{K-1}(Y)=-1, \delta^{K-2}(Y) = 1)\\
\cdot \mathrm{P}(\delta_{K-1}(Y)=-1, \delta^{K-2}(Y) = 1)\\
\dots\\
+\mathrm{P}(-1\neq X|\delta_1(Y) = -1)\mathrm{P}(\delta_1(Y) = -1)
\end{multline*}
Then, by conditioning on $X$, it is straightforward to show that
\begin{equation}\label{eqn:cascadeRisk}
R = [(1-r_K)\pi_K + f_K(1-\pi_K)](1-\sum_{i=1}^{K-1}\alpha_i) + \sum_{i=1}^{K-1}\rho_{i+1}\alpha_i
\end{equation}
using the definitions in Theorem \ref{thm:lowerBound}.
\eqref{eqn:cascadeRisk} can be minimized by controlling the admissible recall and false alarm probabilities of each detector in the chain, 
subject to an inter-stage constraint induced by the law of total probability; i.e., the sum of the censored and uncensored positive examples  
at a stage must equal the total number of positive examples available to that stage. Namely,
\begin{equation}\label{eqn:sequential}
\begin{aligned}
\min_{\substack{r_i\in \mathcal{R}_i\\ f_i\in\mathcal{F}_i\\i = 1, \dots, K}} &R\\
\text{s.t.} &\frac{1-\sum_{j=1}^{i}\alpha_j}{\alpha_i + 1-\sum_{j=1}^{i}\alpha_j}\pi_{i+1} + 
\frac{\alpha_i}{\alpha_i +1- \sum_{j=1}^{i}\alpha_j}\rho_{i+1} = \pi_i,\\
& i = 1, \dots, K-1
\end{aligned}
\end{equation}

Problem \eqref{eqn:sequential} is a sequential decision-making problem \cite{bertsekas1976dynamic}, 
where $r_i, f_i$ are the control variables and $\pi_i$ is the state variable at stage $i$. 
The state dynamic is governed by Bayes' rule and is given in \eqref{eqn:dynamic}. \eqref{eqn:sequential} can then be solved by a dynamic-programming 
procedure, which is given in Theorem \ref{thm:lowerBound}.