function normA = colvecnorm(A, p)

normA = zeros(size(A));
for k = 1:size(A, 2)
    normA(:,k) = A(:,k)/norm(A(:,k), p);
end