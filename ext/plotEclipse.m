function h = plotEclipse(xCenter, yCenter, xRadius, yRadius)
theta = 0 : 0.01 : 2*pi;
x = xRadius * cos(theta) + xCenter;
y = yRadius * sin(theta) + yCenter;
h = plot(x, y, 'r', 'LineWidth', 2);