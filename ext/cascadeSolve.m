function [recall, falarm, val, V, backPtr, ctrlIdx] = cascadeSolve(alpha, pi1, recallSpace, falarmSpace)
% Solve the cascade
% al - 1xN cost proportion, N is the number of stages, must sum to 1
% pi1 - P_1(H = 1), must be in (0, 1)
% recallSpace - custom recall
% flarmSpace - custom falarm
%
% recall - 1xN, opt recall
% falarm - 1xN, opt false alarm
% val - the problem value at pi1
% Long Le
% University of Illinois
%

numStage = numel(alpha);
recall = zeros(1, numStage);
falarm = zeros(1, numStage);

piInterval = 1e-3;
piSpace = [0:piInterval:1]';
if ~exist('recallSpace','var') || ~exist('falarmSpace','var')
    error('no admissible space');
end
V = zeros(numel(piSpace), numStage);
backPtr = zeros(numel(piSpace), numStage);
ctrlIdx = zeros(numel(piSpace), numStage);

% Init
for m = 1:numel(piSpace)
    g = Inf*ones(size(recallSpace,1),1);
    for l = 1:size(recallSpace, 1)
        g(l) = (1-sum(alpha(1:numStage-1)))*((1-recallSpace(l,numStage))*piSpace(m) + falarmSpace(l,numStage)*(1-piSpace(m)));
    end
    [V(m,numStage), idx] = min(g(:));
    ctrlIdx(m,numStage) = idx;
end
% Backward Recursive
lambda = [5:10];
for k = numStage-1:1
    for m = 1:numel(piSpace)
        L = Inf*ones(size(recallSpace,1),numel(lambda));
        m_next = zeros(size(recallSpace,1),1);
        obj_next = zeros(size(recallSpace,1),1);
        for l = 1:size(recallSpace, 1)
            for jj = 1:numel(lambda)
                % Solve the constrained optimization problem using
                % Lagrangian method to improve numerical stability
                prec = recallSpace(l,k)*piSpace(m)/(recallSpace(l,k)*piSpace(m) + falarmSpace(l,k)*(1-piSpace(m)));
                rho = (1-recallSpace(l,k))*piSpace(m)/((1-recallSpace(l,k))*piSpace(m) + (1-falarmSpace(l,k))*(1-piSpace(m)));
                idx = listNearIdx(prec, piSpace);
                m_next(l) = idx(1);
                obj_next(l) = alpha(k)*rho + V(m_next(l),k+1);
                L(l,jj) = obj_next(l) + lambda(jj)*abs( (1-sum(alpha(1:k)))/(alpha(k)+(1-sum(alpha(1:k))))*prec + alpha(k)/(alpha(k)+(1-sum(alpha(1:k))))*rho - piSpace(m) );
            end
        end
        [~, idx] = min(L(:)); [sub1, sub2] = ind2sub(size(L), idx);
        V(m,k) = obj_next(sub1);
        backPtr(m,k) = m_next(sub1);
        ctrlIdx(m,k) = sub1;
    end
end

% Forward tracking
m = listNearIdx(pi1, piSpace);
for k = 1:numel(m)
    if ~isinf(V(m(k), 1))
        val = V(m(k), 1);
        m = m(k);
        break;
    end
end
for k = 1:numStage
    recall(k) = recallSpace(ctrlIdx(m,k), k);
    falarm(k) = falarmSpace(ctrlIdx(m,k), k);
    m = backPtr(m, k);
end