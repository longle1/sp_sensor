function ttpr()
% Tag-Train-Predictive Search-Repeat
%
% Long Le
% University of Illinois
%

clear all; %close all;
addpath(genpath('../IlliadAccess/matlab'));

GEN = false;

%% Tag
DB = 'publicDb';
USER = 'publicUser';
PWD = 'publicPwd';

% Get event with tags
q.limit = 10000;
q.t1 = datenum(2015,3,24,0,0,0); q.t2 = datenum(2015,5,31,0,0,0); 
%q.f1 = 0; q.f2 = 6000;
%q.dur1 = 0.6; q.dur2 = 10.0;
%q.lnp2 = 6e4;
%q.loc(1) = 40.1069855; q.loc(2) = -88.2244681; q.rad = 1;
events = IllQueryCol(DB, USER, PWD, 'event', q);

feat_desc = zeros(numel(events),20);
label_desc = zeros(numel(events),1);
for k = 1:numel(events)
    mTF = zeros(5,2);
    sTF = zeros(5,2);
    % Must sort because the ridge index (desirable) might be different from the field index
    desc = sort(fieldnames(events{k}.TFRidge));
    for l = 1:numel(desc)
        t = events{k}.TFRidge.(desc{l}).time; t = t-min(t); % convert to duration
        f = events{k}.TFRidge.(desc{l}).freq;
        mTF(l,:) = [mean(f) mean(t)];
        sTF(l,:) = [std(f) std(t)];
    end
    feat_desc(k,:) = reshape([mTF;sTF], 20, 1)';
    if (isfield(events{k}, 'tag'))
        label_desc(k) = encodeLabel(strtok(events{k}.tag));
    else
        label_desc(k) = encodeLabel('undefined');
    end
end
disp('histc of labels')
disp([unique(label_desc) histc(label_desc, unique(label_desc))]);
% remove undefined desc since the positive class might be in there
idx = label_desc == -1;
feat_desc(idx,:) = [];
label_desc(idx) = [];

%% Train
% Train model using tag
rng('default')
numpar = 3;
classList = unique(label_desc);
target_class_idx = find(classList==21|classList==23|classList==24|classList==25)';
for k = target_class_idx
    disp(classList(k))
    bin_label = double(label_desc == classList(k)); bin_label(bin_label == 0) = -1;
    
    [train_feat, train_label, test_feat, test_label] = xvalid(feat_desc, bin_label, numpar);
    for l = 1:numpar
        svm_model{1,l} = fitcsvm(train_feat{l},train_label{l},'KernelFunction','rbf', 'KernelScale', 'auto', 'Standardize', true, 'BoxConstraint', 1);
        svm_model{1,l} = fitPosterior(svm_model{1,l}, train_feat{l}, train_label{l});
        [c, ~] = predict(svm_model{1,l}, test_feat{l});
        seed = rng;
        if isempty(find(c==1,1))
            c(randi(numel(c))) = 1;
        elseif isempty(find(c==-1,1))
            c(randi(numel(c))) = -1;
        end
        rng(seed); % restore rand state
        recall(1,l) = mean(c(test_label{l}==1)==1);
        falarm(1,l) = mean(c(test_label{l}==-1)==1);
        prec(1,l) = mean(test_label{l}(c==1)==1);
        rho(1,l) = mean(test_label{l}(c==-1)==1);
    end

    save(sprintf('model/target_%d', classList(k)), 'svm_model', 'recall', 'falarm', 'prec', 'rho');
end
if (GEN)
    % Generate svm json
    for k = target_class_idx
        fname = sprintf('model/target_%d.mat', classList(k));
        if exist(fname, 'file') ~= 2
            continue;
        end
        load(fname, 'svm_model', 'recall', 'prec');
        f1 = 2*recall.*prec./(recall+prec);
        [~, idx] = max(f1(1,:));
        json = svm2json(svm_model{1,idx});
        fid = fopen(sprintf('../IlliadWeb/public_html/data/%s.json', decodeLabel(classList(k))), 'w');
        fprintf(fid, '%s', json);
        fclose(fid);
    end
end

%% Predictive search
target_class = 24;
load(sprintf('model/target_%d', target_class), 'svm_model', 'recall', 'prec');
f1 = 2*recall.*prec./(recall+prec);
[~, idx] = max(f1(1,:));
[~, p] = predict(svm_model{1, idx}, feat_desc);
score = zeros(numel(events),1);
for k = 1:numel(events)
    score(k) = sum(events{k}.logProb)*p(k,2);
end