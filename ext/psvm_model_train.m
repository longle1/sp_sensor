% Probabilistic SVM model training based on 
%   sample-aggregate TF feature 
%   log noise prob
%   time-aggregate cepstrum feature
%   cepstrum feature
%
% Long Le
% University of Illinois
%

clear all; % close all

addpath(genpath('../acousticsearch/common'))
addpath(genpath('../voicebox'))

PLOT = false;
frameLen = 0.005; % second
featSrc = [1 3];
file_limit = Inf;
time_limit = 15; % second
numpar = 3; % number of train partitions
est_method = 1;

% Organize data into class-based clips
[classData, classDataCnt] = readDataset('e:/Research/UrbanSound/data/', {}, file_limit, time_limit);
empty_ind = false(1, classDataCnt);
for k = 1:classDataCnt
    if isempty(classData(k).data)
        empty_ind(k) = true;
    end
end
classData(empty_ind) = [];
classDataCnt = numel(classData);

%% Feature extraction
fStartTime = tic;

feat = cell(numel(featSrc), classDataCnt);
metaFeat = cell(numel(featSrc), classDataCnt);
for l = 1:numel(featSrc)
    for k = 1:classDataCnt
        [feat{l,k}, metaFeat{l,k}] = featExt(classData(k).data, classData(k).srate, frameLen, featSrc(l));
    end
end

fElapsedTime = toc(fStartTime)

label = [classData.label];
pi1 = zeros(1, 10);
for l = 1:10 % all target_class
    pi1(l) = mean(label==l);
end

metaFeat(2,:) = [];
save('feat', 'feat', 'metaFeat');
save('label', 'label', 'pi1');

%% Training/Classification
svm_model = cell(3, 6);
for target_class = 1:10
    rng('default');
    
    bin_label = double(label == target_class); bin_label(bin_label == 0) = -1;
    
    cStartTime = tic;

    for j = 0:1
        % Partition the data into train and test
        % The first argument to xvalid is (20+225+1)xN
        [train_feat, train_label, test_feat, test_label] = xvalid([cell2mat(feat)' cellfun(@(c) -sum(c.ridge_loglik), metaFeat(1,:))'], bin_label', numpar);

        for k = 1:numpar
            for l = 1:3
                if (l == 1)
                    svm_model{l,j*numpar+k} = fitcsvm(train_feat{k}(:,1:20),train_label{k},'KernelFunction','rbf', 'KernelScale', 'auto', 'Standardize', true);
                    svm_model{l,j*numpar+k} = fitPosterior(svm_model{l,j*numpar+k}, train_feat{k}(:,1:20),train_label{k});
                    [c, post] = predict(svm_model{l,j*numpar+k}, test_feat{k}(:,1:20));
                elseif (l == 2)
                    svm_model{l,j*numpar+k} = fitcsvm(train_feat{k}(:,21:245),train_label{k},'KernelFunction','rbf', 'KernelScale', 'auto', 'Standardize', true);
                    svm_model{l,j*numpar+k} = fitPosterior(svm_model{l,j*numpar+k}, train_feat{k}(:,21:245), train_label{k});
                    [c, post] = predict(svm_model{l,j*numpar+k}, test_feat{k}(:,21:245));
                elseif (l == 3)
                    svm_model{l,j*numpar+k} = fitcdiscr(train_feat{k}(:,1:20),train_label{k});
                    [c, post] = predict(svm_model{l,j*numpar+k}, test_feat{k}(:,1:20));
                end
                seed = rng;
                if isempty(find(c==1,1))
                    c(randi(numel(c))) = 1;
                elseif isempty(find(c==-1,1))
                    c(randi(numel(c))) = -1;
                end
                rng(seed); % restore rand state
                recall(l,j*numpar+k) = mean(c(test_label{k}==1)==1);
                falarm(l,j*numpar+k) = mean(c(test_label{k}==-1)==1);
                prec(l,j*numpar+k) = mean(test_label{k}(c==1)==1);
                rho(l,j*numpar+k) = mean(test_label{k}(c==-1)==1);
                
                [pd, pf] = autoRoc(post(:,2), test_label{k}, 20);
                auroc(l,j*numpar+k) = -trapz(pf, pd); % negative due to increasing threshold
            end


            obs_con = fix([0.05 0.1 0.2 0.4 0.6 0.8 1]*size(test_feat{k}, 1));
            if est_method == 1
                % Compute the posterior
                [~, post] = predict(svm_model{1,j*numpar+k}, test_feat{k}(:,1:20));
                % Compute anomaly weight
                ano_weight = test_feat{k}(:,246);
                qUpVal = quantile(ano_weight, 0.75);
                qLowVal = quantile(ano_weight, 0.20);
                ano_weight(ano_weight > qUpVal) = qUpVal; % Upper cap
                ano_weight(ano_weight < qLowVal) = qLowVal; % Lower cap
                ano_weight = ano_weight./norm(ano_weight, 1);
                activation = post(:, 2).*ano_weight;
                [~, s_idx] = sort(activation, 'descend');

                s_label{j*numpar+k} = test_label{k}(s_idx);
                c_all{j*numpar+k} = predict(svm_model{2,j*numpar+k}, test_feat{k}(s_idx,21:245));
            elseif est_method == 2
                seed = rng;
                s_idx = randperm(size(test_feat{k}, 1));
                rng(seed); % restore rand state

                s_label{j*numpar+k} = test_label{k}(s_idx);
                c_all{j*numpar+k} = predict(svm_model{2,j*numpar+k}, test_feat{k}(s_idx,21:245));
            elseif est_method == 3
                % Compute anomaly weight
                ano_weight = test_feat{k}(:,246);
                qUpVal = quantile(ano_weight, 0.80);
                qLowVal = quantile(ano_weight, 0.15);
                ano_weight(ano_weight > qUpVal) = qUpVal; % Upper cap
                ano_weight(ano_weight < qLowVal) = qLowVal; % Lower cap
                ano_weight = ano_weight./norm(ano_weight, 1);
                activation = ano_weight;
                [~, s_idx] = sort(activation, 'descend');

                s_label{j*numpar+k} = test_label{k}(s_idx);
                c_all{j*numpar+k} = predict(svm_model{2,j*numpar+k}, test_feat{k}(s_idx,21:245));
            elseif est_method == 4
                % Compute the posterior
                [~, post] = predict(svm_model{3,j*numpar+k}, test_feat{k}(:,1:20));
                % Compute anomaly weight
                ano_weight = test_feat{k}(:,246);
                qUpVal = quantile(ano_weight, 0.75);
                qLowVal = quantile(ano_weight, 0.20);
                ano_weight(ano_weight > qUpVal) = qUpVal; % Upper cap
                ano_weight(ano_weight < qLowVal) = qLowVal; % Lower cap
                ano_weight = ano_weight./norm(ano_weight, 1);
                activation = post(:, 2).*ano_weight;
                [~, s_idx] = sort(activation, 'descend');

                s_label{j*numpar+k} = test_label{k}(s_idx);
                c_all{j*numpar+k} = predict(svm_model{2,j*numpar+k}, test_feat{k}(s_idx,21:245));
            elseif est_method == 5
                % Compute the posterior
                [~, post] = predict(svm_model{1,j*numpar+k}, test_feat{k}(:,1:20));
                % Compute anomaly weight
                ano_weight = test_feat{k}(:,246);
                qUpVal = quantile(ano_weight, 0.75);
                qLowVal = quantile(ano_weight, 0.20);
                ano_weight(ano_weight > qUpVal) = qUpVal; % Upper cap
                ano_weight(ano_weight < qLowVal) = qLowVal; % Lower cap
                ano_weight = ano_weight./norm(ano_weight, 1);
                activation = (max(post,[],2) - post(:,1)).*ano_weight;
                zidx = activation==0;
                activation(zidx) = ano_weight(zidx)/max(ano_weight(zidx))*min(activation(activation > 0));
                [~, s_idx] = sort(activation, 'descend');

                s_label{j*numpar+k} = test_label{k}(s_idx);
                c_all{j*numpar+k} = predict(svm_model{2,j*numpar+k}, test_feat{k}(s_idx,21:245));
            end

            for l = 1:numel(obs_con)
                c = c_all{j*numpar+k}(1:obs_con(l)); c = [c; -ones(numel(s_label{j*numpar+k})-numel(c),1)];
                seed = rng;
                if isempty(find(c==1,1))
                    c(randi(numel(c))) = 1;
                elseif isempty(find(c==-1,1))
                    c(randi(numel(c))) = -1;
                end
                rng(seed); % restore rand state
                recall2(l,j*numpar+k) = mean(c(s_label{j*numpar+k}==1)==1);
                falarm2(l,j*numpar+k) = mean(c(s_label{j*numpar+k}==-1)==1);
                prec2(l,j*numpar+k) = mean(s_label{j*numpar+k}(c==1)==1);
                rho2(l,j*numpar+k) = mean(s_label{j*numpar+k}(c==-1)==1);
                
                c = c_all{j*numpar+k}(1:obs_con(l)); lab = s_label{j*numpar+k}(1:obs_con(l));
                seed = rng;
                if isempty(find(c==1,1))
                    c(randi(numel(c))) = 1;
                elseif isempty(find(c==-1,1))
                    c(randi(numel(c))) = -1;
                end
                if isempty(find(lab==1,1))
                    lab(randi(numel(lab))) = 1;
                elseif isempty(find(lab==-1,1))
                    lab(randi(numel(lab))) = -1;
                end
                rng(seed); % restore rand state
                recall2a(l,j*numpar+k) = mean(c(lab==1)==1);
                falarm2a(l,j*numpar+k) = mean(c(lab==-1)==1);
                prec2a(l,j*numpar+k) = mean(lab(c==1)==1);
                rho2a(l,j*numpar+k) = mean(lab(c==-1)==1);
                
                c = ones(obs_con(l),1); c = [c; -ones(numel(s_label{j*numpar+k})-numel(c),1)];
                seed = rng;
                if isempty(find(c==1,1))
                    c(randi(numel(c))) = 1;
                elseif isempty(find(c==-1,1))
                    c(randi(numel(c))) = -1;
                end
                rng(seed); % restore rand state
                recall1(l,j*numpar+k) = mean(c(s_label{j*numpar+k}==1)==1);
                falarm1(l,j*numpar+k) = mean(c(s_label{j*numpar+k}==-1)==1);
                prec1(l,j*numpar+k) = mean(s_label{j*numpar+k}(c==1)==1);
                rho1(l,j*numpar+k) = mean(s_label{j*numpar+k}(c==-1)==1);
            end

            if PLOT
                figure;
                subplot(211); stem(c_all{j*numpar+k})
                xlabel('c\_all')
                subplot(212); stem(s_label{j*numpar+k})
                xlabel('s\_label')
            end
        end
    end

    cElapsedTime = toc(cStartTime)

    save(sprintf('model/target_%d', target_class), 'svm_model', 'recall', 'falarm', 'prec', 'rho', 'auroc');
    save(sprintf('result/method_%d_target_%d', est_method, target_class), 'recall1', 'falarm1', 'prec1', 'rho1', 'recall2', 'falarm2', 'prec2', 'rho2', 'recall2a', 'falarm2a', 'prec2a', 'rho2a');
end