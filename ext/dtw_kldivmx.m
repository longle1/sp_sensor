function G = dtw_kldivmx(U, V)

featdim = 128;
G = zeros(size(U, 1), size(V, 1));
for k = 1:size(U, 1)
    for l = 1:size(V, 1)
        % Reshape input into matrix
        UU = reshape(U(k,:), featdim, numel(U(k,:))/featdim);
        VV = reshape(V(l,:), featdim, numel(V(l,:))/featdim);
        % extract data from header
        UU = UU(:,2:UU(1,1)+1);
        VV = VV(:,2:VV(1,1)+1);
        
        % DTW
        CM = kldivmx(UU, VV);
        [p,q,C] = dpfast(CM);
        G(k, l) = C(end, end);
    end
end