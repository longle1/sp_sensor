function [model, c] = dsvlearning(sumdata, data, coarse_thresh, fine_thresh)

numdat = size(sumdata, 1);
c = zeros(numdat, 1);

sv_idx = 1; % Assuming the first data point is the target class
c(sv_idx) = 1;
for k = 1:numdat
    if (ismember(k, sv_idx))
        continue;
    end
    
    simm = simmx(sumdata(k,:)', mean(sumdata(sv_idx, :), 1)');
    if (simm >= coarse_thresh) % the coarse feature is sufficiently alike
        % Check if the current data is a variant
        cost = zeros(1, numel(sv_idx));
        for l = 1:numel(sv_idx)
            SM = simmx(data{k}, data{sv_idx(l)});
            [p,q,C] = dpfast(1-SM);
            cost(l) = C(end, end);
        end
        
        if median(cost) < fine_thresh(2)
            c(k) = 1;
            if median(cost) >= fine_thresh(1) % the fine-grain features is sufficiently different
                % add the variant into the set
                sv_idx = [sv_idx k];
                
                % remove old variant if no longer similar
                remove_idx = [];
                for l = 1:numel(sv_idx)
                    simm = simmx(sumdata(sv_idx(l),:)', mean(sumdata(sv_idx, :), 1)');
                    if (simm < coarse_thresh)
                        remove_idx = [remove_idx sv_idx(l)];
                    end
                end
                sv_idx = setdiff(sv_idx, remove_idx);
            end
        else
            c(k) = -1;
        end
    else
        c(k) = -1;
    end
end
model.sv_idx = sv_idx;