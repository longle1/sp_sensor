% Test svm2json
%
% Long Le
% longle1@illinois.edu
% University of Illinois
%

addpath(genpath('../jsonlab/'));
addpath(genpath('../node-paper/'));

%%
%load('\\UYEN-HP\Public\Log\classLoss_2015-10-02-11-29-50-757_1.mat'); % svmModel, classLoss
load('..\node-paper\localLogs\classLoss_2015-11-06-20-30-01-167_1.mat');

classList = [2 7 11];
%classList = [2 4 5 7 9 11];
for k = 1:numel(classList)
    if strcmp(svmModel{k}.ScoreTransform,'none')
        svmModel{k} = fitPosterior(svmModel{k}, svmModel{k}.X, svmModel{k}.Y);
    end
    json = svm2json(svmModel{k});
    fid = fopen(sprintf('../swarmbox/sas/libs/data/featModels/%s_init.json', labId2sound(classList(k))), 'w');
    fprintf(fid, '%s', json);
    fclose(fid);
end