function labIdx = encodeLabel(labelString)

if strcmp(labelString,'air_conditioner')
    labIdx = 1;
elseif strcmp(labelString,'car_horn')
    labIdx = 2;
elseif strcmp(labelString,'children_playing')
    labIdx = 3;
elseif strcmp(labelString,'dog_bark')
    labIdx = 4;
elseif strcmp(labelString,'drilling')
    labIdx = 5;
elseif strcmp(labelString,'engine_idling')
    labIdx = 6;
elseif strcmp(labelString,'gun_shot')
    labIdx = 7;
elseif strcmp(labelString,'jackhammer')
    labIdx = 8;
elseif strcmp(labelString,'siren')
    labIdx = 9;
elseif strcmp(labelString,'street_music')
    labIdx = 10;
elseif strcmp(labelString,'knock')
    labIdx = 11;
elseif strcmp(labelString,'door_slam')
    labIdx = 12;
elseif strcmp(labelString,'steps')
    labIdx = 13;
elseif strcmp(labelString,'chair_moving')
    labIdx = 14;
elseif strcmp(labelString,'cup_jingle')
    labIdx = 15;
elseif strcmp(labelString,'paper_wrapping')
    labIdx = 16;
elseif strcmp(labelString,'key_jingle')
    labIdx = 17;
elseif strcmp(labelString,'keyboard_type')
    labIdx = 18;
elseif strcmp(labelString,'phone_ring')
    labIdx = 19;
elseif strcmp(labelString,'applause')
    labIdx = 20;
elseif strcmp(labelString,'cough')
    labIdx = 21;
elseif strcmp(labelString,'laugh')
    labIdx = 22;
elseif strcmp(labelString,'speech')
    labIdx = 23;
elseif strcmp(labelString,'whistle')
    labIdx = 24;
elseif strcmp(labelString,'finger_snap')
    labIdx = 25;
else
	labIdx = -1;
end
