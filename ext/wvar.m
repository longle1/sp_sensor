function y = wvar(x, w)
% weighted variance
% x - D x N
% w - 1 x N
% y - D x 1

if isempty(x)
    y = [];
end

w = w/norm(w, 1);
mx = mean(x, 2);
y = ((x-repmat(mx, 1, size(x, 2))).^2)*w';