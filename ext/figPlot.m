% Plot figures
%
% Long Le
% University of Illinois
% 

close all; %clear all; 

colMap = 'rbgcmywk';
markMap = '^v<>do+*';
alpha = [0.05 0.1 0.2 0.4 0.6 0.8 1];
load('label', 'pi1');
num_method = 5;
DEBUG = false;

%% Compare method performance without prior
figure('unit', 'normalized', 'position', [0 0 1 1]);
ord = zeros(1, num_method);
for l = 1:10 % all target_class
    subplot(2,5,l); hold on;
    for k = 1:num_method % all methods
        load(sprintf('result/method_%d_target_%d', k, l), 'recall2', 'prec2');
        f1 = 2*recall2.*prec2./(recall2+prec2);
        ord(k) = plot(alpha(3:end), nanmean(f1(3:end,:), 2), [colMap(k) '-' markMap(k)]);
        ax = gca; ax.XTick = alpha; ax.XTickLabel = {'', '', '0.2', '0.4', '0.6', '0.8', '1'}; grid on;
        xlabel('Normalized data usage'); ylabel('F_1 score')
        target_name = decodeLabel(l); target_name(target_name == '_') = ' ';
        title(sprintf('%s, p_1 = %.3f', target_name, pi1(l)));
    end
end
lh = legend(ord([1 4 5 3 2]),'SVM-\Delta^2 Guided', 'DA-\Delta^2 Guided', 'SVM-\Delta_p Guided', 'Trigger', 'Random', 'location', 'southeast');
pos = get(lh, 'position'); 
set(lh, 'position',[0.01 0.5 pos(3:4)])

%% Run the cascade solver
for l = 1:10
    recallSpace = [];
    falarmSpace = [];
    for k = 1:num_method
        load(sprintf('result/method_%d_target_%d', k, l), 'recall1', 'falarm1', 'recall2a', 'falarm2a');
        recallSpace = [recallSpace;mean(recall1,2) mean(recall2a,2)];
        falarmSpace = [falarmSpace;mean(falarm1,2) mean(falarm2a,2)];
    end
    R = zeros(numel(alpha),1);
    re = zeros(numel(alpha),2);
    fa = zeros(numel(alpha),2);
    for k = 1:numel(alpha)
        [re(k,:), fa(k,:), R(k)] = cascadeSolve([1-alpha(k) alpha(k)], pi1(l), recallSpace, falarmSpace);
    end

    if DEBUG
        pr = re(:,1)*pi1(l)./(re(:,1)*pi1(l) + fa(:,1)*(1-pi1(l)));
        rh = (1-re(:,1))*pi1(l)./((1-re(:,1))*pi1(l) + (1-fa(:,1))*(1-pi1(l)));
        abs(alpha'.*pr+(1-alpha').*rh - pi1(l))
        alpha'.*((1-re(:,2)).*pr + fa(:,2).*(1-pr)) + (1-alpha').*rh
    end
    
    save(sprintf('cascade/target_%d', l), 're', 'fa', 'R');
end
%% Compare method performance using prior/Bayes risk
figure('unit', 'normalized', 'position', [0 0 1 1]);
ord = zeros(1, num_method+1);
%for l = 1:10 % all target_class
for l = [2 3 7] % all target_class
    %subplot(2,5,l); hold on;
    subplot(1,3,find(l==[2 3 7])); hold on;
    
    % Empirical performance
    for k = 1:3%1:num_method % all methods
        load(sprintf('result/method_%d_target_%d', k, l), 'recall2', 'falarm2');
        R2 = (1-recall2)*pi1(l) + falarm2*(1-pi1(l));
        ord(k) = plot(alpha(3:end), nanmean(R2(3:end,:), 2), [colMap(k) '-' markMap(k)]);
    end
    if DEBUG
        % Naive-parameter performance
        load(sprintf('model/target_%d', l), 'recall', 'falarm');
        r2 = repmat(recall(2,:), numel(alpha), 1);
        f2 = repmat(falarm(2,:), numel(alpha), 1);
        a = repmat(alpha',1,size(recall,2));
        for k = 1:num_method
            load(sprintf('result/method_%d_target_%d', k, l), 'prec1', 'rho1');
            R = a.*((1-r2).*prec1 + f2.*((1-prec1))) + (1-a).*rho1;
            plot(alpha(3:end), nanmean(R(3:end,:), 2), [colMap(k) ':' markMap(k)])
        end
        % Exact-parameter performance
        a = repmat(alpha',1,size(recall,2));
        for k = 1:num_method
            load(sprintf('result/method_%d_target_%d', k, l), 'prec1', 'rho1', 'recall2a', 'falarm2a');
            R = a.*((1-recall2a).*prec1 + falarm2a.*((1-prec1))) + (1-a).*rho1;
            plot(alpha(3:end), nanmean(R(3:end,:), 2), [colMap(k) '-.' markMap(k)])
        end
    else
        % Optimal performance
        load(sprintf('cascade/target_%d', l), 'R');
        ord(end) = plot(alpha(3:end), R(3:end), [colMap(end) '-' markMap(end)]);
    end
    
    ax = gca; ax.XTick = alpha(3:end); ax.XTickLabel = {'0.2', '0.4', '0.6', '0.8', '1'}; grid on;
    xlabel('Normalized data usage'); ylabel('Bayes risk')
    target_name = decodeLabel(l); target_name(target_name == '_') = ' ';
    title(sprintf('%s, \\pi_1 = %.3f', target_name, pi1(l)));
end
if DEBUG
    
else
    %lh = legend(ord([1 4 5 3 2 6]), 'SVM-\Delta^2 Guided', 'DA-\Delta^2 Guided', 'SVM-\Delta_p Guided', 'Energy-based', 'Random', 'Lower Bound','location', 'southeast');
    lh = legend(ord([1 3 2 6]), 'Bayesian-based ranking', 'Energy-based', 'Random', 'Lower Bound','location', 'southeast');
end
pos = get(lh, 'position'); 
set(lh, 'position',[0.01 0.5 pos(3:4)])

%% Model performance
all_f1 = zeros(3, 10);
all_auroc = zeros(3, 10);
nsv = zeros(2,10); % number of support vectors for SVM models
for k = 1:10
    load(sprintf('model/target_%d', k), 'recall', 'prec', 'auroc', 'svm_model');
    f1 = 2*recall.*prec./(recall+prec);
    all_f1(:, k) = nanmean(f1, 2);
    all_auroc(:, k) = mean(auroc, 2);
    nsv(:,k) = fix(mean(cellfun(@(c) numel(c.Alpha), svm_model(1:2,:)),2));
end

%% Plot TF feature
%k = 200;
%[ridgetracker, spec, speclog_prob, specbacktrackcumsum] = ridgeDet2(classData(k).data, classData(k).srate, -5.0, frameLen);

[data, srate] = audioread('GCW-A-(1).wav');
%[data, srate] = audioread('1.wav');
[ridgetracker, spec, speclog_prob, specbacktrackcumsum] = ridgeDet2(data, srate, -5.0, frameLen);

mTF = zeros(5,2);
sTF = zeros(5,2);
for l = 1:numel(ridgetracker.B)
    t = ridgetracker.B{l}(:,2); %t = t-min(t); % convert to duration
    f = ridgetracker.B{l}(:,1);
    mTF(l,:) = [mean(f) mean(t)];
    sTF(l,:) = [std(f) std(t)];
end

figure; hold on;
imagesc(-speclog_prob); axis tight; axis xy
for l = 1:numel(ridgetracker.B)
    plot(ridgetracker.B{l}(:,2), ridgetracker.B{l}(:,1), 'k', 'linewidth', 2)
    plotEclipse(mTF(l,2), mTF(l,1), sTF(l, 2), sTF(l, 1));
end
xlabel('Time index'); ylabel('Frequency index');

%% Verify the condition
all_Rdiff = zeros(numel(alpha), 10);
for l = 1:10
    load(sprintf('result/method_%d_target_%d', 1, l), 'prec1', 'rho1', 'recall2a', 'falarm2a');
    a = repmat(alpha',1,size(recall2a,2));
    Rdiff = a.*((recall2a+falarm2a-1).*(prec1-pi1(l))) + (1-a).*(pi1(l)-rho1);
    all_Rdiff(:, l) = mean(Rdiff, 2);
end

%% Plot rho1, prec1
figure('unit', 'normalized', 'position', [0 0 1 1]);
for l = 1:10
    subplot(2,5,l); hold on;
    for k = 1:num_method % all methods
        load(sprintf('result/method_%d_target_%d', k, l), 'rho1');
        % alpha = 1 is random, do not plot
        plot(alpha(3:end-1), nanmean(rho1(3:end-1,:), 2), [colMap(k) '-' markMap(k)]);
        ax = gca; ax.XTick = alpha; ax.XTickLabel = {'', '', '0.2', '0.4', '0.6', '0.8', '1'}; grid on;
        xlabel('Normalized data usage'); ylabel('rho1')
        target_name = decodeLabel(l); target_name(target_name == '_') = ' ';
        title(sprintf('%s, p_1 = %.3f', target_name, pi1(l)));
    end
end

figure('unit', 'normalized', 'position', [0 0 1 1]);
for l = 1:10
    subplot(2,5,l); hold on;
    for k = 1:num_method % all methods
        load(sprintf('result/method_%d_target_%d', k, l), 'prec1');
        plot(alpha(3:end), nanmean(prec1(3:end,:), 2), [colMap(k) '-' markMap(k)]);
        ax = gca; ax.XTick = alpha; ax.XTickLabel = {'', '', '0.2', '0.4', '0.6', '0.8', '1'}; grid on;
        xlabel('Normalized data usage'); ylabel('prec1')
        target_name = decodeLabel(l); target_name(target_name == '_') = ' ';
        title(sprintf('%s, p_1 = %.3f', target_name, pi1(l)));
    end
end