function labId = sound2labId(str)

switch str
    case 'air_conditioner'
        labId = 1;
    case 'car_horn'
        labId = 2;
    case 'children_playing'
        labId = 3;
    case 'dog_bark'
        labId = 4;
    case 'drilling'
        labId = 5;
    case 'engine_idling'
        labId = 6;
    case 'gun_shot'
        labId = 7;
    case 'jackhammer'
        labId = 8;
    case 'siren'
        labId = 9;
    case 'street_music'
        labId = 10;
    case 'speech'
        labId = 11;
    case 'GCWA'
        labId = 12;
    otherwise
        disp('unrecognize class');
        labId = -1;
end
