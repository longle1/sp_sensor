% labelObjectsScript

startnum = 1;
fix_mode = 0;
imdir = '.';
ext = '.bmp';
outdir = '.';
outext = '_objects.mat';

files = dir(fullfile(imdir, ['*' ext]));
fn = {files.name};

if ~exist(outdir, 'file')
    disp(['creating ' outdir])
    try 
        mkdir(outdir);
    catch
        disp(['could not create ' outdir]);
    end
end

for k = startnum:numel(fn)

    im = im2double(imread(fullfile(imdir, fn{k})));
    outname = fullfile(outdir, [strtok(fn{k}, '.') outext]);
    
    % if objects already exist, only edit if in fix_mode
    if exist(outname, 'file')
        if fix_mode
            load(outname);
            disp(' ');
            disp([num2str(k) ': ' fn{k}]);
            objects = objectLabelingTool(im, objects);
            save(outname, 'objects');
        end
    else
        disp(' ');
        disp([num2str(k) ': ' fn{k}]);
        objects = objectLabelingTool(im); 
        save(outname, 'objects');
    end
end