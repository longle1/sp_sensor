function out = stdCentOfMass(w,x)
% Compute the center of mass
%
% Long Le
% University of Illinois
% longle1@illinois.edu

CoM = centOfMass(w,x);
out = sqrt(mean((x - CoM).^2));
%nw = w./sum(w);
%out = sqrt(mean(nw.*(x - CoM).^2));