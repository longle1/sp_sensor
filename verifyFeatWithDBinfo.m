function err = verifyFeatWithDBinfo(aEvent)
% compute summary features from debug info
if ~isfield(aEvent,'FI')
    error('skipped due to missing debug info')
end

fs = aEvent.fs;
fftSize = aEvent.fftSize;

%M = aEvent.M;
F = (aEvent.FI+1)*fs/fftSize; % must +1 since range(FI) = [0,nFc-1]
T = aEvent.TI*(fftSize/2)/fs;

% generate feat
feat = ridgeGroupStat(F,T,nan,nan,fs,fftSize)';

% disp the difference between the service feature extraction and the ground truth
if norm(size(feat)-size(aEvent.feat)) == 0
    err = norm(feat-aEvent.feat);
    fprintf(1, 'num err: %f\n', err);
else
    error('inconsistent size between aEvent.feat and feat')
end