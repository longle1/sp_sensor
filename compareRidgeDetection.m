function compareRidgeDetection()
% Compare DP ridge tracking with Dan Ellis' implementation of MQ model
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%

addpath(genpath('./sinemodel/'))

addpath(genpath('../jsonlab/'))
addpath(genpath('../V1_1_urlread2'));
addpath(genpath('../sas-clientLib/src'))

servAddr = 'acoustic.ifp.illinois.edu';
DB = 'sgnGoldDb';
USER = 'nan';
PWD = 'sgnGoldPwd';
DATA = 'data2';
EVENT = 'event2';

%% query a remote service
q.t1 = datenum(2015,12,03,00,00,00); q.t2 = datenum(2015,12,05,00,00,00);
%q.loc(1) = 40.1069855; q.loc(2) = -88.2244681; q.rad = 1000; % US
q.loc(1) = 10.1069855; q.loc(2) = 98.2244681; q.rad = 1000; % SEA
events = IllQuery(servAddr, DB, USER, PWD, EVENT, q);

% sorted events according to recordDate
usTt = zeros(1,0);
for k = 1:numel(events)
    usTt(k) = datenum8601(events{k}.recordDate);
end
[tt, idx] = sort(usTt);
sEvents = events(idx);

% read raw data
audDat = cell(numel(sEvents),1);
for k = 1:10%numel(sEvents)
    fprintf(1,'%d: %s\n',k,sEvents{k}.filename);
    
    data = IllGridGet(servAddr, DB, USER, PWD, DATA, sEvents{k}.filename);
    if ~strcmp(data(1:4),'RIFF')
        fprintf(1,'missing data\n');
        continue;
    end
    [y, header] = wavread_char(data);
    audDat{k} = y';
end
audDatMat = cell2mat(audDat);

% Run through two ridge detector
fs = 16e3;
blockSize = 256;
[S,tt,ff] = mSpectrogram(audDatMat,fs,blockSize);
figure; imagesc(tt,ff,log(S)); axis xy

[newState, specDet] = detectOneFile(S,[],true);
[R,M] = extractrax(S);