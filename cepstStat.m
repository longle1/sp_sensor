function feat = cepstStat(cepstAll)
% Summary statistics for cepstrum
% 
% Long Le
% longle1@illinois.edu
%

nPart = 3;
% pad to ensure no nan
if size(cepstAll,2) < nPart
   cepstAll = padarray(cepstAll,[0,nPart-size(cepstAll,2)],'replicate','post');
end
cepstAllLen = size(cepstAll,2);
stepsize = floor(cepstAllLen/nPart);

cnt = 0;
feat = zeros(nPart,16);
for k = 1:nPart-1
    cepst = cepstAll(:,cnt+1:cnt+stepsize);
    feat(k,:) = mean(cepst,2);
    
    cnt = cnt+stepsize;
end
cepst = cepstAll(:,cnt+1:cepstAllLen);
feat(nPart,:) = mean(cepst,2);

feat = feat(:)'; % combine all partitions in a file