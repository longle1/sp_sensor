function out = centOfMass(w,x)
% Compute the center of mass
%
% Long Le
% University of Illinois
% longle1@illinois.edu

nw = w./sum(w);
out = sum(nw.*x);