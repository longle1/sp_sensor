function colStr = idx2col(idx)
% index to color string
%
% Long Le
% longle1@illinois.edu
%

idx = mod(idx-1,6)+1;

switch idx
    case 1
        colStr = 'b';
    case 2
        colStr = 'g';
    case 3
        colStr = 'r';
    case 4
        colStr = 'c';
    case 5
        colStr = 'm';
    case 6
        colStr = 'y';
    %{
    case 7
        colStr = 'b--';
    case 8
        colStr = 'g--';
    case 9
        colStr = 'r--';
    case 10
        colStr = 'c--';
    case 11
        colStr = 'm--';
    case 12
        colStr = 'y--';
    case 13
        colStr = 'b-.';
    case 14
        colStr = 'g-.';
    case 15
        colStr = 'r-.';
    case 16
        colStr = 'c-.';
    case 17
        colStr = 'm-.';
    case 18
        colStr = 'y-.';
    %}
    otherwise
        error('invalid idx')
end
