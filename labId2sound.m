function str = labId2sound(labId)

switch labId
    case 1
        str = 'air_conditioner';
    case 2
        str = 'car_horn';
    case 3
        str = 'children_playing';
    case 4
        str = 'dog_bark';
    case 5
        str = 'drilling';
    case 6
        str = 'engine_idling';
    case 7
        str = 'gun_shot';
    case 8
        str = 'jackhammer';
    case 9
        str = 'siren';
    case 10
        str = 'street_music';
    case 11
        str = 'speech';
    case 12
        str = 'GCWA';
    otherwise
        disp('unrecognize class');
        str = 'unknown';
end