function [F,T] = ridgeIdx2TFRidge(R, fs,fftSize,tt)
% A utility subroutine to convert the API from ridge index to 
% time-frequency ridges
%
% Long Le
% longle1@illinois.edu
%

F = R*fs/fftSize; % freq idx 1 is epsilon > 0 Hz
T = repmat(((1:size(R,2)))*fftSize/2/fs, size(R,1), 1);
for k = 1:size(T,1)
    T(k,isnan(F(k,:))) = nan;
end
