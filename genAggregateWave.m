function genAggregateWave()
% various scripts collected into one file
%
% Long Le <longle1@illinois.edu>
% 

%% View data from service
data = IllDownGrid(DB, USER, PWD, 'data', '20151002140153945.wav');
[y{1}, header] = wavread_char(data);
data = IllDownGrid(DB, USER, PWD, 'data', '20151002140201434.wav');
[y{2}, header] = wavread_char(data);
data = IllDownGrid(DB, USER, PWD, 'data', '20151002140210430.wav');
[y{3}, header] = wavread_char(data);
data = IllDownGrid(DB, USER, PWD, 'data', '20151002140224259.wav');
[y{4}, header] = wavread_char(data);

% time domain
figure;
for k = 1:numel(y)
    subplot(numel(y),1,k); plot(y{k})
end

% freq domain
fs = double(header.sampleRate);
fftSize = 256;
figure;
S = cell(numel(y),1);
for k = 1:numel(y)
    subplot(numel(y),1,k); 
    [S{k},tt,ff] = mSpectrogram(y{k},fs,fftSize);
    imagesc(tt, ff, log(abs(S{k}))); axis xy
end

%% Generate aggregate dataset
rootDir = 'F:/UrbanSoundAll/SelectedUrbanSound/data';
aggregateWave([rootDir '/car_horn'], 5, '.');
aggregateWave([rootDir '/dog_bark'], 5, '.');
aggregateWave([rootDir '/gun_shot'], 5, '.');
aggregateWave([rootDir '/siren'], 5, '.');
