% Train model using service debug data
%
% Long Le <longle1@illinois.edu>
%

clear all; close all;

addpath(genpath('../voicebox/'))
addpath(genpath('../jsonlab/'))
addpath(genpath('../V1_1_urlread2'));
addpath(genpath('../sas-clientLib/src'))

%servAddr = '128.32.33.227';
servAddr = 'acoustic.ifp.illinois.edu';
DB = 'publicDb';
USER = 'nan';
PWD = 'publicPwd';
DATA = 'data';
EVENT = 'event';

%% query a remote service
%testFilename = '20151005171519674.wav';
%events = IllColGet(DB, USER, PWD, 'event', testFilename);
q.t1 = datenum(2015,12,01,00,00,00); q.t2 = datenum(2016,01,20,00,00,00);
%q.loc(1) = 40.1069855; q.loc(2) = -88.2244681; q.rad = 1000; % US
%q.loc(1) = 10.1069855; q.loc(2) = 98.2244681; q.rad = 1000; % SEA
events = IllQuery(servAddr, DB, USER, PWD, EVENT, q);
% due to long download time, just back up a local copy
%save('.\localLogs\events.mat', 'events')
interactiveTag(events,1,servAddr, DB, USER, PWD, DATA, EVENT);

%% extract feature and label
lab = -ones(numel(events),1);
featTFRidge = zeros(numel(events),16);
featMFCC = zeros(numel(events),16);
%dat = cell(numel(events),1);
for l = 1:numel(events)
    disp(l);

    % get feat
    if ~isfield(events{l},'TFRidgeFeat')
        disp('skipped due to missing TFRidgeFeat');
        continue;
    else
        featTFRidge(l,:) = events{l}.TFRidgeFeat;
    end
    % get label
    if ~isfield(events{l},'tag')
        disp('skipped due to missing label');
        continue;
    else
        lab(l,1) = sound2labId(events{l}.tag);
    end
    % get data
    data = IllGridGet(servAddr, DB, USER, PWD, 'data', events{l}.filename);
    try
        [y, header] = wavread_char(data);
        %dat{l,1} = y;
        featMFCC(l,:)=mean(melcepst(y,double(header.sampleRate),'Mtaz',16),1);
    catch e
        disp('missing binary data')
        continue;
    end
end

%% Train models
featMode = 1;
if featMode == 1
    feat = featTFRidge;
elseif featMode == 2
    feat = featMFCC;
end

% Train SVMs to detect a target class
rng('default')
labSet = unique(lab);
svmModel = cell(numel(labSet),1);
CVSVMModel = cell(numel(labSet),1);
classLoss = zeros(numel(labSet),1);
for k = 1:numel(labSet)
    % treat nans as missing values and skip
    binLabMat = lab == labSet(k);
    % simply put the data and the model together into a structure
    svmModel{k} = fitcsvm(feat,binLabMat,'KernelFunction','rbf','KernelScale','auto','Standardize',true);
    % Model validation
    CVSVMModel{k} = crossval(svmModel{k},'Kfold',5);
    % simply return the loss
    classLoss(k) = kfoldLoss(CVSVMModel{k});
end

currDateStr = datestr(now,'yyyy-mm-dd-HH-MM-SS-FFF');
%save(['//UYEN-HP/Public/Log/' sprintf('classLoss_%s_%d.mat',currDateStr,method)],'classLoss','svmModel','dataSize');
save(['./localLogs/' sprintf('classLoss_%s_%d.mat',currDateStr,1)],'svmModel','classLoss','featMode','feat','lab');