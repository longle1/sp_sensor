function disRate = compareBatteryLife()
%
% Compare battery life of various processing strategy
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%

load('C:\Users\Long\Projects\node-paper\localLogs\event.mat','events')
[tt,batteryLevel] = dispBattery(events);
ntt = (tt-tt(1))*24;
disRate(1) = mean(diff(batteryLevel))/mean(diff(ntt));

load('C:\Users\Long\Projects\node-paper\localLogs\event2.mat','events')
[tt2,batteryLevel2] = dispBattery(events);
ntt2 = (tt2-tt2(1))*24;
disRate(2) = mean(diff(batteryLevel2))/mean(diff(ntt2));

load('C:\Users\Long\Projects\node-paper\localLogs\event_20151203.mat','events')
[tt3,batteryLevel3] = dispBattery(events);
ntt3 = (tt3-tt3(1))*24;
disRate(3) = mean(diff(batteryLevel3))/mean(diff(ntt3));

figure; hold on; plot(ntt-1.03,batteryLevel); plot(ntt2,batteryLevel2); plot(ntt3-0.43,batteryLevel3)
legend('low threshold','recording','high threshold')