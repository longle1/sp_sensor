function [tt,selEvents,batteryLevel,audDat,spec] = analyzeEvent(events,DATA,EVENT)
% function [tt,selEvents,batteryLevel,audDat,spec] = analyzeEvent(events,DATA,EVENT)
%
% Analyze events from EVENT whose raw data are in DATA
%
% Long Le <longle1@illinois.edu>
% University of Illinois

addpath(genpath('../voicebox/'))
addpath(genpath('../jsonlab/'))
addpath(genpath('../V1_1_urlread2'));
addpath(genpath('../sas-clientLib/src'))

%servAddr = '128.32.33.227';
servAddr = 'acoustic.ifp.illinois.edu';
DB = 'publicDb';
USER = 'nan';
PWD = 'publicPwd';
if ~exist('DATA','var')
    DATA = 'data2';
end
if ~exist('EVENT','var')
   EVENT = 'event2';
end

%{
q.t1 = datenum(2015,11,29,00,00,00); q.t2 = datenum(2015,12,03,00,00,00);
%q.loc(1) = 40.1069855; q.loc(2) = -88.2244681; q.rad = 1000; % US
q.loc(1) = 10.1069855; q.loc(2) = 98.2244681; q.rad = 1000; % SEA
events = IllQuery(servAddr, DB, USER, PWD, EVENT, q);
%}

%% sort events
usTt = zeros(1,0); % unsorted time
for k = 1:numel(events)
    usTt(k) = datenum8601(events{k}.recordDate);
end
[tt, idx] = sort(usTt);
selEvents = events(idx);

%% analyze events
%noiseSpec = zeros(16,0);
batteryLevel = zeros(1,numel(tt));
audDat = cell(numel(tt));
spec = cell(numel(tt));

fftSize = 256;
for k = 7*120:numel(tt)
    disp(k);
    
    %noiseSpec(:,cnt) = selEvents{k}.noiseFloor';
    batteryLevel(k) = selEvents{k}.batteryLevel';

    data = IllGridGet(servAddr, DB, USER, PWD, DATA, selEvents{k}.filename);
    if ~strcmp(data(1:4),'RIFF')
        fprintf(1,'missing data\n');
        
        %{
        fprintf(1,'removing event\n');
        resp = IllColDelete(servAddr, DB, USER, PWD, EVENT, selEvents{k}.filename);
        jsonResp = loadjson(resp);
        if (isfield(jsonResp,'ok'))
            fprintf(1,'event with missing data removed\n');
        else
            fprintf(1,'unable to remove event with missing data\n');
        end
        %}
        
        continue;
    end
    [y, header] = wavread_char(data);
    fs = double(header.sampleRate);
    audDat{k} = y;
    sound(y, fs)

    [S,usTt,ff] = mSpectrogram(y,fs,fftSize);
    spec{k} = S;
    
    figure('units','normalized','outerposition',[0 0 1 1]);
    subplot(211); plot(y); axis tight;
    subplot(212); imagesc(usTt,ff,log(S)); axis xy;
    suptitle(sprintf('Time elapsed: %.2f hours, Battery level: %d %%',(tt(k)-tt(1))*24, batteryLevel(k)));
    
    result = input('Continue? y/n: ','s');
    if result == 'n'
        break;
    end
    close;
end

%figure; imagesc(sortTt, [1:16],sortNoiseSpec); axis xy;
%{
figure; plot((tt-tt(1))*24, sortBatteryLevel);
xlabel('Time (hours)'); ylabel('Battery level')
fprintf(1,'Time elapsed: %.2f hours\n', (tt(end)-tt(1))*24);
%}