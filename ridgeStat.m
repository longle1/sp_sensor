function [feat,selId,bdry] = ridgeStat(F,T,M,fs,fftSize)
% Summary statistics for ridges
% 
% Long Le
% longle1@illinois.edu
%

% Select top ridges
numValidBins = zeros(size(F,1),1);
for k = 1:size(F,1)
    currF = F(k,:);
    numValidBins(k) = sum(~isnan(currF));
end
[~, idx] = sort(numValidBins,'descend');
selId = idx(1:min(32,size(F,1)));

% calculate statistics for each selected ridge
nMelBank = 16;
melFilt = full(melbankm(nMelBank,fftSize,fs,0,0.5,'y')); % avoid zeroing out dc component
% figure; plot((0:size(melFilt,2)-1)/fftSize*fs,melFilt(6,:));
featAll = zeros(nMelBank,3,numel(selId));

W = zeros(1,1,numel(selId));
bdry = zeros(numel(selId),2);
for k = 1:numel(selId)
    % get a subset of F and T
    currF = F(selId(k),:); 
    validCurrF = currF(~isnan(currF));
    currT = T(selId(k),:);
    validCurrT = currT(~isnan(currT));
    currM = M(selId(k),:);
    validCurrM = currM(~isnan(currM));
    
    % compute statistics for valid values
    for l = 1:nMelBank
        featAll(l,1,k) = mean(melFilt(l,round(validCurrF/fs*fftSize)+1).*validCurrM(:)');
        featAll(l,2,k) = std((melFilt(l,round(validCurrF/fs*fftSize)+1)~=0).*(validCurrT(:)-min(validCurrT(:)))');
        featAll(l,3,k) = std((melFilt(l,round(validCurrF/fs*fftSize)+1)~=0).*validCurrF(:)');
    end
    W(k) = numel(validCurrM(:));
    %featAll(:,1,k) = dct(featAll(:,1,k));
    
    % find detection interval
    bdry(k,:) = [min(validCurrT) max(validCurrT)];
end
feat = squeeze(sum(bsxfun(@times,W/sum(W(:)),featAll),3)); % aggregate over all ridge groups

feat = feat(:)'; % combine all ridges in a file