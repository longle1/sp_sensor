function newEvents = interactiveTag(events, startIdx, servAddr, DB, USER, PWD, DATA, EVENT)
% Interactive tagging
% newEvents = interactiveTag(events, startIdx, servAddr, DB, USER, PWD, DATA, EVENT)
%
% Long Le <longle1@illinois.edu>
% University of Illinois
% 

fs = 16e3;
blockSize = 256;

for l = startIdx:numel(events)
    fprintf(1,'%d: %s\n',l,events{l}.filename);
    
    % download the binary data
    data = IllGridGet(servAddr, DB, USER, PWD, DATA, events{l}.filename);
    try
        [y, header] = wavread_char(data);
    catch e
        disp('missing binary data')
        %resp = IllColDelete(servAddr, DB, USER, PWD, EVENT, events{l}.filename);
        %disp(resp);
        continue;
    end
    [S,tt,ff] = mSpectrogram(y,fs,blockSize);
    
    % display data 
    figure; hold on
    imagesc(tt, ff, log(abs(S)));
    % and debug info if available
    colormap(1-gray)
    if isfield(events{l},'TI') && isfield(events{l},'FI')
        plot(events{l}.TI*(blockSize/2)/fs,events{l}.FI/blockSize*fs,'r.');
    end
    axis xy; axis tight
    %title(sprintf('maxDur: %.3f\n',events{l}.maxDur))
    
    % play sound
    sound(y, double(header.sampleRate));

    % ask for a new tag
    if isfield(events{l}, 'tag')
        currTag = events{l}.tag;
    else
        currTag = '';
    end
    newTag = input(['current tag: "' currTag '", enter new tag: ' ],'s');

    if strcmp(newTag, currTag) == 0
        % upload the new tag to the service
        resp = IllColPut(servAddr,DB,USER,PWD,EVENT, events{l}.filename, 'set', ['{"tag":"' newTag '"}']);
        disp(resp);
        % update local tag
        jsonResp = loadjson(resp);
        if isfield(jsonResp,'ok') && jsonResp.ok == 1
            events{l}.tag = newTag;
        end
    end
    
    % close the figure to avoid overflow
    close;
end

newEvents = events;
