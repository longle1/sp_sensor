function [S,tt,ff] = mSpectrogram(y,fs,blockSize, inc)
% My spectrogram
% 
% Long Le
% longle1@illinois.edu
%

if ~exist('inc','var')
    inc = blockSize/2; 
end
Y = enframe(y,hanning(blockSize),inc)'; % Hanning window is better at rejecting interferences
S = zeros(blockSize/2+1,size(Y,2));
for k = 1:size(Y,2)
    allF = fft(Y(:,k),blockSize);
    f = allF(1:blockSize/2+1);
    S(:,k) = abs(f);
end
tt = (1:size(S,2))*inc/fs;   % default specgram step is NFFT/2 i.e. 128
ff = (0:size(S,1)-1)/blockSize*fs;