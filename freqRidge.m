function freqRidge()
% Test online ridge detector

clear all; close all
addpath('../voicebox/');

%% Parameters
outext = '_objects.mat';
rng('default')
SNR = [1000 100 10]';

datSrc = 1; sprintf('datSrc: %d', datSrc)
if (datSrc == 1)
    datFolder = '../sm-paper/genCascade/data/GCW/';
    file = dir([datFolder '*.wav']);
    filename = cell(numel(file),1);
    for k = 1:numel(file)
        filename{k} = [datFolder file(k).name];
    end
    outdir = 'label/GCW/';
    ROCfile = 'GCW.mat';
    thresh{1} = [-1 -2 -3 -4 -5];
    thresh{2} = [-0.2 -0.3 -0.4 -0.6 -0.8 -1];
    thresh{3} = [0];
elseif (datSrc == 2)
    datFolder = '../acousticsearch/data/whistlecommand/';
    file = dir([datFolder '*.wav']);
    filename = cell(numel(file),1);
    for k = 1:numel(file)
        filename{k} = [datFolder file(k).name];
    end
    outdir = 'label/whistlecommand/';
    ROCfile = 'whistlecommand.mat';
    thresh{1} = [-1 -2 -3 -4 -5];
    thresh{2} = [-0.2 -0.3 -0.4 -0.6 -0.8 -1];
    thresh{3} = [0];
elseif (datSrc == 3)
    datFolder = '../matlab-realtime-audioprocessing/TIDIGIT_adults_crop_crop/';
    file = dir([datFolder '*.wav']);
    filename = cell(numel(file),1);
    for k = 1:numel(file)
        filename{k} = [datFolder file(k).name];
    end
    outdir = 'label/TIDIGIT_adults_crop_crop/';
    ROCfile = 'TIDIGIT_adults_crop_crop.mat';
    thresh{1} = [-1 -2 -3 -4 -5];
    thresh{2} = [-0.1 -0.2 -0.4 -0.6 -0.8 -1];
    thresh{3} = [0];
else
    filename{1} = 'data/one2.wav';
end

method = 1; sprintf('method: %d', method)
if (method == 1)
    ridge_min_time = cell(1, numel(file));
    ridge_max_time = cell(1, numel(file));
    ridge_min_freq = cell(1, numel(file));
    ridge_max_freq = cell(1, numel(file));
    ridge_loglik = cell(1, numel(file));
    ridge_max_inst_bw = cell(1, numel(file));
end
PLOT = false;
for SNRidx = 1%:length(SNR)
disp(SNR(SNRidx))
ROC = [];
for threshIdx = 1:length(thresh{method})
disp(thresh{method}(threshIdx))
accDET = 0;
accFA = 0;
accLabel0 = 0;
accLabel1 = 0;
for expIdx = 1:numel(filename)
    %disp(expIdx)
    
    if (PLOT)
        %figure('units','normalized','outerposition',[0 0 1 1]);
        figure;
    end
    
    try
        [y, fs] = audioread(filename{expIdx});
    catch MException
        [y, fs] = readsph(filename{expIdx});
    end
    y = y(:,1);
    if (fs > 20000)
        h = fir1(8, 20000/fs); % low pass filter
        y = filter(h, 1, y);
        y = resample(y, 20000, fs);
        fs = 20000;
    end
    x = y + randn(size(y))*std(y)/sqrt(SNR(SNRidx));
    
    Nblk = 128;
    Nfc = 128;
    w = hann(Nblk*2)';

    if (method == 1)
        %% Ridge tracking 
        % Parameters
        noisetracker.alpha_updown = 0.01;
        noisetracker.slow_scale = 0.1;
        noisetracker.indicator_countdown = 10;
        noisetracker.floor_up = (1+noisetracker.alpha_updown)*ones(Nfc,1);
        noisetracker.floor_up_slow = (1+noisetracker.slow_scale*noisetracker.alpha_updown)*ones(Nfc,1);
        noisetracker.floor_down = (1-noisetracker.alpha_updown)*ones(Nfc,1);
        noisetracker.floor_thresh = 0.1*ones(Nfc,1);
        noisetracker.indicator_last = zeros(Nfc,1);

        ridgetracker.alpha_ridge = exp(-Nblk/(0.1*fs));
        ridgetracker.f_ridge_down_max = ceil(20000*Nblk*Nfc*2/(fs*fs)); % want X Hz/sec chirp rate; f_ridge_down_max units are freq-samples/blklen-time-samples; 1 freq-sample = fs/binct Hz
        ridgetracker.f_ridge_up_max = ridgetracker.f_ridge_down_max;
        ridgetracker.f_ridge_down = zeros(Nfc,1);
        ridgetracker.f_ridge_up = zeros(Nfc,1);
        for ii=1:Nfc,
           ridgetracker.f_ridge_down(ii) = max(1,ii-ridgetracker.f_ridge_down_max);
           ridgetracker.f_ridge_up(ii) = min(Nfc,ii+ridgetracker.f_ridge_up_max);
        end
        ridgetracker.ridge_offset_penalty_max = 0.05;
        ridgetracker.ridge_offset_penalty = ridgetracker.ridge_offset_penalty_max/max(ridgetracker.f_ridge_up_max,ridgetracker.f_ridge_down_max);
        ridgetracker.log_prob_noise_min_ridge = -6.0;
        ridgetracker.detect_ridge_backtrack_thresh = thresh{1}(threshIdx);
        ridgetracker.backtracklen = ceil(0.02*fs/Nblk); % enter the multiplier as the approximate track length in sec
        % States
        ridgetracker.log_prob_noise_ridge_cum_now = zeros(Nfc,1);
        ridgetracker.specridge_link_back = zeros(Nfc,ridgetracker.backtracklen+1);
        ridgetracker.speclog_prob_noise_ridge = zeros(Nfc,ridgetracker.backtracklen+1);
        ridgetracker.stftdetectridgeback = zeros(Nfc,2);
        ridgetracker.ridge_min_time = zeros(1,0);
        ridgetracker.ridge_max_time = zeros(1,0);
        ridgetracker.ridge_min_freq = zeros(1,0);
        ridgetracker.ridge_max_freq = zeros(1,0);
        ridgetracker.ridge_loglik = zeros(1,0);
        ridgetracker.ridge_max_inst_bw = zeros(1,0);
        ridgetracker.ridge_desc = cell(1,0);
        ridgetracker.ridgect = 0;

        % Main
        data = enframe(x,Nblk*2, Nblk*2);
        spec = zeros(Nfc, size(data,1));
        spec_noise_floor = zeros(Nfc, size(data,1));
        specridge_link_back = zeros(Nfc, size(data,1));
        speclog_prob_noise_ridge = zeros(Nfc, size(data,1));
        specbacktrackcumsum = zeros(Nfc, size(data,1));
        specbacktracktouchedflags = zeros(Nfc, size(data,1));
        stftdetectridgeback = zeros(Nfc, size(data,1));
        for k = 1:size(data,1)
            s = abs(fft(w.*data(k,:)));
            s = s(1:Nfc);
            spec(:,k) = s;

            offset_ridge = zeros(Nfc,1);
            ridge_link_back = zeros(Nfc,1);
            log_prob_noise_ridge = zeros(Nfc,1);
            log_prob_noise_ridge_cum_last = ridgetracker.log_prob_noise_ridge_cum_now;
            for j=1:Nfc
                % noise tracking
                if(s(j) > noisetracker.floor_thresh(j))
                    noisetracker.indicator_last(j) = noisetracker.indicator_last(j)-1;
                    if(noisetracker.indicator_last(j) < 0) % signal is highly probable, slow down
                        noisetracker.floor_thresh(j) = noisetracker.floor_thresh(j)*noisetracker.floor_up_slow(j);
                    else % transient signal is highly probable
                        noisetracker.floor_thresh(j) = noisetracker.floor_thresh(j)*noisetracker.floor_up(j);
                    end
                else % background noise is highly probable
                    noisetracker.indicator_last(j) = noisetracker.indicator_countdown;
                    noisetracker.floor_thresh(j) = noisetracker.floor_thresh(j)*noisetracker.floor_down(j);
                end

                % debugging
                spec_noise_floor(:,k) = noisetracker.floor_thresh;

                %  TIME-VARYING SPECTRAL RIDGE TRACKING
                log_prob_noise_ridge(j) = max(ridgetracker.log_prob_noise_min_ridge,-s(j)^2/noisetracker.floor_thresh(j));
                [~, offset_ridge(j)] = ...
                    min((1 - ridgetracker.ridge_offset_penalty*abs(j - [ridgetracker.f_ridge_down(j):ridgetracker.f_ridge_up(j)]')).*...
                    log_prob_noise_ridge_cum_last(ridgetracker.f_ridge_down(j):ridgetracker.f_ridge_up(j)));

                ridgetracker.log_prob_noise_ridge_cum_now(j) = ridgetracker.alpha_ridge*log_prob_noise_ridge_cum_last(ridgetracker.f_ridge_down(j)+offset_ridge(j)-1) + ...
                      (1-ridgetracker.alpha_ridge)*log_prob_noise_ridge(j);
                ridge_link_back(j) = ridgetracker.f_ridge_down(j)+offset_ridge(j)-1;  % pointers to the previous ridge frequency
            end
            ridgetracker.specridge_link_back = [ridgetracker.specridge_link_back(:,2:end) ridge_link_back];
            ridgetracker.speclog_prob_noise_ridge = [ridgetracker.speclog_prob_noise_ridge(:,2:end) log_prob_noise_ridge];

            % debugging
            speclog_prob_noise_ridge(:,k) = log_prob_noise_ridge;
            specridge_link_back(:,k) = ridge_link_back;

            %
            % Backward track detection
            %
            % track back backtracklen time blocks
            backtrackcumsum = log_prob_noise_ridge;
            backtracktouchedflags = ones(Nfc,1);
            for i5=ridgetracker.backtracklen+1:-1:2,
              backtrackcumsumnew = zeros(Nfc,1);
              backtracktouchedflagsnew = zeros(Nfc,1);
              % Follow pointer back from each frequency to ridge-frequency at previous time and update backward cumsum of detection metric
              for iii=1:Nfc
                 if ( backtracktouchedflags(iii) == 1),
                    if ridgetracker.specridge_link_back(iii,i5) == 0
                        continue;
                    end
                    backtrackcumsumnew(ridgetracker.specridge_link_back(iii,i5)) = ...
                        min(backtrackcumsumnew(ridgetracker.specridge_link_back(iii,i5)),...
                        backtrackcumsum(iii)+ridgetracker.speclog_prob_noise_ridge(ridgetracker.specridge_link_back(iii,i5),i5-1));
                    backtracktouchedflagsnew(ridgetracker.specridge_link_back(iii,i5)) = 1;  % indicate which frequencies were part of a backtracked ridge
                 end
              end
              backtrackcumsum = backtrackcumsumnew;
              backtracktouchedflags = backtracktouchedflagsnew;
            end
            backtrackcumsum = (1.0/(ridgetracker.backtracklen+1))*backtrackcumsum;

            % debugging
            if (k > ridgetracker.backtracklen)
                specbacktrackcumsum(:,k-ridgetracker.backtracklen) = backtrackcumsum;
                specbacktracktouchedflags(:,k-ridgetracker.backtracklen) = backtracktouchedflags;
            end

            %
            % find t-f locations detected as part of ridge
            %
            detected_ridge_back = zeros(Nfc,1);
            for iii=1:Nfc
              if ( backtrackcumsum(iii) <= ridgetracker.detect_ridge_backtrack_thresh ),
                 if ridgetracker.specridge_link_back(iii,1) == 0
                     continue;
                 end
                 % pickup from the previous ridge_index
                 ridge_index = ridgetracker.stftdetectridgeback(ridgetracker.specridge_link_back(iii,1),2);
                 if ( ridge_index == 0 ) 
                    ridge_index = -1;
                    
                    for l = iii:-1:ridgetracker.f_ridge_down(iii)
                        if (detected_ridge_back(l) > 0)
                            ridge_index = detected_ridge_back(l);
                            break;
                        end
                    end
                    if (ridge_index == -1) % still no luck, create a new label
                        ridgetracker.ridgect = ridgetracker.ridgect + 1;
                        ridge_index = ridgetracker.ridgect;
                    end
                    
                 end         
                 detected_ridge_back(iii) = ridge_index;
                 
              end
            end

            %[detected_ridge_back, ridgetracker] = mergeNodes(detected_ridge_back, ridgetracker);

            curr_min_freq = (Nfc+1)*ones(1, length(ridgetracker.ridge_loglik));
            curr_max_freq = zeros(1, length(ridgetracker.ridge_loglik));
            for iii=1:Nfc
                ridge_index = detected_ridge_back(iii);
                if (ridge_index > 0)
                    if (ridge_index > length(ridgetracker.ridge_loglik))
                        curr_min_freq(ridge_index) = iii;
                        curr_max_freq(ridge_index) = iii;
                        
                        ridgetracker.ridge_min_time(ridge_index) = k-ridgetracker.backtracklen;
                        ridgetracker.ridge_max_time(ridge_index) = k-ridgetracker.backtracklen;
                        ridgetracker.ridge_min_freq(ridge_index) = iii;
                        ridgetracker.ridge_max_freq(ridge_index) = iii;
                        ridgetracker.ridge_loglik(ridge_index) = backtrackcumsum(iii);
                        ridgetracker.ridge_desc{ridge_index} = [];
                        ridgetracker.ridge_max_inst_bw(ridge_index) = 0;
                    else
                        curr_min_freq(ridge_index) = min(iii, curr_min_freq(ridge_index));
                        curr_max_freq(ridge_index) = max(iii, curr_max_freq(ridge_index));
                        
                        ridgetracker.ridge_max_time(ridge_index) = k-ridgetracker.backtracklen;
                        ridgetracker.ridge_min_freq(ridge_index) = min(iii,ridgetracker.ridge_min_freq(ridge_index));
                        ridgetracker.ridge_max_freq(ridge_index) = max(iii,ridgetracker.ridge_max_freq(ridge_index));
                        ridgetracker.ridge_loglik(ridge_index) = ridgetracker.ridge_loglik(ridge_index)+backtrackcumsum(iii);
                    end
                end
            end           
            ridge_index_set = unique(detected_ridge_back);
            for l = 1:numel(ridge_index_set)
                if (ridge_index_set(l) == 0) % there might be case that ridge_index_set(1) is not 0
                    continue;
                end
                ridgetracker.ridge_desc{ridge_index_set(l)} = [ridgetracker.ridge_desc{ridge_index_set(l)} [curr_min_freq(ridge_index_set(l)); curr_max_freq(ridge_index_set(l))]];
                ridgetracker.ridge_max_inst_bw(ridge_index_set(l)) = max(ridgetracker.ridge_max_inst_bw(ridge_index_set(l)), curr_max_freq(ridge_index_set(l)) - curr_min_freq(ridge_index_set(l)) );
            end
            
            ridgetracker.stftdetectridgeback = [ridgetracker.stftdetectridgeback(:,2) detected_ridge_back];

            % debugging
            if (k > ridgetracker.backtracklen)
                stftdetectridgeback(:,k-ridgetracker.backtracklen) = detected_ridge_back;
            end
        end

        % Remove trivial ridges
        %{
        idxT = ridgetracker.ridge_max_time > ridgetracker.ridge_min_time;
        idxF = ridgetracker.ridge_max_freq > ridgetracker.ridge_min_freq;
        idx = idxT & idxF;
        ridgetracker.ridgect = sum(idx);
        ridgetracker.ridge_min_time(~idx) = [];
        ridgetracker.ridge_max_time(~idx) = [];
        ridgetracker.ridge_min_freq(~idx) = [];
        ridgetracker.ridge_max_freq(~idx) = [];
        ridgetracker.ridge_loglik(~idx) = [];
        ridgetracker.ridge_desc(~idx) = [];
        %}

        % Highlight time-frequency blocks
        B = cell(ridgetracker.ridgect, 1); % ridge boundaries
        for k = 1:ridgetracker.ridgect
            lowfreq = ridgetracker.ridge_desc{k}(1,:);
            highfreq = ridgetracker.ridge_desc{k}(2,:);
            ridgetime = [ridgetracker.ridge_min_time(k):ridgetracker.ridge_max_time(k)];
            % Connected points go from start, lowfreq, connect, then backward high freq
            B{k} = [lowfreq' ridgetime';...
                     flipud([highfreq' ridgetime']);...
                     [highfreq(1):-1:lowfreq(1)]' ones(size([highfreq(1):-1:lowfreq(1)]'))*ridgetracker.ridge_min_time(k)];
        end

        % Convert ridge boundary to binary images
        BW = zeros(size(spec,1), size(spec,2), length(B));
        for k = 1:length(B)
            boundary = B{k};
            tmp = roipoly(spec, boundary(:,2), boundary(:,1));
            tmp(sub2ind(size(tmp), boundary(:,1), boundary(:,2))) = 1; % including the boundary
            BW(:,:,k) = tmp;
        end
        % Prune out trivial/zero-area roi
        %{
        ind = false(1, length(rB));
        for k =1:length(rB)
            tmp = rBW(:,:,k);
            if (sum(tmp(:)) < 15)
                ind(k) = true;
            end
        end
        rB(ind) = [];
        rBW(:,:,ind) = [];
        %}
        BWall = sum(BW, 3); % ridge binary image

        if (PLOT)
            %subplot(211); 
            hold on; imagesc([1:size(data,1)], [1:Nfc], spec)
            for k = 1:length(B)
                plot(B{k}(:,2), B{k}(:,1), 'k', 'linewidth', 2)
            end
            axis('tight')
        end
        
        ridge_min_time{expIdx} = ridgetracker.ridge_min_time;
        ridge_max_time{expIdx} = ridgetracker.ridge_max_time;
        ridge_min_freq{expIdx} = ridgetracker.ridge_min_freq;
        ridge_max_freq{expIdx} = ridgetracker.ridge_max_freq;
        ridge_loglik{expIdx} = ridgetracker.ridge_loglik;
        ridge_max_inst_bw{expIdx} = ridgetracker.ridge_max_inst_bw;
    elseif (method == 2)
        %% Magnitude tracking
        ridgetracker.alpha_ridge = exp(-Nblk/(0.1*fs));
        ridgetracker.f_ridge_down_max = ceil(20000*Nblk*Nfc*2/(fs*fs)); % want X Hz/sec chirp rate; f_ridge_down_max units are freq-samples/blklen-time-samples; 1 freq-sample = fs/binct Hz
        ridgetracker.f_ridge_up_max = ridgetracker.f_ridge_down_max;
        ridgetracker.f_ridge_down = zeros(Nfc,1);
        ridgetracker.f_ridge_up = zeros(Nfc,1);
        for ii=1:Nfc,
           ridgetracker.f_ridge_down(ii) = max(1,ii-ridgetracker.f_ridge_down_max);
           ridgetracker.f_ridge_up(ii) = min(Nfc,ii+ridgetracker.f_ridge_up_max);
        end
        ridgetracker.ridge_offset_penalty_max = 0.05;
        ridgetracker.ridge_offset_penalty = ridgetracker.ridge_offset_penalty_max/max(ridgetracker.f_ridge_up_max,ridgetracker.f_ridge_down_max);
        ridgetracker.log_prob_noise_min_ridge = -6.0;
        ridgetracker.detect_ridge_backtrack_thresh = thresh{2}(threshIdx);
        ridgetracker.backtracklen = ceil(0.02*fs/Nblk); % enter the multiplier as the approximate track length in sec
        % States
        ridgetracker.log_prob_noise_ridge_cum_now = zeros(Nfc,1);
        ridgetracker.specridge_link_back = zeros(Nfc,ridgetracker.backtracklen+1);
        ridgetracker.speclog_prob_noise_ridge = zeros(Nfc,ridgetracker.backtracklen+1);
        ridgetracker.stftdetectridgeback = zeros(Nfc,2);
        ridgetracker.ridge_min_time = zeros(1,0);
        ridgetracker.ridge_max_time = zeros(1,0);
        ridgetracker.ridge_min_freq = zeros(1,0);
        ridgetracker.ridge_max_freq = zeros(1,0);
        ridgetracker.ridge_loglik = zeros(1,0);
        ridgetracker.ridge_desc = cell(1,0);
        ridgetracker.ridgect = 0;

        % Main
        data = enframe(x,Nblk*2, Nblk*2);
        spec = zeros(Nfc, size(data,1));
        spec_noise_floor = zeros(Nfc, size(data,1));
        specridge_link_back = zeros(Nfc, size(data,1));
        speclog_prob_noise_ridge = zeros(Nfc, size(data,1));
        specbacktrackcumsum = zeros(Nfc, size(data,1));
        specbacktracktouchedflags = zeros(Nfc, size(data,1));
        stftdetectridgeback = zeros(Nfc, size(data,1));
        for k = 1:size(data,1)
            s = abs(fft(w.*data(k,:)));
            s = s(1:Nfc);
            spec(:,k) = s;

            offset_ridge = zeros(Nfc,1);
            ridge_link_back = zeros(Nfc,1);
            log_prob_noise_ridge = zeros(Nfc,1);
            log_prob_noise_ridge_cum_last = ridgetracker.log_prob_noise_ridge_cum_now;
            for j=1:Nfc
                %  TIME-VARYING SPECTRAL RIDGE TRACKING
                log_prob_noise_ridge(j) = max(ridgetracker.log_prob_noise_min_ridge,-s(j));
                [~, offset_ridge(j)] = ...
                    min((1 - ridgetracker.ridge_offset_penalty*abs(j - [ridgetracker.f_ridge_down(j):ridgetracker.f_ridge_up(j)]')).*...
                    log_prob_noise_ridge_cum_last(ridgetracker.f_ridge_down(j):ridgetracker.f_ridge_up(j)));

                ridgetracker.log_prob_noise_ridge_cum_now(j) = ridgetracker.alpha_ridge*log_prob_noise_ridge_cum_last(ridgetracker.f_ridge_down(j)+offset_ridge(j)-1) + ...
                      (1-ridgetracker.alpha_ridge)*log_prob_noise_ridge(j);
                ridge_link_back(j) = ridgetracker.f_ridge_down(j)+offset_ridge(j)-1;  % pointers to the previous ridge frequency
            end
            ridgetracker.specridge_link_back = [ridgetracker.specridge_link_back(:,2:end) ridge_link_back];
            ridgetracker.speclog_prob_noise_ridge = [ridgetracker.speclog_prob_noise_ridge(:,2:end) log_prob_noise_ridge];

            % debugging
            speclog_prob_noise_ridge(:,k) = log_prob_noise_ridge;
            specridge_link_back(:,k) = ridge_link_back;

            %
            % Backward track detection
            %
            % track back backtracklen time blocks
            backtrackcumsum = log_prob_noise_ridge;
            backtracktouchedflags = ones(Nfc,1);
            for i5=ridgetracker.backtracklen+1:-1:2,
              backtrackcumsumnew = zeros(Nfc,1);
              backtracktouchedflagsnew = zeros(Nfc,1);
              % Follow pointer back from each frequency to ridge-frequency at previous time and update backward cumsum of detection metric
              for iii=1:Nfc
                 if ( backtracktouchedflags(iii) == 1),
                    if ridgetracker.specridge_link_back(iii,i5) == 0
                        continue;
                    end
                    backtrackcumsumnew(ridgetracker.specridge_link_back(iii,i5)) = ...
                        min(backtrackcumsumnew(ridgetracker.specridge_link_back(iii,i5)),...
                        backtrackcumsum(iii)+ridgetracker.speclog_prob_noise_ridge(ridgetracker.specridge_link_back(iii,i5),i5-1));
                    backtracktouchedflagsnew(ridgetracker.specridge_link_back(iii,i5)) = 1;  % indicate which frequencies were part of a backtracked ridge
                 end
              end
              backtrackcumsum = backtrackcumsumnew;
              backtracktouchedflags = backtracktouchedflagsnew;
            end
            backtrackcumsum = (1.0/(ridgetracker.backtracklen+1))*backtrackcumsum;

            % debugging
            if (k > ridgetracker.backtracklen)
                specbacktrackcumsum(:,k-ridgetracker.backtracklen) = backtrackcumsum;
                specbacktracktouchedflags(:,k-ridgetracker.backtracklen) = backtracktouchedflags;
            end

            %
            % find t-f locations detected as part of ridge
            %
            detected_ridge_back = zeros(Nfc,1);
            for iii=1:Nfc
              if ( backtrackcumsum(iii) <= ridgetracker.detect_ridge_backtrack_thresh ),
                 if ridgetracker.specridge_link_back(iii,1) == 0
                     continue;
                 end
                 % pickup from the previous ridge_index
                 ridge_index = ridgetracker.stftdetectridgeback(ridgetracker.specridge_link_back(iii,1),2);
                 if ( ridge_index == 0 ) 
                    ridge_index = -1;
                    
                    for l = iii:-1:ridgetracker.f_ridge_down(iii)
                        if (detected_ridge_back(l) > 0)
                            ridge_index = detected_ridge_back(l);
                            break;
                        end
                    end
                    if (ridge_index == -1) % still no luck, create a new label
                        ridgetracker.ridgect = ridgetracker.ridgect + 1;
                        ridge_index = ridgetracker.ridgect;
                    end
                    
                 end         
                 detected_ridge_back(iii) = ridge_index;
                 
              end
            end

            %[detected_ridge_back, ridgetracker] = mergeNodes(detected_ridge_back, ridgetracker);

            curr_min_freq = (Nfc+1)*ones(1, length(ridgetracker.ridge_loglik));
            curr_max_freq = zeros(1, length(ridgetracker.ridge_loglik));
            for iii=1:Nfc
                ridge_index = detected_ridge_back(iii);
                if (ridge_index > 0)
                    if (ridge_index > length(ridgetracker.ridge_loglik))
                        curr_min_freq(ridge_index) = iii;
                        curr_max_freq(ridge_index) = iii;
                        
                        ridgetracker.ridge_min_time(ridge_index) = k-ridgetracker.backtracklen;
                        ridgetracker.ridge_max_time(ridge_index) = k-ridgetracker.backtracklen;
                        ridgetracker.ridge_min_freq(ridge_index) = iii;
                        ridgetracker.ridge_max_freq(ridge_index) = iii;
                        ridgetracker.ridge_loglik(ridge_index) = backtrackcumsum(iii);
                        ridgetracker.ridge_desc{ridge_index} = [];
                    else
                        curr_min_freq(ridge_index) = min(iii, curr_min_freq(ridge_index));
                        curr_max_freq(ridge_index) = max(iii, curr_max_freq(ridge_index));
                        
                        ridgetracker.ridge_max_time(ridge_index) = k-ridgetracker.backtracklen;
                        ridgetracker.ridge_min_freq(ridge_index) = min(iii,ridgetracker.ridge_min_freq(ridge_index));
                        ridgetracker.ridge_max_freq(ridge_index) = max(iii,ridgetracker.ridge_max_freq(ridge_index));
                        ridgetracker.ridge_loglik(ridge_index) = ridgetracker.ridge_loglik(ridge_index)+backtrackcumsum(iii);
                    end
                end
            end           
            ridge_index_set = unique(detected_ridge_back);
            for l = 2:numel(ridge_index_set)
                ridgetracker.ridge_desc{ridge_index_set(l)} = [ridgetracker.ridge_desc{ridge_index_set(l)} [curr_min_freq(ridge_index_set(l)); curr_max_freq(ridge_index_set(l))]];
            end
            
            ridgetracker.stftdetectridgeback = [ridgetracker.stftdetectridgeback(:,2) detected_ridge_back];

            % debugging
            if (k > ridgetracker.backtracklen)
                stftdetectridgeback(:,k-ridgetracker.backtracklen) = detected_ridge_back;
            end
        end

        % Remove trivial ridges
        %{
        idxT = ridgetracker.ridge_max_time > ridgetracker.ridge_min_time;
        idxF = ridgetracker.ridge_max_freq > ridgetracker.ridge_min_freq;
        idx = idxT & idxF;
        ridgetracker.ridgect = sum(idx);
        ridgetracker.ridge_min_time(~idx) = [];
        ridgetracker.ridge_max_time(~idx) = [];
        ridgetracker.ridge_min_freq(~idx) = [];
        ridgetracker.ridge_max_freq(~idx) = [];
        ridgetracker.ridge_loglik(~idx) = [];
        ridgetracker.ridge_desc(~idx) = [];
        %}

        % Highlight time-frequency blocks
        B = cell(ridgetracker.ridgect, 1); % ridge boundaries
        for k = 1:ridgetracker.ridgect
            lowfreq = ridgetracker.ridge_desc{k}(1,:);
            highfreq = ridgetracker.ridge_desc{k}(2,:);
            ridgetime = [ridgetracker.ridge_min_time(k):ridgetracker.ridge_max_time(k)];
            % Connected points go from start, lowfreq, connect, then backward high freq
            B{k} = [lowfreq' ridgetime';...
                     flipud([highfreq' ridgetime']);...
                     [highfreq(1):-1:lowfreq(1)]' ones(size([highfreq(1):-1:lowfreq(1)]'))*ridgetracker.ridge_min_time(k)];
        end

        % Convert ridge boundary to binary images
        BW = zeros(size(spec,1), size(spec,2), length(B));
        for k = 1:length(B)
            boundary = B{k};
            tmp = roipoly(spec, boundary(:,2), boundary(:,1));
            tmp(sub2ind(size(tmp), boundary(:,1), boundary(:,2))) = 1; % including the boundary
            BW(:,:,k) = tmp;
        end
        % Prune out trivial/zero-area roi
        %{
        ind = false(1, length(rB));
        for k =1:length(rB)
            tmp = rBW(:,:,k);
            if (sum(tmp(:)) < 15)
                ind(k) = true;
            end
        end
        rB(ind) = [];
        rBW(:,:,ind) = [];
        %}
        BWall = sum(BW, 3); % ridge binary image

        if (PLOT)
            %subplot(211); 
            hold on; imagesc([1:size(data,1)], [1:Nfc], spec)
            for k = 1:length(B)
                plot(B{k}(:,2), B{k}(:,1), 'k', 'linewidth', 2)
            end
            axis('tight')
        end
    elseif (method == 3)
        %% Find the offline/Otsu solution binary images
        data = enframe(x,Nblk*2, Nblk*2);
        spec = zeros(Nfc, size(data,1));
        for k = 1:size(data,1)
            s = abs(fft(w.*data(k,:)));
            s = s(1:Nfc);
            spec(:,k) = s;
        end
        specInd = im2bw(spec, graythresh(spec));
        [B, L] = bwboundaries(specInd, 'noholes');
        % Prune out trivial blobs with too small area
        %{
        stats = regionprops(L, 'ConvexArea');
        ind = false(1, numel(stats));
        for k = 1:numel(stats)
            if (stats(k).ConvexArea < 15)
                ind(k) = true;
            end
        end
        B(ind) = [];
        %}

        % Create blobs for each boundary in a binary image
        BW = zeros(size(spec,1), size(spec,2), length(B));
        for k = 1:length(B)
            boundary = B{k};
            BW(:,:,k) = roipoly(spec, boundary(:,2), boundary(:,1));
        end
        BWall = sum(BW, 3); % ground truth binary image

        if (PLOT)
            %subplot(212); imshow(label2rgb(L, @jet, [.5 .5 .5])); axis xy
            imagesc(L); axis xy;
            hold on
            for k = 1:length(B)
                boundary = B{k};
                plot(boundary(:,2), boundary(:,1), 'k', 'LineWidth', 2)
            end
        end
    elseif (method == 4)
        % Gaussian frequency transition
    end
    
    %% Evaluation with binary image using overlapping criterion
    load([outdir strtok(file(expIdx).name, '.') outext], 'objects');
    label = objects.labels; label(label>1) = 1;
    
    DET = BWall & label; nDET = sum(DET(:)); accDET = accDET + nDET;
    FA = BWall & ~label; nFA = sum(FA(:)); accFA = accFA + nFA;
    nLabel1 = sum(label(:)); accLabel1 = accLabel1 + nLabel1;
    nLabel0 = sum(~label(:)); accLabel0 = accLabel0 + nLabel0;
    if (PLOT)
        title(sprintf('pDET: %f, pFA: %f', nDET/nLabel1, nFA/nLabel0));
    end
end
pDET = accDET/accLabel1;
pFA = accFA/accLabel0;
ROC = [ROC [pDET;pFA]];
end
load(ROCfile, 'ROCs');
ROCs{method} = ROC;
save(ROCfile, 'ROCs', '-append')
end

if (method == 1)
    save(ROCfile, 'ridge_min_time', 'ridge_max_time', 'ridge_min_freq', 'ridge_max_freq', 'ridge_loglik', 'ridge_max_inst_bw', '-append')
end