function synthGCWData()
% Synthesize more GCW data using LPC
clear all; close all;

%datFolder = '../acousticsearch/data/whistlecommand/';
datFolder = '../genCascade/data/GCW/';
file = dir([datFolder '*.wav']);
filename = cell(numel(file),1);
for k = 1:numel(file)
    filename{k} = [datFolder file(k).name];
end
for k = 1:numel(filename)
    [y, fs] = wavread(filename{k});
    [pathstr, name, ext] = fileparts(filename{k});
    y1 = y(:,1);
    %[F,M] = swsmodel(y1,fs);
    %yr = synthtrax(F,M,fs);
    [a,g] = lpcfit(y1);
    yl = lpcsynth(a,g);
    y2 = [zeros(0.1*fs,1);y1];
    %y2 = y(:,2);
    %wavwrite(yr, fs, [pathstr '/' name '-1.wav']);
    wavwrite(yl, fs, [pathstr '/' name '-2.wav']);
    wavwrite(y2, fs, [pathstr '/' name '-3.wav']);
end