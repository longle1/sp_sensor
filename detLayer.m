function [feat] = detLayer(events)
% function [feat] = detLayer(events)
% 
% Detection layer
%
% Long Le <longle1@illinois.edu>
% University of Illinois

addpath(genpath('../voicebox/'))
addpath(genpath('../jsonlab/'))
addpath(genpath('../V1_1_urlread2'));
addpath(genpath('../sas-clientLib/src'))

%servAddr = '128.32.33.227';
servAddr = 'acoustic.ifp.illinois.edu';
DB = 'publicDb';
USER = 'nan';
PWD = 'publicPwd';
if ~exist('DATA','var')
    DATA = 'data2';
end

%% sort events
usTt = zeros(1,0); % unsorted time
for k = 1:numel(events)
    usTt(k) = datenum8601(events{k}.recordDate);
end
[tt, idx] = sort(usTt);
selEvents = events(idx);

%% analyze events
fftSize = 256;

feat = zeros(numel(tt),16);
for k = 41:numel(tt)
    disp(k);
    
    data = IllDownGrid(servAddr, DB, USER, PWD, DATA, selEvents{k}.filename);
    if ~strcmp(data(1:4),'RIFF')
        fprintf(1,'missing data\n');
        
        %{
        fprintf(1,'removing event\n');
        resp = IllDeleteCol(servAddr, DB, USER, PWD, EVENT, selEvents{k}.filename);
        jsonResp = loadjson(resp);
        if (isfield(jsonResp,'ok'))
            fprintf(1,'event with missing data removed\n');
        else
            fprintf(1,'unable to remove event with missing data\n');
        end
        %}
        
        continue;
    end
    [y, header] = wavread_char(data);
    fs = double(header.sampleRate);

    [S,usTt,ff] = mSpectrogram(y,fs,fftSize);
    
    % detection
    if ~exist('newState','var')
        [newState,specDet] = detectOneFile(S,[],false);
    else
        state = newState;
        [newState,specDet] = detectOneFile(S,state,false);
    end
    
    % TODO: compute feat
    
    save(sprintf('localLogs/detLayer/%d.mat',k),'y','S','specDet')
    %{
    figure('units','normalized','outerposition',[0 0 1 1]);
    subplot(311); plot(y); axis tight;
    subplot(312); imagesc(usTt,ff,log(S)); axis xy;
    subplot(313); imagesc(usTt,ff,specDet>0); axis xy;

    result = input('Continue? y/n: ','s');
    if result == 'n'
        break;
    end
    close;
    %}
end