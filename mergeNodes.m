% Define the node merge interface where different merging algorithms can be
% implemented here
%
% Long Le
% University of Illinois
% longle1@illinois.edu
%
function [new_detected_ridge_back, ridgetracker] = mergeNodes(detected_ridge_back, ridgetracker)

labelStack = java.util.Stack();
idxStack = java.util.Stack();
for iii=1:numel(detected_ridge_back)
    idx = labelStack.search(detected_ridge_back(iii));
    if (idx == -1)
        % Build up the stack if the node did not exist
        labelStack.push(detected_ridge_back(iii));
        idxStack.push(iii);
    elseif (idx == 1)
        % update the latest index of the same label
        idxStack.pop();
        idxStack.push(iii);
    elseif (idx > 1)
        if (detected_ridge_back(iii) >= 1)
            % Merge all labeled nodes in between
            for k = 1:idx-1
                labelStack.pop();
                tmpIdxU = idxStack.pop();
                tmpIdxL = idxStack.peek()+1;
                detected_ridge_back(tmpIdxL:tmpIdxU) = detected_ridge_back(iii);
            end
            % update the latest index of the same label
            idxStack.pop();
            idxStack.push(iii);
        else
            % unlabeled and null nodes, add to the stack
            labelStack.push(detected_ridge_back(iii));
            idxStack.push(iii);
        end
    end
end

% Allocate new ridge (not create yet) for unlabeled nodes
labelStack = labelStack.toArray();
idxStack = idxStack.toArray();
for iii = 1:numel(labelStack)
    if (labelStack(iii) == -1)
        ridgetracker.ridgect = ridgetracker.ridgect + 1;
        
        tmpIdxU = idxStack(iii);
        if (iii > 1)
            tmpIdxL = idxStack(iii-1)+1;
        else
            tmpIdxL = 1;
        end
        detected_ridge_back(tmpIdxL:tmpIdxU) = ridgetracker.ridgect;
    end
end

new_detected_ridge_back = detected_ridge_back;
