% init workspace
%
% Long Le
% University of Illinois
% longle1@illinois.edu
%

clear all; close all

addpath(genpath('../voicebox/'))
addpath(genpath('../network-paper/'))
addpath(genpath('./sinemodel/'))
