function bdrySet = partSet(bdry)

tMin = min(bdry(:,1));
tMax = max(bdry(:,2));
tStep = 128/16000;
t = tMin-tStep:tStep:tMax+tStep;
bdryInd = zeros(size(bdry,1),numel(t));
for k = 1:size(bdry,1)
    bdryInd(k,:) = t >= bdry(k,1) & t <= bdry(k,2);
    end
bdryIndAll = any(bdryInd,1);

bdrySet = zeros(0,2);
isOn = false;
for k = 1:numel(bdryIndAll)
    if isOn == false && bdryIndAll(k) == true
        bdrySet(end+1,1) = t(k);
        isOn = true;
    elseif isOn == true && bdryIndAll(k) == false
        bdrySet(end,2) = t(k-1);
        isOn = false;
    end
end