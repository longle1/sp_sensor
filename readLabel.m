function readLabel()
% Read the label
clear all; %close all

%datFolder = '../genCascade/data/GCW/';
%datFolder = '../acousticsearch/data/whistlecommand/';
datFolder = '../matlab-realtime-audioprocessing/TIDIGIT_adults_crop_crop/';
outext = '_objects.mat';

filename = 'GCW/GCW-A-(1)';
load(['label/' filename outext]);
figure; imagesc(objects.labels)
title(filename)

[y, fs] = wavread([datFolder filename '.wav']);
y = y(:,1);
h = fir1(8, 20000/fs); % low pass filter
y = filter(h, 1, y);
y = resample(y, 20000, fs);
fs = 20000;
x = y + randn(size(y))*std(y)/sqrt(1000);

Nblk = 128;
Nfc = 128;
w = hann(Nblk*2)';

% create spectrogram
data = enframe(x,Nblk*2, Nblk*2);
spec = zeros(Nfc, size(data,1));
for k = 1:size(data,1)
    s = abs(fft(w.*data(k,:)));
    s = s(1:Nfc);
    spec(:,k) = s;
end
figure; imagesc([1:size(data,1)], [1:Nfc], spec)
title(filename)