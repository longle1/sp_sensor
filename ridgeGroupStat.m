function [feat,selGId,bdry] = ridgeGroupStat(F,T,gId,M,fs,fftSize)
% Summary statistics for ridges
% 
% Long Le
% longle1@illinois.edu
%

% calculate statistics for each selected group of ridges
nMelBank = 16;
melFilt = full(melbankm(nMelBank,fftSize,fs,0,0.5,'yu')); % avoid zeroing out dc component
% figure; plot((0:size(melFilt,2)-1)/fftSize*fs,melFilt(6,:));
melFiltRange = zeros(nMelBank,2);
for k = 1:nMelBank
    melFiltRange(k,1) = find(diff([0 melFilt(k,:)>0 0]) == 1); % min freq
    melFiltRange(k,2) = find(diff([0 melFilt(k,:)>0 0]) == -1)-1; % max freq
end

% find the summary feature
featMat = zeros(nMelBank,1);
selGId = zeros(nMelBank,1);
for k = 1:nMelBank
    for l = 1:size(F,1)
        currM = M(l,:);
        currF = F(l,:);
        validCurrM = currM(~isnan(currM));
        validCurrF = currF(~isnan(currF));

        %featMat(k) = featMat(k) + sum(melFilt(k,validCurrF/fs*fftSize).*validCurrM);
        featMat(k) = featMat(k) + sum(melFilt(k,validCurrF/fs*fftSize));
    end
end
%feat = featMat/sum(featMat);
feat = featMat;

% find detection interval
bdry = zeros(size(T,1),2);
for k = 1:size(T,1)
    currT = T(k,:);
    validCurrT = currT(~isnan(currT));
    
    bdry(k,:) = [min(validCurrT) max(validCurrT)];
end

%{
for k = 1:nMelBank
    cnt = 0;
    rId = zeros(0,1);
    MGroup = zeros(0,1);
    % for each ridge that fall in a bank, store the ridge's id and calculate its sum M 
    for l = 1:size(F,1)
        if numel(F(l,:)) > 1 % a line must be at least 2 points
            currM = M(l,:);
            currF = F(l,:);
            validCurrM = currM(~isnan(currM));
            validCurrF = currF(~isnan(currF));

            if ~((min(validCurrF)/fs*fftSize > melFiltRange(k,2)) || (max(validCurrF)/fs*fftSize < melFiltRange(k,1)))
                cnt = cnt+1;
                rId(cnt,1) = l;
                %MGroup(cnt,1) = sum(melFilt(k,validCurrF/fs*fftSize).*validCurrM);
                MGroup(cnt,1) = sum(validCurrM);
            end
        end
    end
    % pick the dominant instantaneous frequency line
    if (numel(MGroup) > 0)
        [~, idx]= max(MGroup);
        selGId(k,1) = rId(idx); % Id <= 0 is invalid
        
        % compute features based on selected ridges
        currF = F(rId(idx),:);
        validCurrF = currF(~isnan(currF)); % F <= 0 is invalid
        %currT = T(rId(idx),:);
        %validCurrT = currT(~isnan(currT));
        
        featMat(k,1) = mean(validCurrF);
        featMat(k,2) = max(MGroup)/sum(MGroup);
        featMat(k,3) = std(validCurrF);
    end
end
feat = [featMat(:,1); featMat(:,2); featMat(:,3)]';
%feat = [featMat(:,1); featMat(:,2)]';
%}

%{
gIdClass = unique(gId);
% Select top groups
avgProb = zeros(numel(gIdClass),1);
for k = 1:numel(gIdClass)
    currM = M(gId == gIdClass(k),:);
    avgProb(k) = mean(currM(~isnan(currM)));
end
[~, idx] = sort(avgProb,'descend');
selGIdClass = gIdClass(idx(1:min(32,numel(gIdClass))));

W = zeros(1,1,numel(selGIdClass));
bdry = zeros(numel(selGIdClass),2);
for k = 1:numel(selGIdClass)
    % get a subset of F and T
    currF = F(gId == selGIdClass(k),:);
    validCurrF = currF(~isnan(currF)); validCurrF = validCurrF(:);
    currT = T(gId == selGIdClass(k),:);
    validCurrT = currT(~isnan(currT)); validCurrT = validCurrT(:);
    currM = M(gId == selGIdClass(k),:);
    validCurrM = currM(~isnan(currM)); validCurrM = validCurrM(:);
    
    % compute statistics for valid values
    for l = 1:nMelBank
        featAll(l,1,k) = mean(melFilt(l,validCurrF/fs*fftSize+1)'.*validCurrM.*(validCurrT <= 0.1));
        featAll(l,2,k) = mean(melFilt(l,validCurrF/fs*fftSize+1)'.*validCurrM.*(validCurrT >  0.1));
    end
    W(k) = numel(validCurrM(:));
    %featAll(:,1,k) = dct(featAll(:,1,k));
    
    % find detection interval
    bdry(k,:) = [min(validCurrT) max(validCurrT)];
end
feat = squeeze(sum(bsxfun(@times,W/sum(W(:)),featAll),3)); % aggregate over all ridge groups

feat = feat(:)'; % combine all ridge groups in a file
%}