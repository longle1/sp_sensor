function testAcousticServiceFeature(testFilename)
% Script to verify that feature generated from the acoustic service are
% correct/match with the original MatLab
%
% Long Le
% University of Illinois
%

addpath(genpath('../voicebox/'))
addpath(genpath('../jsonlab/'))
addpath(genpath('../V1_1_urlread2'));
addpath(genpath('../sas-clientLib/src'))

DB = 'publicDb';
USER = 'publicUser';
PWD = 'publicPwd';

isPlot = true;

%% Use the acoustic service to obtain the data
%testFilename = '20151005171519674.wav';
events = IllDownCol(DB, USER, PWD, 'event', testFilename);
data = IllDownGrid(DB, USER, PWD, 'data', testFilename);
[y, header] = wavread_char(data);
%soundsc(y, double(header.sampleRate))

fs = double(header.sampleRate);
fftSize = 256;
[S,tt,ff] = mSpectrogram(y,fs,fftSize);

%% Verify the computation of summary stats of the service
jIdx = fieldnames(events{1}.TI);
%gId = events{1}.groupId';
R = nan(numel(jIdx), size(S,2));
M = nan(numel(jIdx), size(S,2));
for k = 1:numel(jIdx)
    R(k,events{1}.TI.(jIdx{k})) = events{1}.FI.(jIdx{k})+1; % must +1 since range(FI) = [0,nFc-1]
    M(k,events{1}.TI.(jIdx{k})) = events{1}.M.(jIdx{k});
end
[F,T] = ridgeIdx2TFRidge(R,fs,fftSize,tt);

[feat,selGId,bdry] = ridgeGroupStat(F,T,gId,M,fs,fftSize);
disp(reshape(feat,16,1));

% disp the difference between the service feature extraction and the ground truth
disp(norm(feat-events{1}.feat));

if isPlot
    figure; hold on
    imagesc(tt, ff, log(abs(S)));
    colormap(1-gray)
    if size(F,1) > 0 % check if there's at least a ridge
        for k = 1:numel(selGId)
            plot(T(selGId(k),:)',F(selGId(k),:)',idx2col(k));
        end
    end
    axis xy; axis tight
end

%% Compare the feature computed by the service with the locally computed one
% extract ridges
[R,gId,M] = extractRidge(abs(S), fs, fftSize, 0.04, exp(-(10-6))*ones(fftSize/2,1));
[F,T] = ridgeIdx2TFRidge(R,fs,fftSize,tt);

% compute summary stats
[feat,selGId,bdry] = ridgeGroupStat(F,T,gId,M,fs,fftSize);
disp(reshape(feat,16,3));

if isPlot
    figure; hold on
    imagesc(tt, ff, log(abs(S)));
    colormap(1-gray)
    if size(F,1) > 0 % check if there's at least a ridge
        for k = 1:numel(selGId)
            plot(T(gId == selGId(k),:)',F(gId == selGId(k),:)',idx2col(k));
        end
    end
    axis xy; axis tight
end

%% Try to classify the event using the trained model
load('../node-paper/classLoss_2015-09-17-23-03-27-099_1.mat');
labIdx = 4;
% optional Platt weighting
if strcmp(svmModel{labIdx}.ScoreTransform,'none')
    svmModel{labIdx} = fitPosterior(svmModel{labIdx}, svmModel{labIdx}.X, svmModel{labIdx}.Y);
end
[eventOut, eventPost] = predict(svmModel{labIdx}, events{1}.feat);
[featOut, featPost] = predict(svmModel{labIdx}, feat);